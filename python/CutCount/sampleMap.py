import ROOT
import numpy as np
import uproot
import pandas as pd
import copy 

class sampleMap():

	_sample_name = ""
	_leaf_list = []
	_N_leaf_list = 0
	_more_or_less = []

	def __init__(self, samplename):
		self._sample_name = samplename


	def loadRootFile(self, rootpath, rootname, treename , leafname = [] ):
		root_file = uproot.open(rootpath+rootname)
		self._var_pandas = root_file[treename].pandas.df(leafname)
		self._leaf_list = leafname
		self._N_leaf_list = len(leafname)


	def loadMoreLess(self, moreless = [] ):
		self._more_or_less = moreless


	def applyOneCut(self, cutvaluelist = [] ):
		new_var_pandas = copy.deepcopy(self._var_pandas)
		weight_name = self._leaf_list[0]
		
		for i in range(self._N_leaf_list):
			if( self._more_or_less[i] == '>' ):
				new_var_pandas = new_var_pandas[ new_var_pandas[self._leaf_list[i] ] > cutvaluelist[i] ]
			else:
				new_var_pandas = new_var_pandas[ new_var_pandas[self._leaf_list[i] ] < cutvaluelist[i] ]

		pandas_weight = new_var_pandas[weight_name]
		value_sum = pandas_weight.sum()
		value_err = pandas_weight.pow(2).sum()
		
		return value_sum,value_err

	
	def applyOneCutNoErr(self, cutvaluelist = [] ):
		new_var_pandas = copy.deepcopy(self._var_pandas)
		weight_name = self._leaf_list[0]
		
		for i in range(self._N_leaf_list):
			if( self._more_or_less[i] == '>' ):
				new_var_pandas = new_var_pandas[ new_var_pandas[self._leaf_list[i] ] > cutvaluelist[i] ]
			else:
				new_var_pandas = new_var_pandas[ new_var_pandas[self._leaf_list[i] ] < cutvaluelist[i] ]

		pandas_weight = new_var_pandas[weight_name]
		value_sum = pandas_weight.sum()
		#value_err = pandas_weight.pow(2).sum()
		#return value_sum,value_err
		return value_sum
	

	def getName(self):
		return self._sample_name

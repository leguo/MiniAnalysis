#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaTree.h"

// Header file for the classes stored in the TTree if any.

class OneLepTree2nd : public AnaTree {
public:
    OneLepTree2nd(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    Double_t trigWeight_metTrig;
    Bool_t trigMatch_metTrig;
    Double_t trigWeight_singleLepTrig;
    Bool_t trigMatch_singleLepTrig;
    Int_t nLep_base;
    Int_t nLep_combiBase;
    Int_t nLep_combiBaseHighPt;
    Int_t nLep_signal;
    Float_t mu;
    Float_t actual_mu;
    Int_t AnalysisType;
    Int_t nVtx;
    Int_t nFatjets;
    Int_t nJet30;
    Int_t nBJet30_DL1;
    Float_t met;
    Float_t mt;
    Float_t mct;
    Float_t meffInc30;
    Float_t Ht30;
    Float_t lep1Pt;
    Float_t lep1Eta;
    Float_t lep1Phi;
    Int_t lep1IFFTruthClassifier;
    Float_t lep2Pt;
    Float_t lep2Eta;
    Float_t lep2Phi;
    Int_t lep2IFFTruthClassifier;
    Float_t jet1Pt;
    Float_t jet1M;
    Float_t jet1Eta;
    Float_t jet1Phi;
    Float_t jet2Pt;
    Float_t jet2Eta;
    Float_t jet2Phi;
    Float_t mll;
    Float_t mjj;
    Float_t dPhi_lep_met;
    Float_t fatjet1Pt;
    Float_t fatjet1M;
    Float_t fatjet1Eta;
    Float_t fatjet1Phi;
    Float_t fatjet1Wtagged;
    Float_t fatjet1Ztagged;
    Float_t fatjet1D2;
    Float_t fatjet1Tau32;
    Float_t fatjet1nTrk;
    Float_t fatjet2Pt;
    Float_t fatjet2M;
    Float_t met_Phi;
    Float_t met_Signif;
    Double_t pileupWeight;
    Double_t leptonWeight;
    Double_t eventWeight;
    Double_t jvtWeight;
    Double_t bTagWeight;
    Double_t genWeight;
    Double_t genWeightUp;
    Double_t genWeightDown;
    Double_t SherpaVjetsNjetsWeight;
    Double_t polWeight;
    Bool_t HLT_xe70_mht;
    Bool_t HLT_xe90_mht_L1XE50;
    Bool_t HLT_xe110_mht_L1XE50;
    Bool_t HLT_xe110_pufit_L1XE55;
    Bool_t HLT_xe110_pufit_xe70_L1XE50;
    ULong64_t PRWHash;
    ULong64_t EventNumber;
    Float_t xsec;
    Float_t GenHt;
    Float_t GenMET;
    Int_t DatasetNumber;
    Int_t RunNumber;
    Int_t RandomRunNumber;
    Int_t FS;
    // std::vector<float>   *LHE3Weights;
    // std::vector<TString> *LHE3WeightNames;

    // List of branches
    TBranch *b_trigWeight_metTrig;          //!
    TBranch *b_trigMatch_metTrig;           //!
    TBranch *b_trigWeight_singleLepTrig;    //!
    TBranch *b_trigMatch_singleLepTrig;     //!
    TBranch *b_nLep_base;                   //!
    TBranch *b_nLep_combiBase;              //!
    TBranch *b_nLep_combiBaseHighPt;        //!
    TBranch *b_nLep_signal;                 //!
    TBranch *b_mu;                          //!
    TBranch *b_actual_mu;                   //!
    TBranch *b_AnalysisType;                //!
    TBranch *b_nVtx;                        //!
    TBranch *b_nFatjets;                    //!
    TBranch *b_nJet30;                      //!
    TBranch *b_nBJet30_DL1;                 //!
    TBranch *b_met;                         //!
    TBranch *b_mt;                          //!
    TBranch *b_mct;                         //!
    TBranch *b_meffInc30;                   //!
    TBranch *b_Ht30;                        //!
    TBranch *b_lep1Pt;                      //!
    TBranch *b_lep1Eta;                     //!
    TBranch *b_lep1Phi;                     //!
    TBranch *b_lep1IFFTruthClassifier;      //!
    TBranch *b_lep2Pt;                      //!
    TBranch *b_lep2Eta;                     //!
    TBranch *b_lep2Phi;                     //!
    TBranch *b_lep2IFFTruthClassifier;      //!
    TBranch *b_jet1Pt;                      //!
    TBranch *b_jet1M;                       //!
    TBranch *b_jet1Eta;                     //!
    TBranch *b_jet1Phi;                     //!
    TBranch *b_jet2Pt;                      //!
    TBranch *b_jet2Eta;                     //!
    TBranch *b_jet2Phi;                     //!
    TBranch *b_mll;                         //!
    TBranch *b_mjj;                         //!
    TBranch *b_dPhi_lep_met;                //!
    TBranch *b_fatjet1Pt;                   //!
    TBranch *b_fatjet1M;                    //!
    TBranch *b_fatjet1Eta;                  //!
    TBranch *b_fatjet1Phi;                  //!
    TBranch *b_fatjet1Wtagged;              //!
    TBranch *b_fatjet1Ztagged;              //!
    TBranch *b_fatjet1D2;                   //!
    TBranch *b_fatjet1Tau32;                //!
    TBranch *b_fatjet1nTrk;                 //!
    TBranch *b_fatjet2Pt;                   //!
    TBranch *b_fatjet2M;                    //!
    TBranch *b_met_Phi;                     //!
    TBranch *b_met_Signif;                  //!
    TBranch *b_pileupWeight;                //!
    TBranch *b_leptonWeight;                //!
    TBranch *b_eventWeight;                 //!
    TBranch *b_jvtWeight;                   //!
    TBranch *b_bTagWeight;                  //!
    TBranch *b_genWeight;                   //!
    TBranch *b_genWeightUp;                 //!
    TBranch *b_genWeightDown;               //!
    TBranch *b_SherpaVjetsNjetsWeight;      //!
    TBranch *b_polWeight;                   //!
    TBranch *b_HLT_xe70_mht;                //!
    TBranch *b_HLT_xe90_mht_L1XE50;         //!
    TBranch *b_HLT_xe110_mht_L1XE50;        //!
    TBranch *b_HLT_xe110_pufit_L1XE55;      //!
    TBranch *b_HLT_xe110_pufit_xe70_L1XE50; //!
    TBranch *b_PRWHash;                     //!
    TBranch *b_EventNumber;                 //!
    TBranch *b_xsec;                        //!
    TBranch *b_GenHt;                       //!
    TBranch *b_GenMET;                      //!
    TBranch *b_DatasetNumber;               //!
    TBranch *b_RunNumber;                   //!
    TBranch *b_RandomRunNumber;             //!
    TBranch *b_FS;                          //!
    // TBranch        *b_LHE3Weights;   //!
    // TBranch        *b_LHE3WeightNames;   //!

    void Init(TChain *tree) {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set branch addresses and branch pointers
        fChain = tree;
        fChain->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
        fChain->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
        fChain->SetBranchAddress("trigWeight_singleLepTrig", &trigWeight_singleLepTrig, &b_trigWeight_singleLepTrig);
        fChain->SetBranchAddress("trigMatch_singleLepTrig", &trigMatch_singleLepTrig, &b_trigMatch_singleLepTrig);
        fChain->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
        fChain->SetBranchAddress("nLep_combiBase", &nLep_combiBase, &b_nLep_combiBase);
        fChain->SetBranchAddress("nLep_combiBaseHighPt", &nLep_combiBaseHighPt, &b_nLep_combiBaseHighPt);
        fChain->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
        fChain->SetBranchAddress("mu", &mu, &b_mu);
        fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
        fChain->SetBranchAddress("AnalysisType", &AnalysisType, &b_AnalysisType);
        fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
        fChain->SetBranchAddress("nFatjets", &nFatjets, &b_nFatjets);
        fChain->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
        fChain->SetBranchAddress("nBJet30_DL1", &nBJet30_DL1, &b_nBJet30_DL1);
        fChain->SetBranchAddress("met", &met, &b_met);
        fChain->SetBranchAddress("mt", &mt, &b_mt);
        fChain->SetBranchAddress("mct", &mct, &b_mct);
        fChain->SetBranchAddress("meffInc30", &meffInc30, &b_meffInc30);
        fChain->SetBranchAddress("Ht30", &Ht30, &b_Ht30);
        fChain->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
        fChain->SetBranchAddress("lep1Eta", &lep1Eta, &b_lep1Eta);
        fChain->SetBranchAddress("lep1Phi", &lep1Phi, &b_lep1Phi);
        fChain->SetBranchAddress("lep1IFFTruthClassifier", &lep1IFFTruthClassifier, &b_lep1IFFTruthClassifier);
        fChain->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
        fChain->SetBranchAddress("lep2Eta", &lep2Eta, &b_lep2Eta);
        fChain->SetBranchAddress("lep2Phi", &lep2Phi, &b_lep2Phi);
        fChain->SetBranchAddress("lep2IFFTruthClassifier", &lep2IFFTruthClassifier, &b_lep2IFFTruthClassifier);
        fChain->SetBranchAddress("jet1Pt", &jet1Pt, &b_jet1Pt);
        fChain->SetBranchAddress("jet1M", &jet1M, &b_jet1M);
        fChain->SetBranchAddress("jet1Eta", &jet1Eta, &b_jet1Eta);
        fChain->SetBranchAddress("jet1Phi", &jet1Phi, &b_jet1Phi);
        fChain->SetBranchAddress("jet2Pt", &jet2Pt, &b_jet2Pt);
        fChain->SetBranchAddress("jet2Eta", &jet2Eta, &b_jet2Eta);
        fChain->SetBranchAddress("jet2Phi", &jet2Phi, &b_jet2Phi);
        fChain->SetBranchAddress("mll", &mll, &b_mll);
        fChain->SetBranchAddress("mjj", &mjj, &b_mjj);
        fChain->SetBranchAddress("dPhi_lep_met", &dPhi_lep_met, &b_dPhi_lep_met);
        fChain->SetBranchAddress("fatjet1Pt", &fatjet1Pt, &b_fatjet1Pt);
        fChain->SetBranchAddress("fatjet1M", &fatjet1M, &b_fatjet1M);
        fChain->SetBranchAddress("fatjet1Eta", &fatjet1Eta, &b_fatjet1Eta);
        fChain->SetBranchAddress("fatjet1Phi", &fatjet1Phi, &b_fatjet1Phi);
        fChain->SetBranchAddress("fatjet1Wtagged", &fatjet1Wtagged, &b_fatjet1Wtagged);
        fChain->SetBranchAddress("fatjet1Ztagged", &fatjet1Ztagged, &b_fatjet1Ztagged);
        fChain->SetBranchAddress("fatjet1D2", &fatjet1D2, &b_fatjet1D2);
        fChain->SetBranchAddress("fatjet1Tau32", &fatjet1Tau32, &b_fatjet1Tau32);
        fChain->SetBranchAddress("fatjet1nTrk", &fatjet1nTrk, &b_fatjet1nTrk);
        fChain->SetBranchAddress("fatjet2Pt", &fatjet2Pt, &b_fatjet2Pt);
        fChain->SetBranchAddress("fatjet2M", &fatjet2M, &b_fatjet2M);
        fChain->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
        fChain->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
        fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
        fChain->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
        fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
        fChain->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
        fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
        fChain->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
        fChain->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
        fChain->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
        fChain->SetBranchAddress("SherpaVjetsNjetsWeight", &SherpaVjetsNjetsWeight, &b_SherpaVjetsNjetsWeight);
        fChain->SetBranchAddress("polWeight", &polWeight, &b_polWeight);
        fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
        fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_pufit_L1XE55", &HLT_xe110_pufit_L1XE55, &b_HLT_xe110_pufit_L1XE55);
        fChain->SetBranchAddress("HLT_xe110_pufit_xe70_L1XE50", &HLT_xe110_pufit_xe70_L1XE50, &b_HLT_xe110_pufit_xe70_L1XE50);
        fChain->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
        fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
        fChain->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
        fChain->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
        fChain->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
        fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
        fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
        fChain->SetBranchAddress("FS", &FS, &b_FS);
        // fChain->SetBranchAddress("LHE3Weights", &LHE3Weights, &b_LHE3Weights);
        // fChain->SetBranchAddress("LHE3WeightNames", &LHE3WeightNames, &b_LHE3WeightNames);
    }
};

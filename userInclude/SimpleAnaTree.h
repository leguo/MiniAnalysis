#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "PhyUtils.h"

class SimpleAnaTree : public AnaTree {
public:
    SimpleAnaTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    AnaObjs getElectrons() {
        AnaObjs eleVec;
        for (unsigned int i = 0; i < this->electrons_pt->size(); i++) {
            AnaObj ele(ELECTRON, this->electrons_charge->at(i), this->electrons_id->at(i));
            ele.SetPtEtaPhiM(this->electrons_pt->at(i), this->electrons_eta->at(i), this->electrons_phi->at(i), this->electrons_m->at(i));
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(MetTST_met, 0, MetTST_phi, MetTST_met);
            ele.setMt(PhyUtils::calcMT(ele, METVec));
            eleVec.emplace_back(ele);
        }
        return eleVec;
    }

    AnaObjs getMuons() {
        AnaObjs muonVec;
        for (unsigned int i = 0; i < this->muons_pt->size(); i++) {
            AnaObj muon(MUON, this->muons_charge->at(i), this->muons_id->at(i));
            muon.SetPtEtaPhiM(this->muons_pt->at(i), this->muons_eta->at(i), this->muons_phi->at(i), this->muons_m->at(i));
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(MetTST_met, 0, MetTST_phi, MetTST_met);
            muon.setMt(PhyUtils::calcMT(muon, METVec));
            muonVec.emplace_back(muon);
        }
        return muonVec;
    }

    AnaObjs getTaus() {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->taus_pt->size(); i++) {
            AnaObj tau(TAU, this->taus_charge->at(i), this->taus_id->at(i));
            tau.SetPtEtaPhiM(this->taus_pt->at(i), this->taus_eta->at(i), this->taus_phi->at(i), this->taus_m->at(i));
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(MetTST_met, 0, MetTST_phi, MetTST_met);
            tau.setMt(PhyUtils::calcMT(tau, METVec));
            tauVec.emplace_back(tau);
        }
        return tauVec;
    }

    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->jets_pt->size(); i++) {
            AnaObj jet(JET, 0, this->jets_id->at(i));
            jet.SetPtEtaPhiM(this->jets_pt->at(i), this->jets_eta->at(i), this->jets_phi->at(i), this->jets_m->at(i));
            jetVec.emplace_back(jet);
        }
        return jetVec;
    }

    // Declaration of leaf types
    Int_t Event;
    Double_t NormWeight;
    Float_t eventWeight;
    Int_t mcChannelNumber;
    Float_t MetTST_met;
    Float_t MetTST_phi;
    Int_t SUSYFinalState;
    std::vector<float> *electrons_pt;
    std::vector<float> *electrons_eta;
    std::vector<float> *electrons_phi;
    std::vector<float> *electrons_e;
    std::vector<int> *electrons_charge;
    std::vector<float> *electrons_m;
    std::vector<int> *electrons_type;
    std::vector<int> *electrons_id;
    std::vector<int> *electrons_motherID;
    std::vector<float> *jets_pt;
    std::vector<float> *jets_eta;
    std::vector<float> *jets_phi;
    std::vector<float> *jets_e;
    std::vector<int> *jets_charge;
    std::vector<float> *jets_m;
    std::vector<int> *jets_id;
    std::vector<int> *jets_motherID;
    std::vector<float> *muons_pt;
    std::vector<float> *muons_eta;
    std::vector<float> *muons_phi;
    std::vector<float> *muons_e;
    std::vector<int> *muons_charge;
    std::vector<float> *muons_m;
    std::vector<int> *muons_type;
    std::vector<int> *muons_id;
    std::vector<int> *muons_motherID;
    Int_t n_BJets;
    Int_t n_BaseElec;
    Int_t n_BaseJets;
    Int_t n_BaseMuon;
    Int_t n_BaseTau;
    Int_t n_SignalElec;
    Int_t n_SignalJets;
    Int_t n_SignalMuon;
    Int_t n_SignalTau;
    std::vector<float> *taus_pt;
    std::vector<float> *taus_eta;
    std::vector<float> *taus_phi;
    std::vector<float> *taus_e;
    std::vector<int> *taus_charge;
    std::vector<float> *taus_m;
    std::vector<int> *taus_type;
    std::vector<int> *taus_id;
    std::vector<int> *taus_motherID;
    Float_t preselection;
    Double_t Weight;
    Double_t ADE;
    Double_t tau1Pt;
    Double_t tau2Pt;
    Double_t tau1Mt;
    Double_t tau2Mt;
    Double_t dRtt;
    Double_t dPhitt;
    Double_t lepPt;
    Double_t mT2;
    Double_t MET_sig;
    Double_t MET;
    Double_t Mtt;
    Double_t lepMt;
    Double_t meff;
    Double_t mct;
    Int_t Njet;
    Int_t NTightTau;
    Double_t MTsum;
    Double_t METmeff;
    Double_t TwotauPt;
    Double_t dRLepTwoTau;
    Double_t dPhiLepTwoTau;
    Double_t dRt1MET;
    Double_t dRt2MET;
    Double_t dRlepMET;
    Double_t dR2TauMET;
    Double_t dPhit1MET;
    Double_t dPhit2MET;
    Double_t dPhilepMET;
    Double_t dPhi2TauMET;

    // List of branches
    TBranch *b_NormWeight;         //!
    TBranch *b_Event;              //!
    TBranch *b_eventWeight;        //!
    TBranch *b_mcChannelNumber;    //!
    TBranch *b_MetTST_met;         //!
    TBranch *b_MetTST_phi;         //!
    TBranch *b_SUSYFinalState;     //!
    TBranch *b_electrons_pt;       //!
    TBranch *b_electrons_eta;      //!
    TBranch *b_electrons_phi;      //!
    TBranch *b_electrons_e;        //!
    TBranch *b_electrons_charge;   //!
    TBranch *b_electrons_m;        //!
    TBranch *b_electrons_type;     //!
    TBranch *b_electrons_id;       //!
    TBranch *b_electrons_motherID; //!
    TBranch *b_jets_pt;            //!
    TBranch *b_jets_eta;           //!
    TBranch *b_jets_phi;           //!
    TBranch *b_jets_e;             //!
    TBranch *b_jets_charge;        //!
    TBranch *b_jets_m;             //!
    TBranch *b_jets_id;            //!
    TBranch *b_jets_motherID;      //!
    TBranch *b_muons_pt;           //!
    TBranch *b_muons_eta;          //!
    TBranch *b_muons_phi;          //!
    TBranch *b_muons_e;            //!
    TBranch *b_muons_charge;       //!
    TBranch *b_muons_m;            //!
    TBranch *b_muons_type;         //!
    TBranch *b_muons_id;           //!
    TBranch *b_muons_motherID;     //!
    TBranch *b_n_BJets;            //!
    TBranch *b_n_BaseElec;         //!
    TBranch *b_n_BaseJets;         //!
    TBranch *b_n_BaseMuon;         //!
    TBranch *b_n_BaseTau;          //!
    TBranch *b_n_SignalElec;       //!
    TBranch *b_n_SignalJets;       //!
    TBranch *b_n_SignalMuon;       //!
    TBranch *b_n_SignalTau;        //!
    TBranch *b_taus_pt;            //!
    TBranch *b_taus_eta;           //!
    TBranch *b_taus_phi;           //!
    TBranch *b_taus_e;             //!
    TBranch *b_taus_charge;        //!
    TBranch *b_taus_m;             //!
    TBranch *b_taus_type;          //!
    TBranch *b_taus_id;            //!
    TBranch *b_taus_motherID;      //!
    TBranch *b_preselection;       //!
    TBranch *b_Weight;             //!
    TBranch *b_ADE;                //!
    TBranch *b_tau1Pt;             //!
    TBranch *b_tau2Pt;             //!
    TBranch *b_tau1Mt;             //!
    TBranch *b_tau2Mt;             //!
    TBranch *b_dRtt;               //!
    TBranch *b_dPhitt;             //!
    TBranch *b_lepPt;              //!
    TBranch *b_mT2;                //!
    TBranch *b_MET_sig;            //!
    TBranch *b_MET;                //!
    TBranch *b_Mtt;                //!
    TBranch *b_lepMt;              //!
    TBranch *b_meff;               //!
    TBranch *b_mct;                //!
    TBranch *b_Njet;               //!
    TBranch *b_NTightTau;          //!
    TBranch *b_MTsum;              //!
    TBranch *b_METmeff;            //!
    TBranch *b_TwotauPt;           //!
    TBranch *b_dRLepTwoTau;        //!
    TBranch *b_dPhiLepTwoTau;      //!
    TBranch *b_dRt1MET;            //!
    TBranch *b_dRt2MET;            //!
    TBranch *b_dRlepMET;           //!
    TBranch *b_dR2TauMET;          //!
    TBranch *b_dPhit1MET;          //!
    TBranch *b_dPhit2MET;          //!
    TBranch *b_dPhilepMET;         //!
    TBranch *b_dPhi2TauMET;        //!

    void Init(TChain *tree) override {
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        ADE = 0;
        Weight = 0;
        electrons_pt = 0;
        electrons_eta = 0;
        electrons_phi = 0;
        electrons_e = 0;
        electrons_charge = 0;
        electrons_m = 0;
        electrons_type = 0;
        electrons_id = 0;
        electrons_motherID = 0;
        jets_pt = 0;
        jets_eta = 0;
        jets_phi = 0;
        jets_e = 0;
        jets_charge = 0;
        jets_m = 0;
        jets_id = 0;
        jets_motherID = 0;
        muons_pt = 0;
        muons_eta = 0;
        muons_phi = 0;
        muons_e = 0;
        muons_charge = 0;
        muons_m = 0;
        muons_type = 0;
        muons_id = 0;
        muons_motherID = 0;
        taus_pt = 0;
        taus_eta = 0;
        taus_phi = 0;
        taus_e = 0;
        taus_charge = 0;
        taus_m = 0;
        taus_type = 0;
        taus_id = 0;
        taus_motherID = 0;

        fChain = tree;

        fChain->SetBranchAddress("NormWeight", &NormWeight, &b_NormWeight);

        fChain->SetBranchAddress("Event", &Event, &b_Event);
        fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
        fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
        fChain->SetBranchAddress("MetTST_met", &MetTST_met, &b_MetTST_met);
        fChain->SetBranchAddress("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
        fChain->SetBranchAddress("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
        fChain->SetBranchAddress("electrons_pt", &electrons_pt, &b_electrons_pt);
        fChain->SetBranchAddress("electrons_eta", &electrons_eta, &b_electrons_eta);
        fChain->SetBranchAddress("electrons_phi", &electrons_phi, &b_electrons_phi);
        fChain->SetBranchAddress("electrons_e", &electrons_e, &b_electrons_e);
        fChain->SetBranchAddress("electrons_charge", &electrons_charge, &b_electrons_charge);
        fChain->SetBranchAddress("electrons_m", &electrons_m, &b_electrons_m);
        fChain->SetBranchAddress("electrons_type", &electrons_type, &b_electrons_type);
        fChain->SetBranchAddress("electrons_id", &electrons_id, &b_electrons_id);
        fChain->SetBranchAddress("electrons_motherID", &electrons_motherID, &b_electrons_motherID);
        fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
        fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
        fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
        fChain->SetBranchAddress("jets_e", &jets_e, &b_jets_e);
        fChain->SetBranchAddress("jets_charge", &jets_charge, &b_jets_charge);
        fChain->SetBranchAddress("jets_m", &jets_m, &b_jets_m);
        fChain->SetBranchAddress("jets_id", &jets_id, &b_jets_id);
        fChain->SetBranchAddress("jets_motherID", &jets_motherID, &b_jets_motherID);
        fChain->SetBranchAddress("muons_pt", &muons_pt, &b_muons_pt);
        fChain->SetBranchAddress("muons_eta", &muons_eta, &b_muons_eta);
        fChain->SetBranchAddress("muons_phi", &muons_phi, &b_muons_phi);
        fChain->SetBranchAddress("muons_e", &muons_e, &b_muons_e);
        fChain->SetBranchAddress("muons_charge", &muons_charge, &b_muons_charge);
        fChain->SetBranchAddress("muons_m", &muons_m, &b_muons_m);
        fChain->SetBranchAddress("muons_type", &muons_type, &b_muons_type);
        fChain->SetBranchAddress("muons_id", &muons_id, &b_muons_id);
        fChain->SetBranchAddress("muons_motherID", &muons_motherID, &b_muons_motherID);
        fChain->SetBranchAddress("n_BJets", &n_BJets, &b_n_BJets);
        fChain->SetBranchAddress("n_BaseElec", &n_BaseElec, &b_n_BaseElec);
        fChain->SetBranchAddress("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
        fChain->SetBranchAddress("n_BaseMuon", &n_BaseMuon, &b_n_BaseMuon);
        fChain->SetBranchAddress("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
        fChain->SetBranchAddress("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
        fChain->SetBranchAddress("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
        fChain->SetBranchAddress("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
        fChain->SetBranchAddress("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
        fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
        fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
        fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
        fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);
        fChain->SetBranchAddress("taus_charge", &taus_charge, &b_taus_charge);
        fChain->SetBranchAddress("taus_m", &taus_m, &b_taus_m);
        fChain->SetBranchAddress("taus_type", &taus_type, &b_taus_type);
        fChain->SetBranchAddress("taus_id", &taus_id, &b_taus_id);
        fChain->SetBranchAddress("taus_motherID", &taus_motherID, &b_taus_motherID);
        fChain->SetBranchAddress("preselection", &preselection, &b_preselection);
        fChain->SetBranchAddress("Weight", &Weight, &b_Weight);
        fChain->SetBranchAddress("ADE", &ADE, &b_ADE);
        fChain->SetBranchAddress("tau1Pt", &tau1Pt, &b_tau1Pt);
        fChain->SetBranchAddress("tau2Pt", &tau2Pt, &b_tau2Pt);
        fChain->SetBranchAddress("tau1Mt", &tau1Mt, &b_tau1Mt);
        fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
        fChain->SetBranchAddress("dRtt", &dRtt, &b_dRtt);
        fChain->SetBranchAddress("dPhitt", &dPhitt, &b_dPhitt);
        fChain->SetBranchAddress("lepPt", &lepPt, &b_lepPt);
        fChain->SetBranchAddress("mT2", &mT2, &b_mT2);
        fChain->SetBranchAddress("MET_sig", &MET_sig, &b_MET_sig);
        fChain->SetBranchAddress("MET", &MET, &b_MET);
        fChain->SetBranchAddress("Mtt", &Mtt, &b_Mtt);
        fChain->SetBranchAddress("lepMt", &lepMt, &b_lepMt);
        fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
        fChain->SetBranchAddress("meff", &meff, &b_meff);
        fChain->SetBranchAddress("mct", &mct, &b_mct);
        fChain->SetBranchAddress("Njet", &Njet, &b_Njet);
        fChain->SetBranchAddress("NTightTau", &NTightTau, &b_NTightTau);
        fChain->SetBranchAddress("MTsum", &MTsum, &b_MTsum);
        fChain->SetBranchAddress("METmeff", &METmeff, &b_METmeff);
        fChain->SetBranchAddress("TwotauPt", &TwotauPt, &b_TwotauPt);
        fChain->SetBranchAddress("dRLepTwoTau", &dRLepTwoTau, &b_dRLepTwoTau);
        fChain->SetBranchAddress("dPhiLepTwoTau", &dPhiLepTwoTau, &b_dPhiLepTwoTau);
        fChain->SetBranchAddress("dRt1MET", &dRt1MET, &b_dRt1MET);
        fChain->SetBranchAddress("dRt2MET", &dRt2MET, &b_dRt2MET);
        fChain->SetBranchAddress("dRlepMET", &dRlepMET, &b_dRlepMET);
        fChain->SetBranchAddress("dR2TauMET", &dR2TauMET, &b_dR2TauMET);
        fChain->SetBranchAddress("dPhit1MET", &dPhit1MET, &b_dPhit1MET);
        fChain->SetBranchAddress("dPhit2MET", &dPhit2MET, &b_dPhit2MET);
        fChain->SetBranchAddress("dPhilepMET", &dPhilepMET, &b_dPhilepMET);
        fChain->SetBranchAddress("dPhi2TauMET", &dPhi2TauMET, &b_dPhi2TauMET);
    }
};

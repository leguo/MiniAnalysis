#pragma once

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <vector>
#include "AnaObjs.h"
#include "AnaTree.h"
// Header file for the classes stored in the TTree if any.

class CompressedVBF : public AnaTree {
public :
    CompressedVBF(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
   //TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   //Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           nAjets;
   Int_t           nBasejets;
   Int_t           nFjets;
   Int_t           nCjets;
   Int_t           nVBFjets;
   Int_t           nLep_EWKCombiBaseline;
   Int_t           nLep_ZCR;
   Int_t           nLep_WCR;
   std::vector<float>   *aJets_Pt;            //36-41 vector->TvectorD
   std::vector<float>   *aJets_Eta;
   std::vector<float>   *aJets_Phi;
   std::vector<float>   *aJets_M;
   std::vector<float>   *aJets_Btagged;
   std::vector<float>   *aJets_dl1r;
   Float_t         HtVec30_allJets;
   Float_t         Ht30_allJets;
   Float_t         HtVec20_allJets;
   Float_t         Ht20_allJets;
   Bool_t          validVBFtags;
   Bool_t          validSpecJet;
   Float_t         vbfTagJet1_Pt;
   Float_t         vbfTagJet1_Eta;
   Float_t         vbfTagJet1_Phi;
   Float_t         vbfTagJet1_M;
   Float_t         vbfTagJet1_tileEnergy;
   Float_t         vbfTagJet1_DPhiMet;
   Float_t         vbfTagJet1_DPhiMet_LepInvis;
   Float_t         vbfTagJet1_minDRaJet20;
   Int_t           vbfTagJet1_Btagged;
   Float_t         vbfTagJet1_dl1r;
   Float_t         vbfTagJet1_JVT;
   Bool_t          vbfTagJet1_passOR;
   Bool_t          vbfTagJet1_passDRcut;
   Float_t         vbfTagJet2_Pt;
   Float_t         vbfTagJet2_Eta;
   Float_t         vbfTagJet2_Phi;
   Float_t         vbfTagJet2_M;
   Float_t         vbfTagJet2_tileEnergy;
   Float_t         vbfTagJet2_DPhiMet;
   Float_t         vbfTagJet2_DPhiMet_LepInvis;
   Float_t         vbfTagJet2_minDRaJet20;
   Int_t           vbfTagJet2_Btagged;
   Float_t         vbfTagJet2_dl1r;
   Float_t         vbfTagJet2_JVT;
   Bool_t          vbfTagJet2_passOR;
   Bool_t          vbfTagJet2_passDRcut;
   Float_t         vbfSpecJet_Pt;
   Float_t         vbfSpecJet_Eta;
   Float_t         vbfSpecJet_Phi;
   Float_t         vbfSpecJet_M;
   Float_t         vbfSpecJet_tileEnergy;
   Float_t         vbfSpecJet_DPhiMet;
   Float_t         vbfSpecJet_DPhiMet_LepInvis;
   Float_t         vbfSpecJet_minDRaJet20;
   Int_t           vbfSpecJet_Btagged;
   Float_t         vbfSpecJet_dl1r;
   Float_t         vbfSpecJet_JVT;
   Bool_t          vbfSpecJet_passOR;
   Bool_t          vbfSpecJet_passDRcut;
   Float_t         jetC_Pt;
   Float_t         jetC_Eta;
   Float_t         jetC_Phi;
   Float_t         jetC_M;
   Float_t         jetC_tileEnergy;
   Float_t         jetC_DPhiMet;
   Float_t         jetC_DPhiMet_LepInvis;
   Float_t         jetC_minDRaJet20;
   Int_t           jetC_Btagged;
   Float_t         jetC_dl1r;
   Float_t         jetC_JVT;
   Bool_t          jetC_passOR;
   Bool_t          jetC_passDRcut;
   Float_t         jetD_Pt;
   Float_t         jetD_Eta;
   Float_t         jetD_Phi;
   Float_t         jetD_M;
   Float_t         jetD_tileEnergy;
   Float_t         jetD_DPhiMet;
   Float_t         jetD_DPhiMet_LepInvis;
   Float_t         jetD_minDRaJet20;
   Int_t           jetD_Btagged;
   Float_t         jetD_dl1r;
   Float_t         jetD_JVT;
   Bool_t          jetD_passOR;
   Bool_t          jetD_passDRcut;
   Int_t           vbfTagJet1_Index;
   Int_t           vbfTagJet2_Index;
   Int_t           SpecJet_Index;
   Int_t           jetC_Index;
   Int_t           jetD_Index;
   Float_t         vbfjjPt;
   Float_t         vbfjjEta;
   Float_t         vbfjjPhi;
   Float_t         vbfjjM;
   Float_t         vbfjjDEta;
   Float_t         vbfjjDPhi;
   Float_t         vbfjjDR;
   Float_t         vbfEtaEta;
   Float_t         SpecJet_Cen;
   Float_t         SpecJet_ExpCen;
   Float_t         jetC_Cen;
   Float_t         jetC_ExpCen;
   Float_t         jetD_Cen;
   Float_t         jetD_ExpCen;
   Float_t         jetC_mjjrel;
   Float_t         jetD_mjjrel;
   Float_t         HTVBF;
   Float_t         HTVBFOverMET;
   Float_t         HTVBFOverMETTrack;
   Float_t         vbfjj_dphi_met;
   Float_t         vbfjj_dphi_mettrack;
   Float_t         MT_VBFMET;
   Float_t         HTVBFOverHTLep;
   Float_t         SMET;
   Float_t         SMET_LepInvis;
   Int_t           WCR_LepIndex;
   Float_t         lep1_DPhi_vbfjj;
   Float_t         lep1_DR_vbfjj;
   Float_t         lep1_CenVBF;
   Float_t         lep2_DPhi_vbfjj;
   Float_t         lep2_DR_vbfjj;
   Float_t         lep2_CenVBF;
   Float_t         lep3_DPhi_vbfjj;
   Float_t         lep3_DR_vbfjj;
   Float_t         lep3_CenVBF;
   Float_t         ZCR_ll_M;
   Float_t         ZCR_ll_Pt;
   Float_t         ZCR_ll_DPhiMet;
   Float_t         ZCR_ll_DPhiMet_LepInvis;
   Float_t         ZCR_ll_DPhi_l1l2;
   Float_t         ZCR_ll_DR_l1l2;
   Int_t           ZCR_mll_Lep1Index;
   Int_t           ZCR_mll_Lep2Index;
   Int_t           ZCR_mll_flavour;
   Float_t         met_Et_MinusBaseL1;
   Float_t         met_Et_MinusBaseL1L2;
   Float_t         met_Et_LepInvis;
   Float_t         met_Phi_LepInvis;
   Float_t         met_Signif_LepInvis;
   Float_t         vbfjj_dphi_met_LepInvis;
   Float_t         MT_VBFMET_LepInvis;
   Float_t         HTVBFOverMET_LepInvis;
   Float_t         TST_Et_LepInvis;
   Float_t         deltaPhi_MET_TST_Phi_LepInvis;
   Float_t         vbfRJR_PTVBF;
   Float_t         vbfRJR_PTS;
   Float_t         vbfRJR_RVBF;
   Float_t         vbfRJR_MS;
   Float_t         vbfRJR_MVBF;
   Float_t         vbfRJR_dphiVBFI;
   Int_t           NtruthVBFJets;
   Int_t           NtruthStatus3Leptons;
   Float_t         Truth_vbfPtV;
   Float_t         Truth_vbfjjM;
   Float_t         Truth_vbfjjDPhi;
   Float_t         Truth_j0_pt;
   Float_t         Truth_j1_pt;
   Float_t         Truth_MET;
   Int_t           truthStatus3Lepton1_pdgId;
   Float_t         truthStatus3Lepton1_Pt;
   Float_t         truthStatus3Lepton1_Eta;
   Float_t         truthStatus3Lepton1_Phi;
   Float_t         truthStatus3Lepton1_E;
   Int_t           truthStatus3Lepton2_pdgId;
   Float_t         truthStatus3Lepton2_Pt;
   Float_t         truthStatus3Lepton2_Eta;
   Float_t         truthStatus3Lepton2_Phi;
   Float_t         truthStatus3Lepton2_E;
   Int_t           truthTauFromW_n;
   std::vector<int>     *truthTauFromW_DMV;    //gl
   Int_t           jetY_Index;
   Int_t           jetZ_Index;
   Int_t           jetY_LepInvis_Index;
   Int_t           jetZ_LepInvis_Index;
   Float_t         jetY_DPhiMet;
   Float_t         jetZ_DPhiMet;
   Float_t         jetY_LepInvis_DPhiMet;
   Float_t         jetZ_LepInvis_DPhiMet;
   Float_t         jetY_MT;
   Float_t         jetZ_MT;
   Float_t         jetY_LepInvis_MT;
   Float_t         jetZ_LepInvis_MT;
   Double_t        trigWeight_metTrig;
   Bool_t          trigMatch_metTrig;
   Bool_t          higgsinoTrig2016;
   Bool_t          higgsinoTrig2017;
   Double_t        trigWeight_singleMuonTrig;
   Bool_t          trigMatch_singleMuonTrig;
   Double_t        trigWeight_singleElectronTrig;
   Bool_t          trigMatch_singleElectronTrig;
   Double_t        trigWeight_diMuonTrig;
   Bool_t          trigMatch_diMuonTrig;
   Double_t        trigWeight_diElectronTrig;
   Bool_t          trigMatch_diElectronTrig;
   Bool_t          passEmulTightJetCleaning1Jet;
   Bool_t          passEmulTightJetCleaning2Jet;
   Bool_t          passBadTileJetVeto;
   Bool_t          HLT_2mu4_j85_xe50_mht_emul;
   Bool_t          HLT_mu4_j125_xe90_mht_emul;
   Float_t         L1_met_Et;
   Float_t         L1_jet1Pt;
   Float_t         HLT_met_pufit_Et;
   Float_t         HLT_met_cell_Et;
   Float_t         HLT_jet1Pt;
   Float_t         mu;
   Float_t         avg_mu;
   Float_t         actual_mu;
   Int_t           nVtx;
   Int_t           nLep_base;
   Int_t           nLep_signal;
   Int_t           nEle_base;
   Int_t           nEle_signal;
   Int_t           nMu_base;
   Int_t           nMu_signal;
   Int_t           nLep_signal_PLTLooseLoose;
   Int_t           nTau_base;
   Int_t           nTau_signal;
   Bool_t          tau1Loose;
   Bool_t          tau1Medium;
   Bool_t          tau1Tight;
   Float_t         tau1Pt;
   Float_t         tau1Eta;
   Float_t         tau1Phi;
   Float_t         tau1M;
   Int_t           tau1NTracks;
   Bool_t          tau2Loose;
   Bool_t          tau2Medium;
   Bool_t          tau2Tight;
   Float_t         tau2Pt;
   Float_t         tau2Eta;
   Float_t         tau2Phi;
   Float_t         tau2M;
   Int_t           tau2NTracks;
   Int_t           nLep_signal_PLTLooseTight;
   Int_t           nLep_signal_PLTTightLoose;
   Int_t           nLep_signal_PLTTightTight;
   Int_t           lep1Flavor;
   Int_t           lep1Charge;
   Int_t           lep1Author;
   Float_t         lep1Pt;
   Float_t         lep1Eta;
   Float_t         lep1Phi;
   Float_t         lep1M;
   Float_t         lep1MT_Met;
   Float_t         lep1MT_Met_LepInvis;
   Float_t         lep1_DPhiMet;
   Float_t         lep1_DPhiMet_LepInvis;
   Float_t         lep1D0;
   Float_t         lep1D0Sig;
   Float_t         lep1Z0;
   Float_t         lep1Z0SinTheta;
   Float_t         lep1Topoetcone20;
   Float_t         lep1Topoetcone30;
   Float_t         lep1Topoetcone40;
   Float_t         lep1Ptvarcone20;
   Float_t         lep1Ptvarcone30;
   Float_t         lep1Ptvarcone40;
   Float_t         lep1CorrTopoetcone20;
   Float_t         lep1CorrTopoetcone30;
   Float_t         lep1CorrTopoetcone40;
   Float_t         lep1CorrPtvarcone20;
   Float_t         lep1CorrPtvarcone30;
   Float_t         lep1CorrPtvarcone40;
   Bool_t          lep1PassOR;
   Int_t           lep1Type;
   Int_t           lep1Origin;
   Int_t           lep1EgMotherType;
   Int_t           lep1EgMotherOrigin;
   Int_t           lep1NPix;
   Bool_t          lep1PassBL;
   Bool_t          lep1baseline;
   Bool_t          lep1VeryLoose;
   Bool_t          lep1Loose;
   Bool_t          lep1Medium;
   Bool_t          lep1Tight;
   Bool_t          lep1LowPtWP;
   Bool_t          lep1LooseBLllh;
   Bool_t          lep1IsoHighPtCaloOnly;
   Bool_t          lep1IsoHighPtTrackOnly;
   Bool_t          lep1corr_IsoHighPtCaloOnly;
   Bool_t          lep1IsoFCHighPtCaloOnly;
   Bool_t          lep1IsoFCTightTrackOnly;
   Bool_t          lep1IsoFCLoose;
   Bool_t          lep1IsoFCTight;
   Bool_t          lep1IsoFCLoose_FixedRad;
   Bool_t          lep1IsoFCTight_FixedRad;
   Bool_t          lep1IsoLoose_VarRad;
   Bool_t          lep1IsoLoose_FixedRad;
   Bool_t          lep1IsoTight_VarRad;
   Bool_t          lep1IsoTight_FixedRad;
   Bool_t          lep1IsoTightTrackOnly_VarRad;
   Bool_t          lep1IsoTightTrackOnly_FixedRad;
   Bool_t          lep1IsoPflowLoose_VarRad;
   Bool_t          lep1IsoPflowLoose_FixedRad;
   Bool_t          lep1IsoPflowTight_VarRad;
   Bool_t          lep1IsoPflowTight_FixedRad;
   Bool_t          lep1IsoPLVLoose;
   Bool_t          lep1IsoPLVTight;
   Bool_t          lep1Signal;
   Bool_t          lep1SignalPLTLoose;
   Bool_t          lep1SignalPLTTight;
   Bool_t          lep1TruthMatched;
   Int_t           lep1TruthCharge;
   Float_t         lep1TruthPt;
   Float_t         lep1TruthEta;
   Float_t         lep1TruthPhi;
   Float_t         lep1TruthM;
   Float_t         lep1TrackPt;
   Float_t         lep1TrackEta;
   Float_t         lep1TrackPhi;
   Double_t        lep1Weight;
   Float_t         lep1PromptLepVeto_score;
   Float_t         lep1PLTInput_TrackJetNTrack;
   Float_t         lep1PLTInput_DRlj;
   Float_t         lep1PLTInput_rnnip;
   Float_t         lep1PLTInput_DL1mu;
   Float_t         lep1PLTInput_PtRel;
   Float_t         lep1PLTInput_PtFrac;
   Float_t         lep1CorrPLTInput_TrackJetNTrack;
   Float_t         lep1CorrPLTInput_DRlj;
   Float_t         lep1CorrPLTInput_rnnip;
   Float_t         lep1CorrPLTInput_DL1mu;
   Float_t         lep1CorrPLTInput_PtRel;
   Float_t         lep1CorrPLTInput_PtFrac;
   Float_t         lep1CorrPromptLepVeto;
   Float_t         lep1LowPtPLT;
   Float_t         lep1LowPtPLT_Cone40;
   Bool_t          lep1IsoLowPtPLT_Loose;
   Bool_t          lep1IsoLowPtPLT_Tight;
   Bool_t          lep1IsoLowPtPLT_MediumLH_Loose;
   Bool_t          lep1IsoLowPtPLT_MediumLH_Tight;
   Bool_t          lep1EWKCombiBaseline;
   Bool_t          lep1PassZCR;
   Bool_t          lep1PassWCR;
   Int_t           lep2Flavor;
   Int_t           lep2Charge;
   Int_t           lep2Author;
   Float_t         lep2Pt;
   Float_t         lep2Eta;
   Float_t         lep2Phi;
   Float_t         lep2M;
   Float_t         lep2MT_Met;
   Float_t         lep2MT_Met_LepInvis;
   Float_t         lep2_DPhiMet;
   Float_t         lep2_DPhiMet_LepInvis;
   Float_t         lep2D0;
   Float_t         lep2D0Sig;
   Float_t         lep2Z0;
   Float_t         lep2Z0SinTheta;
   Float_t         lep2Topoetcone20;
   Float_t         lep2Topoetcone30;
   Float_t         lep2Topoetcone40;
   Float_t         lep2Ptvarcone20;
   Float_t         lep2Ptvarcone30;
   Float_t         lep2Ptvarcone40;
   Float_t         lep2CorrTopoetcone20;
   Float_t         lep2CorrTopoetcone30;
   Float_t         lep2CorrTopoetcone40;
   Float_t         lep2CorrPtvarcone20;
   Float_t         lep2CorrPtvarcone30;
   Float_t         lep2CorrPtvarcone40;
   Bool_t          lep2PassOR;
   Int_t           lep2Type;
   Int_t           lep2Origin;
   Int_t           lep2EgMotherType;
   Int_t           lep2EgMotherOrigin;
   Int_t           lep2NPix;
   Bool_t          lep2PassBL;
   Bool_t          lep2baseline;
   Bool_t          lep2VeryLoose;
   Bool_t          lep2Loose;
   Bool_t          lep2Medium;
   Bool_t          lep2Tight;
   Bool_t          lep2LowPtWP;
   Bool_t          lep2LooseBLllh;
   Bool_t          lep2IsoHighPtCaloOnly;
   Bool_t          lep2IsoHighPtTrackOnly;
   Bool_t          lep2corr_IsoHighPtCaloOnly;
   Bool_t          lep2IsoFCHighPtCaloOnly;
   Bool_t          lep2IsoFCTightTrackOnly;
   Bool_t          lep2IsoFCLoose;
   Bool_t          lep2IsoFCTight;
   Bool_t          lep2IsoFCLoose_FixedRad;
   Bool_t          lep2IsoFCTight_FixedRad;
   Bool_t          lep2IsoLoose_VarRad;
   Bool_t          lep2IsoLoose_FixedRad;
   Bool_t          lep2IsoTight_VarRad;
   Bool_t          lep2IsoTight_FixedRad;
   Bool_t          lep2IsoTightTrackOnly_VarRad;
   Bool_t          lep2IsoTightTrackOnly_FixedRad;
   Bool_t          lep2IsoPflowLoose_VarRad;
   Bool_t          lep2IsoPflowLoose_FixedRad;
   Bool_t          lep2IsoPflowTight_VarRad;
   Bool_t          lep2IsoPflowTight_FixedRad;
   Bool_t          lep2IsoPLVLoose;
   Bool_t          lep2IsoPLVTight;
   Bool_t          lep2Signal;
   Bool_t          lep2SignalPLTLoose;
   Bool_t          lep2SignalPLTTight;
   Bool_t          lep2TruthMatched;
   Int_t           lep2TruthCharge;
   Float_t         lep2TruthPt;
   Float_t         lep2TruthEta;
   Float_t         lep2TruthPhi;
   Float_t         lep2TruthM;
   Float_t         lep2TrackPt;
   Float_t         lep2TrackEta;
   Float_t         lep2TrackPhi;
   Double_t        lep2Weight;
   Float_t         lep2PromptLepVeto_score;
   Float_t         lep2PLTInput_TrackJetNTrack;
   Float_t         lep2PLTInput_DRlj;
   Float_t         lep2PLTInput_rnnip;
   Float_t         lep2PLTInput_DL1mu;
   Float_t         lep2PLTInput_PtRel;
   Float_t         lep2PLTInput_PtFrac;
   Float_t         lep2CorrPLTInput_TrackJetNTrack;
   Float_t         lep2CorrPLTInput_DRlj;
   Float_t         lep2CorrPLTInput_rnnip;
   Float_t         lep2CorrPLTInput_DL1mu;
   Float_t         lep2CorrPLTInput_PtRel;
   Float_t         lep2CorrPLTInput_PtFrac;
   Float_t         lep2CorrPromptLepVeto;
   Float_t         lep2LowPtPLT;
   Float_t         lep2LowPtPLT_Cone40;
   Bool_t          lep2IsoLowPtPLT_Loose;
   Bool_t          lep2IsoLowPtPLT_Tight;
   Bool_t          lep2IsoLowPtPLT_MediumLH_Loose;
   Bool_t          lep2IsoLowPtPLT_MediumLH_Tight;
   Bool_t          lep2EWKCombiBaseline;
   Bool_t          lep2PassZCR;
   Bool_t          lep2PassWCR;
   Int_t           lep3Flavor;
   Int_t           lep3Charge;
   Int_t           lep3Author;
   Float_t         lep3Pt;
   Float_t         lep3Eta;
   Float_t         lep3Phi;
   Float_t         lep3M;
   Float_t         lep3MT_Met;
   Float_t         lep3MT_Met_LepInvis;
   Float_t         lep3_DPhiMet;
   Float_t         lep3_DPhiMet_LepInvis;
   Float_t         lep3D0;
   Float_t         lep3D0Sig;
   Float_t         lep3Z0;
   Float_t         lep3Z0SinTheta;
   Float_t         lep3Topoetcone20;
   Float_t         lep3Topoetcone30;
   Float_t         lep3Topoetcone40;
   Float_t         lep3Ptvarcone20;
   Float_t         lep3Ptvarcone30;
   Float_t         lep3Ptvarcone40;
   Float_t         lep3CorrTopoetcone20;
   Float_t         lep3CorrTopoetcone30;
   Float_t         lep3CorrTopoetcone40;
   Float_t         lep3CorrPtvarcone20;
   Float_t         lep3CorrPtvarcone30;
   Float_t         lep3CorrPtvarcone40;
   Bool_t          lep3PassOR;
   Int_t           lep3Type;
   Int_t           lep3Origin;
   Int_t           lep3EgMotherType;
   Int_t           lep3EgMotherOrigin;
   Int_t           lep3NPix;
   Bool_t          lep3PassBL;
   Bool_t          lep3baseline;
   Bool_t          lep3VeryLoose;
   Bool_t          lep3Loose;
   Bool_t          lep3Medium;
   Bool_t          lep3Tight;
   Bool_t          lep3LowPtWP;
   Bool_t          lep3LooseBLllh;
   Bool_t          lep3IsoHighPtCaloOnly;
   Bool_t          lep3IsoHighPtTrackOnly;
   Bool_t          lep3corr_IsoHighPtCaloOnly;
   Bool_t          lep3IsoFCHighPtCaloOnly;
   Bool_t          lep3IsoFCTightTrackOnly;
   Bool_t          lep3IsoFCLoose;
   Bool_t          lep3IsoFCTight;
   Bool_t          lep3IsoFCLoose_FixedRad;
   Bool_t          lep3IsoFCTight_FixedRad;
   Bool_t          lep3IsoLoose_VarRad;
   Bool_t          lep3IsoLoose_FixedRad;
   Bool_t          lep3IsoTight_VarRad;
   Bool_t          lep3IsoTight_FixedRad;
   Bool_t          lep3IsoTightTrackOnly_VarRad;
   Bool_t          lep3IsoTightTrackOnly_FixedRad;
   Bool_t          lep3IsoPflowLoose_VarRad;
   Bool_t          lep3IsoPflowLoose_FixedRad;
   Bool_t          lep3IsoPflowTight_VarRad;
   Bool_t          lep3IsoPflowTight_FixedRad;
   Bool_t          lep3IsoPLVLoose;
   Bool_t          lep3IsoPLVTight;
   Bool_t          lep3Signal;
   Bool_t          lep3SignalPLTLoose;
   Bool_t          lep3SignalPLTTight;
   Bool_t          lep3TruthMatched;
   Int_t           lep3TruthCharge;
   Float_t         lep3TruthPt;
   Float_t         lep3TruthEta;
   Float_t         lep3TruthPhi;
   Float_t         lep3TruthM;
   Float_t         lep3TrackPt;
   Float_t         lep3TrackEta;
   Float_t         lep3TrackPhi;
   Double_t        lep3Weight;
   Float_t         lep3PromptLepVeto_score;
   Float_t         lep3PLTInput_TrackJetNTrack;
   Float_t         lep3PLTInput_DRlj;
   Float_t         lep3PLTInput_rnnip;
   Float_t         lep3PLTInput_DL1mu;
   Float_t         lep3PLTInput_PtRel;
   Float_t         lep3PLTInput_PtFrac;
   Float_t         lep3CorrPLTInput_TrackJetNTrack;
   Float_t         lep3CorrPLTInput_DRlj;
   Float_t         lep3CorrPLTInput_rnnip;
   Float_t         lep3CorrPLTInput_DL1mu;
   Float_t         lep3CorrPLTInput_PtRel;
   Float_t         lep3CorrPLTInput_PtFrac;
   Float_t         lep3CorrPromptLepVeto;
   Float_t         lep3LowPtPLT;
   Float_t         lep3LowPtPLT_Cone40;
   Bool_t          lep3IsoLowPtPLT_Loose;
   Bool_t          lep3IsoLowPtPLT_Tight;
   Bool_t          lep3IsoLowPtPLT_MediumLH_Loose;
   Bool_t          lep3IsoLowPtPLT_MediumLH_Tight;
   Bool_t          lep3EWKCombiBaseline;
   Bool_t          lep3PassZCR;
   Bool_t          lep3PassWCR;
   Int_t           nJet30;
   Int_t           nJet25;
   Int_t           nJet20;
   Int_t           nBJet30;
   Int_t           nBJet25;
   Int_t           nBJet20;
   Int_t           nTotalJet;
   Int_t           nTotalJet20;
   Int_t           DecayModeTTbar;
   Float_t         met_Et;
   Float_t         met_Phi;
   Float_t         met_Signif;
   Float_t         TST_Et;
   Float_t         TST_Phi;
   Float_t         met_track_Et;
   Float_t         met_track_Phi;
   Float_t         deltaPhi_MET_TST_Phi;
   Float_t         met_muons_invis_Et;
   Float_t         met_muons_invis_Phi;
   Float_t         meffInc30;
   Float_t         Ht30;
   Float_t         LepAplanarity;
   Float_t         JetAplanarity;
   Float_t         mjj;
   Float_t         dEtajj;
   Float_t         dPhijj;
   Float_t         mt_lep1;
   Float_t         mt_lep2;
   Float_t         mt_lep3;
   Float_t         mt_lep1_metTrack;
   Float_t         mt_lep2_metTrack;
   Float_t         mt_lep3_metTrack;
   Float_t         METOverHT;
   Float_t         METOverJ1pT;
   Float_t         METTrackOverHT;
   Float_t         METTrackOverJ1pT;
   Float_t         DPhiJ1Met;
   Float_t         DPhiJ2Met;
   Float_t         DPhiJ3Met;
   Float_t         DPhiJ4Met;
   Float_t         minDPhiCenJetsMet;
   Float_t         minDPhiAllJetsMet;
   Float_t         minDPhiAllJetsMet_LepInvis;
   Float_t         DPhiJ1MetTrack;
   Float_t         DPhiJ2MetTrack;
   Float_t         DPhiJ3MetTrack;
   Float_t         DPhiJ4MetTrack;
   Float_t         minDPhiCenJetsMetTrack;
   Float_t         METOverHTLep;
   Float_t         METTrackOverHTLep;
   Float_t         mll;
   Float_t         Rll;
   Float_t         Ptll;
   Float_t         dPhiPllMet;
   Float_t         dPhiPllMetTrack;
   Float_t         METRel;
   Float_t         METTrackRel;
   Float_t         dPhiNearMet;
   Float_t         dPhiNearMetTrack;
   Float_t         dPhiMetAndMetTrack;
   Float_t         MTauTau;
   Float_t         MTauTau_metTrack;
   Float_t         RjlOverEl;
   Float_t         LepCosThetaLab;
   Float_t         LepCosThetaCoM;
   Float_t         mt2leplsp_0;
   Float_t         mt2leplsp_50;
   Float_t         mt2leplsp_100;
   Float_t         mt2leplsp_150;
   Float_t         mt2leplsp_200;
   Float_t         mt2leplsp_300;
   Float_t         mt2leplsp_0_metTrack;
   Float_t         mt2leplsp_50_metTrack;
   Float_t         mt2leplsp_100_metTrack;
   Float_t         mt2leplsp_150_metTrack;
   Float_t         mt2leplsp_200_metTrack;
   Float_t         mt2leplsp_300_metTrack;
   Int_t           nSFOS;
   Float_t         mt_minMll;
   Float_t         minMll;
   Float_t         minRll;
   Float_t         m3l;
   Float_t         pT3l;
   Float_t         dPhi3lMet;
   Float_t         ptZ_minMll;
   Float_t         ptW_minMll;
   Float_t         ptWlep_minMll;
   Float_t         RZlep_minMll;
   Float_t         RZWlep_minMll;
   Float_t         dPhiZMet_minMll;
   Float_t         dPhiWLepMet_minMll;
   Float_t         mt2WZlsp_0;
   Float_t         mt2WZlsp_100;
   Double_t        pileupWeight;
   Double_t        leptonWeight;
   Double_t        eventWeight;
   Double_t        genWeight;
   Double_t        bTagWeight;
   Double_t        jvtWeight;
   Double_t        genWeightUp;
   Double_t        genWeightDown;
   Double_t        triggerWeight;
   Double_t        totalWeight;
   Double_t        truthMll;
   Double_t        winoBinoMllWeight;
   Double_t        winoBinoXsecWeight;
   Double_t        winoBinoBrFracWeight;
   Double_t        winoBinoWeight;
   Double_t        NUHM2weight_350m12;
   Double_t        NUHM2weight_400m12;
   Double_t        NUHM2weight_500m12;
   Double_t        NUHM2weight_600m12;
   Double_t        NUHM2weight_700m12;
   Double_t        NUHM2weight_800m12;
   Double_t        FFWeight;
   Int_t           nLep_antiID;
   Bool_t          lep1AntiID;
   Bool_t          lep2AntiID;
   Bool_t          lep3AntiID;
   Int_t           nLep_signalActual;
   Bool_t          lep1SignalActual;
   Bool_t          lep2SignalActual;
   Bool_t          lep3SignalActual;
   Double_t        ETMissTrigSF;
   Bool_t          HLT_mu4;
   Bool_t          HLT_2mu4;
   Bool_t          HLT_2mu10;
   Bool_t          HLT_2mu4_j85_xe50_mht;
   Bool_t          HLT_mu4_j125_xe90_mht;
   Bool_t          HLT_xe70;
   Bool_t          HLT_xe70_mht;
   Bool_t          HLT_xe70_mht_wEFMu;
   Bool_t          HLT_xe70_tc_lcw;
   Bool_t          HLT_xe70_tc_lcw_wEFMu;
   Bool_t          HLT_xe80_tc_lcw_L1XE50;
   Bool_t          HLT_xe90_tc_lcw_L1XE50;
   Bool_t          HLT_xe90_mht_L1XE50;
   Bool_t          HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Bool_t          HLT_xe90_mht_wEFMu_L1XE50;
   Bool_t          HLT_xe100_L1XE50;
   Bool_t          HLT_xe100_wEFMu_L1XE50;
   Bool_t          HLT_xe100_tc_lcw_L1XE50;
   Bool_t          HLT_xe100_mht_L1XE50;
   Bool_t          HLT_xe100_tc_lcw_wEFMu_L1XE50;
   Bool_t          HLT_xe100_mht_wEFMu_L1XE50;
   Bool_t          HLT_xe110_L1XE50;
   Bool_t          HLT_xe110_tc_em_L1XE50;
   Bool_t          HLT_xe110_wEFMu_L1XE50;
   Bool_t          HLT_xe110_tc_em_wEFMu_L1XE50;
   Bool_t          HLT_xe110_mht_L1XE50;
   Bool_t          HLT_xe90_mht_L1XE40;
   Bool_t          HLT_xe50_mht_L1XE20;
   Bool_t          HLT_xe110_pufit_xe65_L1XE50;
   Bool_t          HLT_xe110_pufit_xe70_L1XE50;
   Bool_t          HLT_j85_L1J40;
   Bool_t          HLT_j125_L1J50;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          HLT_e60_lhmedium_nod0;
   Bool_t          HLT_e60_medium;
   Bool_t          HLT_e140_lhloose_nod0;
   Bool_t          HLT_mu26_ivarmedium;
   Bool_t          HLT_e5_lhvloose_nod0;
   Bool_t          HLT_2e5_lhvloose_nod0;
   Bool_t          HLT_e5_lhloose_nod0;
   //Bool_t          HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30;
   //Bool_t          HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30;
   //Bool_t          HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE40_DPHI-J20s2XE30;
   //Bool_t          HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50;
   //Bool_t          HLT_2mu4_invm1_j20_xe80_pufit_2dphi10_L12MU4_J40_XE50;
   //Bool_t          HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30;
   //Bool_t          HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60;
   //Bool_t          HLT_mu4_j80_xe80_pufit_2dphi10_L1MU4_XE60;
   //Bool_t          HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;
   //Bool_t          HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60;
   //Bool_t          HLT_2e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60;
   //Bool_t          HLT_2e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;
   //Bool_t          HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30;
   //Bool_t          HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;
   //Bool_t          HLT_e5_lhloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30;
   //Bool_t          HLT_e5_lhmedium_nod0_j50_xe80_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;
   //Bool_t          HLT_e5_lhmedium_nod0_mu4_j30_xe65_pufit_2dphi10_L1MU4_XE60;
   //Bool_t          HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60;
   //Bool_t          HLT_e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60;
   //Bool_t          HLT_e5_lhmedium_nod0_j50_xe90_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50;
   //Bool_t          HLT_mu4_j100_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30;
   //Bool_t          HLT_mu4_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30;
   //Bool_t          HLT_2mu4_xe40mht_L12MU4_J20_XE30_DPHI-J20sXE30;
   //Bool_t          HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30;
   //Bool_t          HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50;
   Bool_t          HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul;
   Bool_t          HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul;
   Bool_t          HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul;
   Bool_t          HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul;
   Bool_t          HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul;
   Bool_t          HLT_2dphi10_emul;
   Bool_t          HLT_2mu4_invm1_emul;
   Bool_t          HLT_xe90_pufit_L1XE50;
   Bool_t          HLT_xe100_pufit_L1XE50;
   Bool_t          HLT_xe100_pufit_L1XE55;
   Bool_t          HLT_xe110_pufit_L1XE50;
   Bool_t          HLT_xe110_pufit_L1XE55;
   Bool_t          HLT_j20;
   Bool_t          HLT_j40;
   Bool_t          HLT_j30;
   Bool_t          HLT_j50;
   Bool_t          HLT_j90;
   Bool_t          L1_DPHI_2J20XE_emul;
   Bool_t          L1_MU4;
   Bool_t          L1_2MU4;
   Bool_t          L1_J20;
   Bool_t          L1_J30;
   Bool_t          L1_J40;
   Bool_t          L1_J50;
   Bool_t          L1_XE30;
   Bool_t          L1_XE40;
   Bool_t          L1_XE50;
   Float_t         x1;
   Float_t         x2;
   Float_t         pdf1;
   Float_t         pdf2;
   Float_t         scalePDF;
   Int_t           id1;
   Int_t           id2;
   Double_t        RJR_PTISR;
   Double_t        RJR_RISR;
   Double_t        RJR_MS;
   Double_t        RJR_MISR;
   Double_t        RJR_dphiISRI;
   Int_t           RJR_NjV;
   Int_t           RJR_NjISR;
   Double_t        SUSYPt;
   Double_t        ISRWeight;
   ULong64_t       PRWHash;
   ULong64_t       EventNumber;
   Float_t         xsec;
   Float_t         GenHt;
   Float_t         GenMET;
   Int_t           DatasetNumber;
   Int_t           RunNumber;
   Int_t           RandomRunNumber;
   Int_t           FS;
   std::vector<float>   *LHE3Weights;      //gl
   std::vector<TString> *LHE3WeightNames;   //gl

   // List of branches
   TBranch        *b_nAjets;   //!
   TBranch        *b_nBasejets;   //!
   TBranch        *b_nFjets;   //!
   TBranch        *b_nCjets;   //!
   TBranch        *b_nVBFjets;   //!
   TBranch        *b_nLep_EWKCombiBaseline;   //!
   TBranch        *b_nLep_ZCR;   //!
   TBranch        *b_nLep_WCR;   //!
   TBranch        *b_aJets_Pt;   //!
   TBranch        *b_aJets_Eta;   //!
   TBranch        *b_aJets_Phi;   //!
   TBranch        *b_aJets_M;   //!
   TBranch        *b_aJets_Btagged;   //!
   TBranch        *b_aJets_dl1r;   //!
   TBranch        *b_HtVec30_allJets;   //!
   TBranch        *b_Ht30_allJets;   //!
   TBranch        *b_HtVec20_allJets;   //!
   TBranch        *b_Ht20_allJets;   //!
   TBranch        *b_validVBFtags;   //!
   TBranch        *b_validSpecJet;   //!
   TBranch        *b_vbfTagJet1_Pt;   //!
   TBranch        *b_vbfTagJet1_Eta;   //!
   TBranch        *b_vbfTagJet1_Phi;   //!
   TBranch        *b_vbfTagJet1_M;   //!
   TBranch        *b_vbfTagJet1_tileEnergy;   //!
   TBranch        *b_vbfTagJet1_DPhiMet;   //!
   TBranch        *b_vbfTagJet1_DPhiMet_LepInvis;   //!
   TBranch        *b_vbfTagJet1_minDRaJet20;   //!
   TBranch        *b_vbfTagJet1_Btagged;   //!
   TBranch        *b_vbfTagJet1_dl1r;   //!
   TBranch        *b_vbfTagJet1_JVT;   //!
   TBranch        *b_vbfTagJet1_passOR;   //!
   TBranch        *b_vbfTagJet1_passDRcut;   //!
   TBranch        *b_vbfTagJet2_Pt;   //!
   TBranch        *b_vbfTagJet2_Eta;   //!
   TBranch        *b_vbfTagJet2_Phi;   //!
   TBranch        *b_vbfTagJet2_M;   //!
   TBranch        *b_vbfTagJet2_tileEnergy;   //!
   TBranch        *b_vbfTagJet2_DPhiMet;   //!
   TBranch        *b_vbfTagJet2_DPhiMet_LepInvis;   //!
   TBranch        *b_vbfTagJet2_minDRaJet20;   //!
   TBranch        *b_vbfTagJet2_Btagged;   //!
   TBranch        *b_vbfTagJet2_dl1r;   //!
   TBranch        *b_vbfTagJet2_JVT;   //!
   TBranch        *b_vbfTagJet2_passOR;   //!
   TBranch        *b_vbfTagJet2_passDRcut;   //!
   TBranch        *b_vbfSpecJet_Pt;   //!
   TBranch        *b_vbfSpecJet_Eta;   //!
   TBranch        *b_vbfSpecJet_Phi;   //!
   TBranch        *b_vbfSpecJet_M;   //!
   TBranch        *b_vbfSpecJet_tileEnergy;   //!
   TBranch        *b_vbfSpecJet_DPhiMet;   //!
   TBranch        *b_vbfSpecJet_DPhiMet_LepInvis;   //!
   TBranch        *b_vbfSpecJet_minDRaJet20;   //!
   TBranch        *b_vbfSpecJet_Btagged;   //!
   TBranch        *b_vbfSpecJet_dl1r;   //!
   TBranch        *b_vbfSpecJet_JVT;   //!
   TBranch        *b_vbfSpecJet_passOR;   //!
   TBranch        *b_vbfSpecJet_passDRcut;   //!
   TBranch        *b_jetC_Pt;   //!
   TBranch        *b_jetC_Eta;   //!
   TBranch        *b_jetC_Phi;   //!
   TBranch        *b_jetC_M;   //!
   TBranch        *b_jetC_tileEnergy;   //!
   TBranch        *b_jetC_DPhiMet;   //!
   TBranch        *b_jetC_DPhiMet_LepInvis;   //!
   TBranch        *b_jetC_minDRaJet20;   //!
   TBranch        *b_jetC_Btagged;   //!
   TBranch        *b_jetC_dl1r;   //!
   TBranch        *b_jetC_JVT;   //!
   TBranch        *b_jetC_passOR;   //!
   TBranch        *b_jetC_passDRcut;   //!
   TBranch        *b_jetD_Pt;   //!
   TBranch        *b_jetD_Eta;   //!
   TBranch        *b_jetD_Phi;   //!
   TBranch        *b_jetD_M;   //!
   TBranch        *b_jetD_tileEnergy;   //!
   TBranch        *b_jetD_DPhiMet;   //!
   TBranch        *b_jetD_DPhiMet_LepInvis;   //!
   TBranch        *b_jetD_minDRaJet20;   //!
   TBranch        *b_jetD_Btagged;   //!
   TBranch        *b_jetD_dl1r;   //!
   TBranch        *b_jetD_JVT;   //!
   TBranch        *b_jetD_passOR;   //!
   TBranch        *b_jetD_passDRcut;   //!
   TBranch        *b_vbfTagJet1_Index;   //!
   TBranch        *b_vbfTagJet2_Index;   //!
   TBranch        *b_SpecJet_Index;   //!
   TBranch        *b_jetC_Index;   //!
   TBranch        *b_jetD_Index;   //!
   TBranch        *b_vbfjjPt;   //!
   TBranch        *b_vbfjjEta;   //!
   TBranch        *b_vbfjjPhi;   //!
   TBranch        *b_vbfjjM;   //!
   TBranch        *b_vbfjjDEta;   //!
   TBranch        *b_vbfjjDPhi;   //!
   TBranch        *b_vbfjjDR;   //!
   TBranch        *b_vbfEtaEta;   //!
   TBranch        *b_SpecJet_Cen;   //!
   TBranch        *b_SpecJet_ExpCen;   //!
   TBranch        *b_jetC_Cen;   //!
   TBranch        *b_jetC_ExpCen;   //!
   TBranch        *b_jetD_Cen;   //!
   TBranch        *b_jetD_ExpCen;   //!
   TBranch        *b_jetC_mjjrel;   //!
   TBranch        *b_jetD_mjjrel;   //!
   TBranch        *b_HTVBF;   //!
   TBranch        *b_HTVBFOverMET;   //!
   TBranch        *b_HTVBFOverMETTrack;   //!
   TBranch        *b_vbfjj_dphi_met;   //!
   TBranch        *b_vbfjj_dphi_mettrack;   //!
   TBranch        *b_MT_VBFMET;   //!
   TBranch        *b_HTVBFOverHTLep;   //!
   TBranch        *b_SMET;   //!
   TBranch        *b_SMET_LepInvis;   //!
   TBranch        *b_WCR_LepIndex;   //!
   TBranch        *b_lep1_DPhi_vbfjj;   //!
   TBranch        *b_lep1_DR_vbfjj;   //!
   TBranch        *b_lep1_CenVBF;   //!
   TBranch        *b_lep2_DPhi_vbfjj;   //!
   TBranch        *b_lep2_DR_vbfjj;   //!
   TBranch        *b_lep2_CenVBF;   //!
   TBranch        *b_lep3_DPhi_vbfjj;   //!
   TBranch        *b_lep3_DR_vbfjj;   //!
   TBranch        *b_lep3_CenVBF;   //!
   TBranch        *b_ZCR_ll_M;   //!
   TBranch        *b_ZCR_ll_Pt;   //!
   TBranch        *b_ZCR_ll_DPhiMet;   //!
   TBranch        *b_ZCR_ll_DPhiMet_LepInvis;   //!
   TBranch        *b_ZCR_ll_DPhi_l1l2;   //!
   TBranch        *b_ZCR_ll_DR_l1l2;   //!
   TBranch        *b_ZCR_mll_Lep1Index;   //!
   TBranch        *b_ZCR_mll_Lep2Index;   //!
   TBranch        *b_ZCR_mll_flavour;   //!
   TBranch        *b_met_Et_MinusBaseL1;   //!
   TBranch        *b_met_Et_MinusBaseL1L2;   //!
   TBranch        *b_met_Et_LepInvis;   //!
   TBranch        *b_met_Phi_LepInvis;   //!
   TBranch        *b_met_Signif_LepInvis;   //!
   TBranch        *b_vbfjj_dphi_met_LepInvis;   //!
   TBranch        *b_MT_VBFMET_LepInvis;   //!
   TBranch        *b_HTVBFOverMET_LepInvis;   //!
   TBranch        *b_TST_Et_LepInvis;   //!
   TBranch        *b_deltaPhi_MET_TST_Phi_LepInvis;   //!
   TBranch        *b_vbfRJR_PTVBF;   //!
   TBranch        *b_vbfRJR_PTS;   //!
   TBranch        *b_vbfRJR_RVBF;   //!
   TBranch        *b_vbfRJR_MS;   //!
   TBranch        *b_vbfRJR_MVBF;   //!
   TBranch        *b_vbfRJR_dphiVBFI;   //!
   TBranch        *b_NtruthVBFJets;   //!
   TBranch        *b_NtruthStatus3Leptons;   //!
   TBranch        *b_Truth_vbfPtV;   //!
   TBranch        *b_Truth_vbfjjM;   //!
   TBranch        *b_Truth_vbfjjDPhi;   //!
   TBranch        *b_Truth_j0_pt;   //!
   TBranch        *b_Truth_j1_pt;   //!
   TBranch        *b_Truth_MET;   //!
   TBranch        *b_truthStatus3Lepton1_pdgId;   //!
   TBranch        *b_truthStatus3Lepton1_Pt;   //!
   TBranch        *b_truthStatus3Lepton1_Eta;   //!
   TBranch        *b_truthStatus3Lepton1_Phi;   //!
   TBranch        *b_truthStatus3Lepton1_E;   //!
   TBranch        *b_truthStatus3Lepton2_pdgId;   //!
   TBranch        *b_truthStatus3Lepton2_Pt;   //!
   TBranch        *b_truthStatus3Lepton2_Eta;   //!
   TBranch        *b_truthStatus3Lepton2_Phi;   //!
   TBranch        *b_truthStatus3Lepton2_E;   //!
   TBranch        *b_truthTauFromW_n;   //!
   TBranch        *b_truthTauFromW_DMV;   //!
   TBranch        *b_jetY_Index;   //!
   TBranch        *b_jetZ_Index;   //!
   TBranch        *b_jetY_LepInvis_Index;   //!
   TBranch        *b_jetZ_LepInvis_Index;   //!
   TBranch        *b_jetY_DPhiMet;   //!
   TBranch        *b_jetZ_DPhiMet;   //!
   TBranch        *b_jetY_LepInvis_DPhiMet;   //!
   TBranch        *b_jetZ_LepInvis_DPhiMet;   //!
   TBranch        *b_jetY_MT;   //!
   TBranch        *b_jetZ_MT;   //!
   TBranch        *b_jetY_LepInvis_MT;   //!
   TBranch        *b_jetZ_LepInvis_MT;   //!
   TBranch        *b_trigWeight_metTrig;   //!
   TBranch        *b_trigMatch_metTrig;   //!
   TBranch        *b_higgsinoTrig2016;   //!
   TBranch        *b_higgsinoTrig2017;   //!
   TBranch        *b_trigWeight_singleMuonTrig;   //!
   TBranch        *b_trigMatch_singleMuonTrig;   //!
   TBranch        *b_trigWeight_singleElectronTrig;   //!
   TBranch        *b_trigMatch_singleElectronTrig;   //!
   TBranch        *b_trigWeight_diMuonTrig;   //!
   TBranch        *b_trigMatch_diMuonTrig;   //!
   TBranch        *b_trigWeight_diElectronTrig;   //!
   TBranch        *b_trigMatch_diElectronTrig;   //!
   TBranch        *b_passEmulTightJetCleaning1Jet;   //!
   TBranch        *b_passEmulTightJetCleaning2Jet;   //!
   TBranch        *b_passBadTileJetVeto;   //!
   TBranch        *b_HLT_2mu4_j85_xe50_mht_emul;   //!
   TBranch        *b_HLT_mu4_j125_xe90_mht_emul;   //!
   TBranch        *b_L1_met_Et;   //!
   TBranch        *b_L1_jet1Pt;   //!
   TBranch        *b_HLT_met_pufit_Et;   //!
   TBranch        *b_HLT_met_cell_Et;   //!
   TBranch        *b_HLT_jet1Pt;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_avg_mu;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_nLep_base;   //!
   TBranch        *b_nLep_signal;   //!
   TBranch        *b_nEle_base;   //!
   TBranch        *b_nEle_signal;   //!
   TBranch        *b_nMu_base;   //!
   TBranch        *b_nMu_signal;   //!
   TBranch        *b_nLep_signal_PLTLooseLoose;   //!
   TBranch        *b_nTau_base;   //!
   TBranch        *b_nTau_signal;   //!
   TBranch        *b_tau1Loose;   //!
   TBranch        *b_tau1Medium;   //!
   TBranch        *b_tau1Tight;   //!
   TBranch        *b_tau1Pt;   //!
   TBranch        *b_tau1Eta;   //!
   TBranch        *b_tau1Phi;   //!
   TBranch        *b_tau1M;   //!
   TBranch        *b_tau1NTracks;   //!
   TBranch        *b_tau2Loose;   //!
   TBranch        *b_tau2Medium;   //!
   TBranch        *b_tau2Tight;   //!
   TBranch        *b_tau2Pt;   //!
   TBranch        *b_tau2Eta;   //!
   TBranch        *b_tau2Phi;   //!
   TBranch        *b_tau2M;   //!
   TBranch        *b_tau2NTracks;   //!
   TBranch        *b_nLep_signal_PLTLooseTight;   //!
   TBranch        *b_nLep_signal_PLTTightLoose;   //!
   TBranch        *b_nLep_signal_PLTTightTight;   //!
   TBranch        *b_lep1Flavor;   //!
   TBranch        *b_lep1Charge;   //!
   TBranch        *b_lep1Author;   //!
   TBranch        *b_lep1Pt;   //!
   TBranch        *b_lep1Eta;   //!
   TBranch        *b_lep1Phi;   //!
   TBranch        *b_lep1M;   //!
   TBranch        *b_lep1MT_Met;   //!
   TBranch        *b_lep1MT_Met_LepInvis;   //!
   TBranch        *b_lep1_DPhiMet;   //!
   TBranch        *b_lep1_DPhiMet_LepInvis;   //!
   TBranch        *b_lep1D0;   //!
   TBranch        *b_lep1D0Sig;   //!
   TBranch        *b_lep1Z0;   //!
   TBranch        *b_lep1Z0SinTheta;   //!
   TBranch        *b_lep1Topoetcone20;   //!
   TBranch        *b_lep1Topoetcone30;   //!
   TBranch        *b_lep1Topoetcone40;   //!
   TBranch        *b_lep1Ptvarcone20;   //!
   TBranch        *b_lep1Ptvarcone30;   //!
   TBranch        *b_lep1Ptvarcone40;   //!
   TBranch        *b_lep1CorrTopoetcone20;   //!
   TBranch        *b_lep1CorrTopoetcone30;   //!
   TBranch        *b_lep1CorrTopoetcone40;   //!
   TBranch        *b_lep1CorrPtvarcone20;   //!
   TBranch        *b_lep1CorrPtvarcone30;   //!
   TBranch        *b_lep1CorrPtvarcone40;   //!
   TBranch        *b_lep1PassOR;   //!
   TBranch        *b_lep1Type;   //!
   TBranch        *b_lep1Origin;   //!
   TBranch        *b_lep1EgMotherType;   //!
   TBranch        *b_lep1EgMotherOrigin;   //!
   TBranch        *b_lep1NPix;   //!
   TBranch        *b_lep1PassBL;   //!
   TBranch        *b_lep1baseline;   //!
   TBranch        *b_lep1VeryLoose;   //!
   TBranch        *b_lep1Loose;   //!
   TBranch        *b_lep1Medium;   //!
   TBranch        *b_lep1Tight;   //!
   TBranch        *b_lep1LowPtWP;   //!
   TBranch        *b_lep1LooseBLllh;   //!
   TBranch        *b_lep1IsoHighPtCaloOnly;   //!
   TBranch        *b_lep1IsoHighPtTrackOnly;   //!
   TBranch        *b_lep1corr_IsoHighPtCaloOnly;   //!
   TBranch        *b_lep1IsoFCHighPtCaloOnly;   //!
   TBranch        *b_lep1IsoFCTightTrackOnly;   //!
   TBranch        *b_lep1IsoFCLoose;   //!
   TBranch        *b_lep1IsoFCTight;   //!
   TBranch        *b_lep1IsoFCLoose_FixedRad;   //!
   TBranch        *b_lep1IsoFCTight_FixedRad;   //!
   TBranch        *b_lep1IsoLoose_VarRad;   //!
   TBranch        *b_lep1IsoLoose_FixedRad;   //!
   TBranch        *b_lep1IsoTight_VarRad;   //!
   TBranch        *b_lep1IsoTight_FixedRad;   //!
   TBranch        *b_lep1IsoTightTrackOnly_VarRad;   //!
   TBranch        *b_lep1IsoTightTrackOnly_FixedRad;   //!
   TBranch        *b_lep1IsoPflowLoose_VarRad;   //!
   TBranch        *b_lep1IsoPflowLoose_FixedRad;   //!
   TBranch        *b_lep1IsoPflowTight_VarRad;   //!
   TBranch        *b_lep1IsoPflowTight_FixedRad;   //!
   TBranch        *b_lep1IsoPLVLoose;   //!
   TBranch        *b_lep1IsoPLVTight;   //!
   TBranch        *b_lep1Signal;   //!
   TBranch        *b_lep1SignalPLTLoose;   //!
   TBranch        *b_lep1SignalPLTTight;   //!
   TBranch        *b_lep1TruthMatched;   //!
   TBranch        *b_lep1TruthCharge;   //!
   TBranch        *b_lep1TruthPt;   //!
   TBranch        *b_lep1TruthEta;   //!
   TBranch        *b_lep1TruthPhi;   //!
   TBranch        *b_lep1TruthM;   //!
   TBranch        *b_lep1TrackPt;   //!
   TBranch        *b_lep1TrackEta;   //!
   TBranch        *b_lep1TrackPhi;   //!
   TBranch        *b_lep1Weight;   //!
   TBranch        *b_lep1PromptLepVeto_score;   //!
   TBranch        *b_lep1PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep1PLTInput_DRlj;   //!
   TBranch        *b_lep1PLTInput_rnnip;   //!
   TBranch        *b_lep1PLTInput_DL1mu;   //!
   TBranch        *b_lep1PLTInput_PtRel;   //!
   TBranch        *b_lep1PLTInput_PtFrac;   //!
   TBranch        *b_lep1CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep1CorrPLTInput_DRlj;   //!
   TBranch        *b_lep1CorrPLTInput_rnnip;   //!
   TBranch        *b_lep1CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep1CorrPLTInput_PtRel;   //!
   TBranch        *b_lep1CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep1CorrPromptLepVeto;   //!
   TBranch        *b_lep1LowPtPLT;   //!
   TBranch        *b_lep1LowPtPLT_Cone40;   //!
   TBranch        *b_lep1IsoLowPtPLT_Loose;   //!
   TBranch        *b_lep1IsoLowPtPLT_Tight;   //!
   TBranch        *b_lep1IsoLowPtPLT_MediumLH_Loose;   //!
   TBranch        *b_lep1IsoLowPtPLT_MediumLH_Tight;   //!
   TBranch        *b_lep1EWKCombiBaseline;   //!
   TBranch        *b_lep1PassZCR;   //!
   TBranch        *b_lep1PassWCR;   //!
   TBranch        *b_lep2Flavor;   //!
   TBranch        *b_lep2Charge;   //!
   TBranch        *b_lep2Author;   //!
   TBranch        *b_lep2Pt;   //!
   TBranch        *b_lep2Eta;   //!
   TBranch        *b_lep2Phi;   //!
   TBranch        *b_lep2M;   //!
   TBranch        *b_lep2MT_Met;   //!
   TBranch        *b_lep2MT_Met_LepInvis;   //!
   TBranch        *b_lep2_DPhiMet;   //!
   TBranch        *b_lep2_DPhiMet_LepInvis;   //!
   TBranch        *b_lep2D0;   //!
   TBranch        *b_lep2D0Sig;   //!
   TBranch        *b_lep2Z0;   //!
   TBranch        *b_lep2Z0SinTheta;   //!
   TBranch        *b_lep2Topoetcone20;   //!
   TBranch        *b_lep2Topoetcone30;   //!
   TBranch        *b_lep2Topoetcone40;   //!
   TBranch        *b_lep2Ptvarcone20;   //!
   TBranch        *b_lep2Ptvarcone30;   //!
   TBranch        *b_lep2Ptvarcone40;   //!
   TBranch        *b_lep2CorrTopoetcone20;   //!
   TBranch        *b_lep2CorrTopoetcone30;   //!
   TBranch        *b_lep2CorrTopoetcone40;   //!
   TBranch        *b_lep2CorrPtvarcone20;   //!
   TBranch        *b_lep2CorrPtvarcone30;   //!
   TBranch        *b_lep2CorrPtvarcone40;   //!
   TBranch        *b_lep2PassOR;   //!
   TBranch        *b_lep2Type;   //!
   TBranch        *b_lep2Origin;   //!
   TBranch        *b_lep2EgMotherType;   //!
   TBranch        *b_lep2EgMotherOrigin;   //!
   TBranch        *b_lep2NPix;   //!
   TBranch        *b_lep2PassBL;   //!
   TBranch        *b_lep2baseline;   //!
   TBranch        *b_lep2VeryLoose;   //!
   TBranch        *b_lep2Loose;   //!
   TBranch        *b_lep2Medium;   //!
   TBranch        *b_lep2Tight;   //!
   TBranch        *b_lep2LowPtWP;   //!
   TBranch        *b_lep2LooseBLllh;   //!
   TBranch        *b_lep2IsoHighPtCaloOnly;   //!
   TBranch        *b_lep2IsoHighPtTrackOnly;   //!
   TBranch        *b_lep2corr_IsoHighPtCaloOnly;   //!
   TBranch        *b_lep2IsoFCHighPtCaloOnly;   //!
   TBranch        *b_lep2IsoFCTightTrackOnly;   //!
   TBranch        *b_lep2IsoFCLoose;   //!
   TBranch        *b_lep2IsoFCTight;   //!
   TBranch        *b_lep2IsoFCLoose_FixedRad;   //!
   TBranch        *b_lep2IsoFCTight_FixedRad;   //!
   TBranch        *b_lep2IsoLoose_VarRad;   //!
   TBranch        *b_lep2IsoLoose_FixedRad;   //!
   TBranch        *b_lep2IsoTight_VarRad;   //!
   TBranch        *b_lep2IsoTight_FixedRad;   //!
   TBranch        *b_lep2IsoTightTrackOnly_VarRad;   //!
   TBranch        *b_lep2IsoTightTrackOnly_FixedRad;   //!
   TBranch        *b_lep2IsoPflowLoose_VarRad;   //!
   TBranch        *b_lep2IsoPflowLoose_FixedRad;   //!
   TBranch        *b_lep2IsoPflowTight_VarRad;   //!
   TBranch        *b_lep2IsoPflowTight_FixedRad;   //!
   TBranch        *b_lep2IsoPLVLoose;   //!
   TBranch        *b_lep2IsoPLVTight;   //!
   TBranch        *b_lep2Signal;   //!
   TBranch        *b_lep2SignalPLTLoose;   //!
   TBranch        *b_lep2SignalPLTTight;   //!
   TBranch        *b_lep2TruthMatched;   //!
   TBranch        *b_lep2TruthCharge;   //!
   TBranch        *b_lep2TruthPt;   //!
   TBranch        *b_lep2TruthEta;   //!
   TBranch        *b_lep2TruthPhi;   //!
   TBranch        *b_lep2TruthM;   //!
   TBranch        *b_lep2TrackPt;   //!
   TBranch        *b_lep2TrackEta;   //!
   TBranch        *b_lep2TrackPhi;   //!
   TBranch        *b_lep2Weight;   //!
   TBranch        *b_lep2PromptLepVeto_score;   //!
   TBranch        *b_lep2PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep2PLTInput_DRlj;   //!
   TBranch        *b_lep2PLTInput_rnnip;   //!
   TBranch        *b_lep2PLTInput_DL1mu;   //!
   TBranch        *b_lep2PLTInput_PtRel;   //!
   TBranch        *b_lep2PLTInput_PtFrac;   //!
   TBranch        *b_lep2CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep2CorrPLTInput_DRlj;   //!
   TBranch        *b_lep2CorrPLTInput_rnnip;   //!
   TBranch        *b_lep2CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep2CorrPLTInput_PtRel;   //!
   TBranch        *b_lep2CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep2CorrPromptLepVeto;   //!
   TBranch        *b_lep2LowPtPLT;   //!
   TBranch        *b_lep2LowPtPLT_Cone40;   //!
   TBranch        *b_lep2IsoLowPtPLT_Loose;   //!
   TBranch        *b_lep2IsoLowPtPLT_Tight;   //!
   TBranch        *b_lep2IsoLowPtPLT_MediumLH_Loose;   //!
   TBranch        *b_lep2IsoLowPtPLT_MediumLH_Tight;   //!
   TBranch        *b_lep2EWKCombiBaseline;   //!
   TBranch        *b_lep2PassZCR;   //!
   TBranch        *b_lep2PassWCR;   //!
   TBranch        *b_lep3Flavor;   //!
   TBranch        *b_lep3Charge;   //!
   TBranch        *b_lep3Author;   //!
   TBranch        *b_lep3Pt;   //!
   TBranch        *b_lep3Eta;   //!
   TBranch        *b_lep3Phi;   //!
   TBranch        *b_lep3M;   //!
   TBranch        *b_lep3MT_Met;   //!
   TBranch        *b_lep3MT_Met_LepInvis;   //!
   TBranch        *b_lep3_DPhiMet;   //!
   TBranch        *b_lep3_DPhiMet_LepInvis;   //!
   TBranch        *b_lep3D0;   //!
   TBranch        *b_lep3D0Sig;   //!
   TBranch        *b_lep3Z0;   //!
   TBranch        *b_lep3Z0SinTheta;   //!
   TBranch        *b_lep3Topoetcone20;   //!
   TBranch        *b_lep3Topoetcone30;   //!
   TBranch        *b_lep3Topoetcone40;   //!
   TBranch        *b_lep3Ptvarcone20;   //!
   TBranch        *b_lep3Ptvarcone30;   //!
   TBranch        *b_lep3Ptvarcone40;   //!
   TBranch        *b_lep3CorrTopoetcone20;   //!
   TBranch        *b_lep3CorrTopoetcone30;   //!
   TBranch        *b_lep3CorrTopoetcone40;   //!
   TBranch        *b_lep3CorrPtvarcone20;   //!
   TBranch        *b_lep3CorrPtvarcone30;   //!
   TBranch        *b_lep3CorrPtvarcone40;   //!
   TBranch        *b_lep3PassOR;   //!
   TBranch        *b_lep3Type;   //!
   TBranch        *b_lep3Origin;   //!
   TBranch        *b_lep3EgMotherType;   //!
   TBranch        *b_lep3EgMotherOrigin;   //!
   TBranch        *b_lep3NPix;   //!
   TBranch        *b_lep3PassBL;   //!
   TBranch        *b_lep3baseline;   //!
   TBranch        *b_lep3VeryLoose;   //!
   TBranch        *b_lep3Loose;   //!
   TBranch        *b_lep3Medium;   //!
   TBranch        *b_lep3Tight;   //!
   TBranch        *b_lep3LowPtWP;   //!
   TBranch        *b_lep3LooseBLllh;   //!
   TBranch        *b_lep3IsoHighPtCaloOnly;   //!
   TBranch        *b_lep3IsoHighPtTrackOnly;   //!
   TBranch        *b_lep3corr_IsoHighPtCaloOnly;   //!
   TBranch        *b_lep3IsoFCHighPtCaloOnly;   //!
   TBranch        *b_lep3IsoFCTightTrackOnly;   //!
   TBranch        *b_lep3IsoFCLoose;   //!
   TBranch        *b_lep3IsoFCTight;   //!
   TBranch        *b_lep3IsoFCLoose_FixedRad;   //!
   TBranch        *b_lep3IsoFCTight_FixedRad;   //!
   TBranch        *b_lep3IsoLoose_VarRad;   //!
   TBranch        *b_lep3IsoLoose_FixedRad;   //!
   TBranch        *b_lep3IsoTight_VarRad;   //!
   TBranch        *b_lep3IsoTight_FixedRad;   //!
   TBranch        *b_lep3IsoTightTrackOnly_VarRad;   //!
   TBranch        *b_lep3IsoTightTrackOnly_FixedRad;   //!
   TBranch        *b_lep3IsoPflowLoose_VarRad;   //!
   TBranch        *b_lep3IsoPflowLoose_FixedRad;   //!
   TBranch        *b_lep3IsoPflowTight_VarRad;   //!
   TBranch        *b_lep3IsoPflowTight_FixedRad;   //!
   TBranch        *b_lep3IsoPLVLoose;   //!
   TBranch        *b_lep3IsoPLVTight;   //!
   TBranch        *b_lep3Signal;   //!
   TBranch        *b_lep3SignalPLTLoose;   //!
   TBranch        *b_lep3SignalPLTTight;   //!
   TBranch        *b_lep3TruthMatched;   //!
   TBranch        *b_lep3TruthCharge;   //!
   TBranch        *b_lep3TruthPt;   //!
   TBranch        *b_lep3TruthEta;   //!
   TBranch        *b_lep3TruthPhi;   //!
   TBranch        *b_lep3TruthM;   //!
   TBranch        *b_lep3TrackPt;   //!
   TBranch        *b_lep3TrackEta;   //!
   TBranch        *b_lep3TrackPhi;   //!
   TBranch        *b_lep3Weight;   //!
   TBranch        *b_lep3PromptLepVeto_score;   //!
   TBranch        *b_lep3PLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep3PLTInput_DRlj;   //!
   TBranch        *b_lep3PLTInput_rnnip;   //!
   TBranch        *b_lep3PLTInput_DL1mu;   //!
   TBranch        *b_lep3PLTInput_PtRel;   //!
   TBranch        *b_lep3PLTInput_PtFrac;   //!
   TBranch        *b_lep3CorrPLTInput_TrackJetNTrack;   //!
   TBranch        *b_lep3CorrPLTInput_DRlj;   //!
   TBranch        *b_lep3CorrPLTInput_rnnip;   //!
   TBranch        *b_lep3CorrPLTInput_DL1mu;   //!
   TBranch        *b_lep3CorrPLTInput_PtRel;   //!
   TBranch        *b_lep3CorrPLTInput_PtFrac;   //!
   TBranch        *b_lep3CorrPromptLepVeto;   //!
   TBranch        *b_lep3LowPtPLT;   //!
   TBranch        *b_lep3LowPtPLT_Cone40;   //!
   TBranch        *b_lep3IsoLowPtPLT_Loose;   //!
   TBranch        *b_lep3IsoLowPtPLT_Tight;   //!
   TBranch        *b_lep3IsoLowPtPLT_MediumLH_Loose;   //!
   TBranch        *b_lep3IsoLowPtPLT_MediumLH_Tight;   //!
   TBranch        *b_lep3EWKCombiBaseline;   //!
   TBranch        *b_lep3PassZCR;   //!
   TBranch        *b_lep3PassWCR;   //!
   TBranch        *b_nJet30;   //!
   TBranch        *b_nJet25;   //!
   TBranch        *b_nJet20;   //!
   TBranch        *b_nBJet30;   //!
   TBranch        *b_nBJet25;   //!
   TBranch        *b_nBJet20;   //!
   TBranch        *b_nTotalJet;   //!
   TBranch        *b_nTotalJet20;   //!
   TBranch        *b_DecayModeTTbar;   //!
   TBranch        *b_met_Et;   //!
   TBranch        *b_met_Phi;   //!
   TBranch        *b_met_Signif;   //!
   TBranch        *b_TST_Et;   //!
   TBranch        *b_TST_Phi;   //!
   TBranch        *b_met_track_Et;   //!
   TBranch        *b_met_track_Phi;   //!
   TBranch        *b_deltaPhi_MET_TST_Phi;   //!
   TBranch        *b_met_muons_invis_Et;   //!
   TBranch        *b_met_muons_invis_Phi;   //!
   TBranch        *b_meffInc30;   //!
   TBranch        *b_Ht30;   //!
   TBranch        *b_LepAplanarity;   //!
   TBranch        *b_JetAplanarity;   //!
   TBranch        *b_mjj;   //!
   TBranch        *b_dEtajj;   //!
   TBranch        *b_dPhijj;   //!
   TBranch        *b_mt_lep1;   //!
   TBranch        *b_mt_lep2;   //!
   TBranch        *b_mt_lep3;   //!
   TBranch        *b_mt_lep1_metTrack;   //!
   TBranch        *b_mt_lep2_metTrack;   //!
   TBranch        *b_mt_lep3_metTrack;   //!
   TBranch        *b_METOverHT;   //!
   TBranch        *b_METOverJ1pT;   //!
   TBranch        *b_METTrackOverHT;   //!
   TBranch        *b_METTrackOverJ1pT;   //!
   TBranch        *b_DPhiJ1Met;   //!
   TBranch        *b_DPhiJ2Met;   //!
   TBranch        *b_DPhiJ3Met;   //!
   TBranch        *b_DPhiJ4Met;   //!
   TBranch        *b_minDPhiCenJetsMet;   //!
   TBranch        *b_minDPhiAllJetsMet;   //!
   TBranch        *b_minDPhiAllJetsMet_LepInvis;   //!
   TBranch        *b_DPhiJ1MetTrack;   //!
   TBranch        *b_DPhiJ2MetTrack;   //!
   TBranch        *b_DPhiJ3MetTrack;   //!
   TBranch        *b_DPhiJ4MetTrack;   //!
   TBranch        *b_minDPhiCenJetsMetTrack;   //!
   TBranch        *b_METOverHTLep;   //!
   TBranch        *b_METTrackOverHTLep;   //!
   TBranch        *b_mll;   //!
   TBranch        *b_Rll;   //!
   TBranch        *b_Ptll;   //!
   TBranch        *b_dPhiPllMet;   //!
   TBranch        *b_dPhiPllMetTrack;   //!
   TBranch        *b_METRel;   //!
   TBranch        *b_METTrackRel;   //!
   TBranch        *b_dPhiNearMet;   //!
   TBranch        *b_dPhiNearMetTrack;   //!
   TBranch        *b_dPhiMetAndMetTrack;   //!
   TBranch        *b_MTauTau;   //!
   TBranch        *b_MTauTau_metTrack;   //!
   TBranch        *b_RjlOverEl;   //!
   TBranch        *b_LepCosThetaLab;   //!
   TBranch        *b_LepCosThetaCoM;   //!
   TBranch        *b_mt2leplsp_0;   //!
   TBranch        *b_mt2leplsp_50;   //!
   TBranch        *b_mt2leplsp_100;   //!
   TBranch        *b_mt2leplsp_150;   //!
   TBranch        *b_mt2leplsp_200;   //!
   TBranch        *b_mt2leplsp_300;   //!
   TBranch        *b_mt2leplsp_0_metTrack;   //!
   TBranch        *b_mt2leplsp_50_metTrack;   //!
   TBranch        *b_mt2leplsp_100_metTrack;   //!
   TBranch        *b_mt2leplsp_150_metTrack;   //!
   TBranch        *b_mt2leplsp_200_metTrack;   //!
   TBranch        *b_mt2leplsp_300_metTrack;   //!
   TBranch        *b_nSFOS;   //!
   TBranch        *b_mt_minMll;   //!
   TBranch        *b_minMll;   //!
   TBranch        *b_minRll;   //!
   TBranch        *b_m3l;   //!
   TBranch        *b_pT3l;   //!
   TBranch        *b_dPhi3lMet;   //!
   TBranch        *b_ptZ_minMll;   //!
   TBranch        *b_ptW_minMll;   //!
   TBranch        *b_ptWlep_minMll;   //!
   TBranch        *b_RZlep_minMll;   //!
   TBranch        *b_RZWlep_minMll;   //!
   TBranch        *b_dPhiZMet_minMll;   //!
   TBranch        *b_dPhiWLepMet_minMll;   //!
   TBranch        *b_mt2WZlsp_0;   //!
   TBranch        *b_mt2WZlsp_100;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_leptonWeight;   //!
   TBranch        *b_eventWeight;   //!
   TBranch        *b_genWeight;   //!
   TBranch        *b_bTagWeight;   //!
   TBranch        *b_jvtWeight;   //!
   TBranch        *b_genWeightUp;   //!
   TBranch        *b_genWeightDown;   //!
   TBranch        *b_triggerWeight;   //!
   TBranch        *b_totalWeight;   //!
   TBranch        *b_truthMll;   //!
   TBranch        *b_winoBinoMllWeight;   //!
   TBranch        *b_winoBinoXsecWeight;   //!
   TBranch        *b_winoBinoBrFracWeight;   //!
   TBranch        *b_winoBinoWeight;   //!
   TBranch        *b_NUHM2weight_350m12;   //!
   TBranch        *b_NUHM2weight_400m12;   //!
   TBranch        *b_NUHM2weight_500m12;   //!
   TBranch        *b_NUHM2weight_600m12;   //!
   TBranch        *b_NUHM2weight_700m12;   //!
   TBranch        *b_NUHM2weight_800m12;   //!
   TBranch        *b_FFWeight;   //!
   TBranch        *b_nLep_antiID;   //!
   TBranch        *b_lep1AntiID;   //!
   TBranch        *b_lep2AntiID;   //!
   TBranch        *b_lep3AntiID;   //!
   TBranch        *b_nLep_signalActual;   //!
   TBranch        *b_lep1SignalActual;   //!
   TBranch        *b_lep2SignalActual;   //!
   TBranch        *b_lep3SignalActual;   //!
   TBranch        *b_ETMissTrigSF;   //!
   TBranch        *b_HLT_mu4;   //!
   TBranch        *b_HLT_2mu4;   //!
   TBranch        *b_HLT_2mu10;   //!
   TBranch        *b_HLT_2mu4_j85_xe50_mht;   //!
   TBranch        *b_HLT_mu4_j125_xe90_mht;   //!
   TBranch        *b_HLT_xe70;   //!
   TBranch        *b_HLT_xe70_mht;   //!
   TBranch        *b_HLT_xe70_mht_wEFMu;   //!
   TBranch        *b_HLT_xe70_tc_lcw;   //!
   TBranch        *b_HLT_xe70_tc_lcw_wEFMu;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe100_L1XE50;   //!
   TBranch        *b_HLT_xe100_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe100_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe100_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe110_L1XE50;   //!
   TBranch        *b_HLT_xe110_tc_em_L1XE50;   //!
   TBranch        *b_HLT_xe110_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe110_tc_em_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE40;   //!
   TBranch        *b_HLT_xe50_mht_L1XE20;   //!
   TBranch        *b_HLT_xe110_pufit_xe65_L1XE50;   //!
   TBranch        *b_HLT_xe110_pufit_xe70_L1XE50;   //!
   TBranch        *b_HLT_j85_L1J40;   //!
   TBranch        *b_HLT_j125_L1J50;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e60_medium;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e5_lhvloose_nod0;   //!
   TBranch        *b_HLT_2e5_lhvloose_nod0;   //!
   TBranch        *b_HLT_e5_lhloose_nod0;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE40_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe80_pufit_2dphi10_L12MU4_J40_XE50;   //!
   //TBranch        *b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60;   //!
   //TBranch        *b_HLT_mu4_j80_xe80_pufit_2dphi10_L1MU4_XE60;   //!
   //TBranch        *b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;   //!
   //TBranch        *b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60;   //!
   //TBranch        *b_HLT_2e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60;   //!
   //TBranch        *b_HLT_2e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;   //!
   //TBranch        *b_HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;   //!
   //TBranch        *b_HLT_e5_lhloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30;   //!
   //TBranch        *b_HLT_e5_lhmedium_nod0_j50_xe80_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50;   //!
   //TBranch        *b_HLT_e5_lhmedium_nod0_mu4_j30_xe65_pufit_2dphi10_L1MU4_XE60;   //!
   //TBranch        *b_HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60;   //!
   //TBranch        *b_HLT_e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60;   //!
   //TBranch        *b_HLT_e5_lhmedium_nod0_j50_xe90_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50;   //!
   //TBranch        *b_HLT_mu4_j100_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30;   //!
   //TBranch        *b_HLT_mu4_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30;   //!
   //TBranch        *b_HLT_2mu4_xe40mht_L12MU4_J20_XE30_DPHI-J20sXE30;   //!
   //TBranch        *b_HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30;   //!
   //TBranch        *b_HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50;   //!
   //TBranch        *b_HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul;   //!
   TBranch        *b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul;   //!
   TBranch        *b_HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul;   //!
   TBranch        *b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul;   //!
   TBranch        *b_HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul;   //!
   TBranch        *b_HLT_2dphi10_emul;   //!
   TBranch        *b_HLT_2mu4_invm1_emul;   //!
   TBranch        *b_HLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_HLT_j20;   //!
   TBranch        *b_HLT_j40;   //!
   TBranch        *b_HLT_j30;   //!
   TBranch        *b_HLT_j50;   //!
   TBranch        *b_HLT_j90;   //!
   TBranch        *b_L1_DPHI_2J20XE_emul;   //!
   TBranch        *b_L1_MU4;   //!
   TBranch        *b_L1_2MU4;   //!
   TBranch        *b_L1_J20;   //!
   TBranch        *b_L1_J30;   //!
   TBranch        *b_L1_J40;   //!
   TBranch        *b_L1_J50;   //!
   TBranch        *b_L1_XE30;   //!
   TBranch        *b_L1_XE40;   //!
   TBranch        *b_L1_XE50;   //!
   TBranch        *b_x1;   //!
   TBranch        *b_x2;   //!
   TBranch        *b_pdf1;   //!
   TBranch        *b_pdf2;   //!
   TBranch        *b_scalePDF;   //!
   TBranch        *b_id1;   //!
   TBranch        *b_id2;   //!
   TBranch        *b_RJR_PTISR;   //!
   TBranch        *b_RJR_RISR;   //!
   TBranch        *b_RJR_MS;   //!
   TBranch        *b_RJR_MISR;   //!
   TBranch        *b_RJR_dphiISRI;   //!
   TBranch        *b_RJR_NjV;   //!
   TBranch        *b_RJR_NjISR;   //!
   TBranch        *b_SUSYPt;   //!
   TBranch        *b_ISRWeight;   //!
   TBranch        *b_PRWHash;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_GenHt;   //!
   TBranch        *b_GenMET;   //!
   TBranch        *b_DatasetNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_FS;   //!
   TBranch        *b_LHE3Weights;   //!
   TBranch        *b_LHE3WeightNames;   //!

//   CompressedVBF(TTree *tree=0);
//   virtual ~CompressedVBF();
//   virtual Int_t    Cut(Long64_t entry);
//   virtual Int_t    GetEntry(Long64_t entry);
//   virtual Long64_t LoadTree(Long64_t entry);
//   virtual void     Init(TTree *tree);
//   virtual void     Loop();
//   virtual Bool_t   Notify();
//   virtual void     Show(Long64_t entry = -1);
//};
//
//#endif
//
//#ifdef CompressedVBF_cxx
//CompressedVBF::CompressedVBF(TTree *tree) : fChain(0) 
//{
//// if parameter tree is not specified (or zero), connect the file
//// used to generate this class and read the Tree.
//   if (tree == 0) {
//      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("AllSingals_SusySkimHiggsino_VBF_v0.7_EXOT5_tree_newsignalpoint_skimmed.root");
//      if (!f || !f->IsOpen()) {
//         f = new TFile("AllSingals_SusySkimHiggsino_VBF_v0.7_EXOT5_tree_newsignalpoint_skimmed.root");
//      }
//      f->GetObject("MGPy8EG_A14N23LO_VBFWBall_100_99_MET125_NoSys",tree);
//
//   }
//   Init(tree);
//}
//
//CompressedVBF::~CompressedVBF{
//   if (!fChain) return;
//   delete fChain->GetCurrentFile();
//}
//
//Int_t CompressedVBF::GetEntry(Long64_t entry)
//{
//// Read contents of entry.
//   if (!fChain) return 0;
//   return fChain->GetEntry(entry);
//}
//Long64_t CompressedVBF::LoadTree(Long64_t entry)
//{
//// Set the environment to read one entry
//   if (!fChain) return -5;
//   Long64_t centry = fChain->LoadTree(entry);
//   if (centry < 0) return centry;
//   if (fChain->GetTreeNumber() != fCurrent) {
//      fCurrent = fChain->GetTreeNumber();
//      Notify();
//   }
//   return centry;
//}

//void CompressedVBF::Init(TTree *tree)
void Init(TChain *tree) override 
{
    std::lock_guard<std::mutex> lock(_mutex);
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   aJets_Pt = 0;
   aJets_Eta = 0;
   aJets_Phi = 0;
   aJets_M = 0;
   aJets_Btagged = 0;
   aJets_dl1r = 0;
   truthTauFromW_DMV = 0;
   LHE3Weights = 0;
   LHE3WeightNames = 0;
   // Set branch addresses and branch pointers
   //if (!tree) return;
   fChain = tree;
   //fCurrent = -1;
   //fChain->SetMakeClass(1);

   fChain->SetBranchAddress("nAjets", &nAjets, &b_nAjets);
   fChain->SetBranchAddress("nBasejets", &nBasejets, &b_nBasejets);
   fChain->SetBranchAddress("nFjets", &nFjets, &b_nFjets);
   fChain->SetBranchAddress("nCjets", &nCjets, &b_nCjets);
   fChain->SetBranchAddress("nVBFjets", &nVBFjets, &b_nVBFjets);
   fChain->SetBranchAddress("nLep_EWKCombiBaseline", &nLep_EWKCombiBaseline, &b_nLep_EWKCombiBaseline);
   fChain->SetBranchAddress("nLep_ZCR", &nLep_ZCR, &b_nLep_ZCR);
   fChain->SetBranchAddress("nLep_WCR", &nLep_WCR, &b_nLep_WCR);
   fChain->SetBranchAddress("aJets_Pt", &aJets_Pt, &b_aJets_Pt);
   fChain->SetBranchAddress("aJets_Eta", &aJets_Eta, &b_aJets_Eta);
   fChain->SetBranchAddress("aJets_Phi", &aJets_Phi, &b_aJets_Phi);
   fChain->SetBranchAddress("aJets_M", &aJets_M, &b_aJets_M);
   fChain->SetBranchAddress("aJets_Btagged", &aJets_Btagged, &b_aJets_Btagged);
   fChain->SetBranchAddress("aJets_dl1r", &aJets_dl1r, &b_aJets_dl1r);
   fChain->SetBranchAddress("HtVec30_allJets", &HtVec30_allJets, &b_HtVec30_allJets);
   fChain->SetBranchAddress("Ht30_allJets", &Ht30_allJets, &b_Ht30_allJets);
   fChain->SetBranchAddress("HtVec20_allJets", &HtVec20_allJets, &b_HtVec20_allJets);
   fChain->SetBranchAddress("Ht20_allJets", &Ht20_allJets, &b_Ht20_allJets);
   fChain->SetBranchAddress("validVBFtags", &validVBFtags, &b_validVBFtags);
   fChain->SetBranchAddress("validSpecJet", &validSpecJet, &b_validSpecJet);
   fChain->SetBranchAddress("vbfTagJet1_Pt", &vbfTagJet1_Pt, &b_vbfTagJet1_Pt);
   fChain->SetBranchAddress("vbfTagJet1_Eta", &vbfTagJet1_Eta, &b_vbfTagJet1_Eta);
   fChain->SetBranchAddress("vbfTagJet1_Phi", &vbfTagJet1_Phi, &b_vbfTagJet1_Phi);
   fChain->SetBranchAddress("vbfTagJet1_M", &vbfTagJet1_M, &b_vbfTagJet1_M);
   fChain->SetBranchAddress("vbfTagJet1_tileEnergy", &vbfTagJet1_tileEnergy, &b_vbfTagJet1_tileEnergy);
   fChain->SetBranchAddress("vbfTagJet1_DPhiMet", &vbfTagJet1_DPhiMet, &b_vbfTagJet1_DPhiMet);
   fChain->SetBranchAddress("vbfTagJet1_DPhiMet_LepInvis", &vbfTagJet1_DPhiMet_LepInvis, &b_vbfTagJet1_DPhiMet_LepInvis);
   fChain->SetBranchAddress("vbfTagJet1_minDRaJet20", &vbfTagJet1_minDRaJet20, &b_vbfTagJet1_minDRaJet20);
   fChain->SetBranchAddress("vbfTagJet1_Btagged", &vbfTagJet1_Btagged, &b_vbfTagJet1_Btagged);
   fChain->SetBranchAddress("vbfTagJet1_dl1r", &vbfTagJet1_dl1r, &b_vbfTagJet1_dl1r);
   fChain->SetBranchAddress("vbfTagJet1_JVT", &vbfTagJet1_JVT, &b_vbfTagJet1_JVT);
   fChain->SetBranchAddress("vbfTagJet1_passOR", &vbfTagJet1_passOR, &b_vbfTagJet1_passOR);
   fChain->SetBranchAddress("vbfTagJet1_passDRcut", &vbfTagJet1_passDRcut, &b_vbfTagJet1_passDRcut);
   fChain->SetBranchAddress("vbfTagJet2_Pt", &vbfTagJet2_Pt, &b_vbfTagJet2_Pt);
   fChain->SetBranchAddress("vbfTagJet2_Eta", &vbfTagJet2_Eta, &b_vbfTagJet2_Eta);
   fChain->SetBranchAddress("vbfTagJet2_Phi", &vbfTagJet2_Phi, &b_vbfTagJet2_Phi);
   fChain->SetBranchAddress("vbfTagJet2_M", &vbfTagJet2_M, &b_vbfTagJet2_M);
   fChain->SetBranchAddress("vbfTagJet2_tileEnergy", &vbfTagJet2_tileEnergy, &b_vbfTagJet2_tileEnergy);
   fChain->SetBranchAddress("vbfTagJet2_DPhiMet", &vbfTagJet2_DPhiMet, &b_vbfTagJet2_DPhiMet);
   fChain->SetBranchAddress("vbfTagJet2_DPhiMet_LepInvis", &vbfTagJet2_DPhiMet_LepInvis, &b_vbfTagJet2_DPhiMet_LepInvis);
   fChain->SetBranchAddress("vbfTagJet2_minDRaJet20", &vbfTagJet2_minDRaJet20, &b_vbfTagJet2_minDRaJet20);
   fChain->SetBranchAddress("vbfTagJet2_Btagged", &vbfTagJet2_Btagged, &b_vbfTagJet2_Btagged);
   fChain->SetBranchAddress("vbfTagJet2_dl1r", &vbfTagJet2_dl1r, &b_vbfTagJet2_dl1r);
   fChain->SetBranchAddress("vbfTagJet2_JVT", &vbfTagJet2_JVT, &b_vbfTagJet2_JVT);
   fChain->SetBranchAddress("vbfTagJet2_passOR", &vbfTagJet2_passOR, &b_vbfTagJet2_passOR);
   fChain->SetBranchAddress("vbfTagJet2_passDRcut", &vbfTagJet2_passDRcut, &b_vbfTagJet2_passDRcut);
   fChain->SetBranchAddress("vbfSpecJet_Pt", &vbfSpecJet_Pt, &b_vbfSpecJet_Pt);
   fChain->SetBranchAddress("vbfSpecJet_Eta", &vbfSpecJet_Eta, &b_vbfSpecJet_Eta);
   fChain->SetBranchAddress("vbfSpecJet_Phi", &vbfSpecJet_Phi, &b_vbfSpecJet_Phi);
   fChain->SetBranchAddress("vbfSpecJet_M", &vbfSpecJet_M, &b_vbfSpecJet_M);
   fChain->SetBranchAddress("vbfSpecJet_tileEnergy", &vbfSpecJet_tileEnergy, &b_vbfSpecJet_tileEnergy);
   fChain->SetBranchAddress("vbfSpecJet_DPhiMet", &vbfSpecJet_DPhiMet, &b_vbfSpecJet_DPhiMet);
   fChain->SetBranchAddress("vbfSpecJet_DPhiMet_LepInvis", &vbfSpecJet_DPhiMet_LepInvis, &b_vbfSpecJet_DPhiMet_LepInvis);
   fChain->SetBranchAddress("vbfSpecJet_minDRaJet20", &vbfSpecJet_minDRaJet20, &b_vbfSpecJet_minDRaJet20);
   fChain->SetBranchAddress("vbfSpecJet_Btagged", &vbfSpecJet_Btagged, &b_vbfSpecJet_Btagged);
   fChain->SetBranchAddress("vbfSpecJet_dl1r", &vbfSpecJet_dl1r, &b_vbfSpecJet_dl1r);
   fChain->SetBranchAddress("vbfSpecJet_JVT", &vbfSpecJet_JVT, &b_vbfSpecJet_JVT);
   fChain->SetBranchAddress("vbfSpecJet_passOR", &vbfSpecJet_passOR, &b_vbfSpecJet_passOR);
   fChain->SetBranchAddress("vbfSpecJet_passDRcut", &vbfSpecJet_passDRcut, &b_vbfSpecJet_passDRcut);
   fChain->SetBranchAddress("jetC_Pt", &jetC_Pt, &b_jetC_Pt);
   fChain->SetBranchAddress("jetC_Eta", &jetC_Eta, &b_jetC_Eta);
   fChain->SetBranchAddress("jetC_Phi", &jetC_Phi, &b_jetC_Phi);
   fChain->SetBranchAddress("jetC_M", &jetC_M, &b_jetC_M);
   fChain->SetBranchAddress("jetC_tileEnergy", &jetC_tileEnergy, &b_jetC_tileEnergy);
   fChain->SetBranchAddress("jetC_DPhiMet", &jetC_DPhiMet, &b_jetC_DPhiMet);
   fChain->SetBranchAddress("jetC_DPhiMet_LepInvis", &jetC_DPhiMet_LepInvis, &b_jetC_DPhiMet_LepInvis);
   fChain->SetBranchAddress("jetC_minDRaJet20", &jetC_minDRaJet20, &b_jetC_minDRaJet20);
   fChain->SetBranchAddress("jetC_Btagged", &jetC_Btagged, &b_jetC_Btagged);
   fChain->SetBranchAddress("jetC_dl1r", &jetC_dl1r, &b_jetC_dl1r);
   fChain->SetBranchAddress("jetC_JVT", &jetC_JVT, &b_jetC_JVT);
   fChain->SetBranchAddress("jetC_passOR", &jetC_passOR, &b_jetC_passOR);
   fChain->SetBranchAddress("jetC_passDRcut", &jetC_passDRcut, &b_jetC_passDRcut);
   fChain->SetBranchAddress("jetD_Pt", &jetD_Pt, &b_jetD_Pt);
   fChain->SetBranchAddress("jetD_Eta", &jetD_Eta, &b_jetD_Eta);
   fChain->SetBranchAddress("jetD_Phi", &jetD_Phi, &b_jetD_Phi);
   fChain->SetBranchAddress("jetD_M", &jetD_M, &b_jetD_M);
   fChain->SetBranchAddress("jetD_tileEnergy", &jetD_tileEnergy, &b_jetD_tileEnergy);
   fChain->SetBranchAddress("jetD_DPhiMet", &jetD_DPhiMet, &b_jetD_DPhiMet);
   fChain->SetBranchAddress("jetD_DPhiMet_LepInvis", &jetD_DPhiMet_LepInvis, &b_jetD_DPhiMet_LepInvis);
   fChain->SetBranchAddress("jetD_minDRaJet20", &jetD_minDRaJet20, &b_jetD_minDRaJet20);
   fChain->SetBranchAddress("jetD_Btagged", &jetD_Btagged, &b_jetD_Btagged);
   fChain->SetBranchAddress("jetD_dl1r", &jetD_dl1r, &b_jetD_dl1r);
   fChain->SetBranchAddress("jetD_JVT", &jetD_JVT, &b_jetD_JVT);
   fChain->SetBranchAddress("jetD_passOR", &jetD_passOR, &b_jetD_passOR);
   fChain->SetBranchAddress("jetD_passDRcut", &jetD_passDRcut, &b_jetD_passDRcut);
   fChain->SetBranchAddress("vbfTagJet1_Index", &vbfTagJet1_Index, &b_vbfTagJet1_Index);
   fChain->SetBranchAddress("vbfTagJet2_Index", &vbfTagJet2_Index, &b_vbfTagJet2_Index);
   fChain->SetBranchAddress("SpecJet_Index", &SpecJet_Index, &b_SpecJet_Index);
   fChain->SetBranchAddress("jetC_Index", &jetC_Index, &b_jetC_Index);
   fChain->SetBranchAddress("jetD_Index", &jetD_Index, &b_jetD_Index);
   fChain->SetBranchAddress("vbfjjPt", &vbfjjPt, &b_vbfjjPt);
   fChain->SetBranchAddress("vbfjjEta", &vbfjjEta, &b_vbfjjEta);
   fChain->SetBranchAddress("vbfjjPhi", &vbfjjPhi, &b_vbfjjPhi);
   fChain->SetBranchAddress("vbfjjM", &vbfjjM, &b_vbfjjM);
   fChain->SetBranchAddress("vbfjjDEta", &vbfjjDEta, &b_vbfjjDEta);
   fChain->SetBranchAddress("vbfjjDPhi", &vbfjjDPhi, &b_vbfjjDPhi);
   fChain->SetBranchAddress("vbfjjDR", &vbfjjDR, &b_vbfjjDR);
   fChain->SetBranchAddress("vbfEtaEta", &vbfEtaEta, &b_vbfEtaEta);
   fChain->SetBranchAddress("SpecJet_Cen", &SpecJet_Cen, &b_SpecJet_Cen);
   fChain->SetBranchAddress("SpecJet_ExpCen", &SpecJet_ExpCen, &b_SpecJet_ExpCen);
   fChain->SetBranchAddress("jetC_Cen", &jetC_Cen, &b_jetC_Cen);
   fChain->SetBranchAddress("jetC_ExpCen", &jetC_ExpCen, &b_jetC_ExpCen);
   fChain->SetBranchAddress("jetD_Cen", &jetD_Cen, &b_jetD_Cen);
   fChain->SetBranchAddress("jetD_ExpCen", &jetD_ExpCen, &b_jetD_ExpCen);
   fChain->SetBranchAddress("jetC_mjjrel", &jetC_mjjrel, &b_jetC_mjjrel);
   fChain->SetBranchAddress("jetD_mjjrel", &jetD_mjjrel, &b_jetD_mjjrel);
   fChain->SetBranchAddress("HTVBF", &HTVBF, &b_HTVBF);
   fChain->SetBranchAddress("HTVBFOverMET", &HTVBFOverMET, &b_HTVBFOverMET);
   fChain->SetBranchAddress("HTVBFOverMETTrack", &HTVBFOverMETTrack, &b_HTVBFOverMETTrack);
   fChain->SetBranchAddress("vbfjj_dphi_met", &vbfjj_dphi_met, &b_vbfjj_dphi_met);
   fChain->SetBranchAddress("vbfjj_dphi_mettrack", &vbfjj_dphi_mettrack, &b_vbfjj_dphi_mettrack);
   fChain->SetBranchAddress("MT_VBFMET", &MT_VBFMET, &b_MT_VBFMET);
   fChain->SetBranchAddress("HTVBFOverHTLep", &HTVBFOverHTLep, &b_HTVBFOverHTLep);
   fChain->SetBranchAddress("SMET", &SMET, &b_SMET);
   fChain->SetBranchAddress("SMET_LepInvis", &SMET_LepInvis, &b_SMET_LepInvis);
   fChain->SetBranchAddress("WCR_LepIndex", &WCR_LepIndex, &b_WCR_LepIndex);
   fChain->SetBranchAddress("lep1_DPhi_vbfjj", &lep1_DPhi_vbfjj, &b_lep1_DPhi_vbfjj);
   fChain->SetBranchAddress("lep1_DR_vbfjj", &lep1_DR_vbfjj, &b_lep1_DR_vbfjj);
   fChain->SetBranchAddress("lep1_CenVBF", &lep1_CenVBF, &b_lep1_CenVBF);
   fChain->SetBranchAddress("lep2_DPhi_vbfjj", &lep2_DPhi_vbfjj, &b_lep2_DPhi_vbfjj);
   fChain->SetBranchAddress("lep2_DR_vbfjj", &lep2_DR_vbfjj, &b_lep2_DR_vbfjj);
   fChain->SetBranchAddress("lep2_CenVBF", &lep2_CenVBF, &b_lep2_CenVBF);
   fChain->SetBranchAddress("lep3_DPhi_vbfjj", &lep3_DPhi_vbfjj, &b_lep3_DPhi_vbfjj);
   fChain->SetBranchAddress("lep3_DR_vbfjj", &lep3_DR_vbfjj, &b_lep3_DR_vbfjj);
   fChain->SetBranchAddress("lep3_CenVBF", &lep3_CenVBF, &b_lep3_CenVBF);
   fChain->SetBranchAddress("ZCR_ll_M", &ZCR_ll_M, &b_ZCR_ll_M);
   fChain->SetBranchAddress("ZCR_ll_Pt", &ZCR_ll_Pt, &b_ZCR_ll_Pt);
   fChain->SetBranchAddress("ZCR_ll_DPhiMet", &ZCR_ll_DPhiMet, &b_ZCR_ll_DPhiMet);
   fChain->SetBranchAddress("ZCR_ll_DPhiMet_LepInvis", &ZCR_ll_DPhiMet_LepInvis, &b_ZCR_ll_DPhiMet_LepInvis);
   fChain->SetBranchAddress("ZCR_ll_DPhi_l1l2", &ZCR_ll_DPhi_l1l2, &b_ZCR_ll_DPhi_l1l2);
   fChain->SetBranchAddress("ZCR_ll_DR_l1l2", &ZCR_ll_DR_l1l2, &b_ZCR_ll_DR_l1l2);
   fChain->SetBranchAddress("ZCR_mll_Lep1Index", &ZCR_mll_Lep1Index, &b_ZCR_mll_Lep1Index);
   fChain->SetBranchAddress("ZCR_mll_Lep2Index", &ZCR_mll_Lep2Index, &b_ZCR_mll_Lep2Index);
   fChain->SetBranchAddress("ZCR_mll_flavour", &ZCR_mll_flavour, &b_ZCR_mll_flavour);
   fChain->SetBranchAddress("met_Et_MinusBaseL1", &met_Et_MinusBaseL1, &b_met_Et_MinusBaseL1);
   fChain->SetBranchAddress("met_Et_MinusBaseL1L2", &met_Et_MinusBaseL1L2, &b_met_Et_MinusBaseL1L2);
   fChain->SetBranchAddress("met_Et_LepInvis", &met_Et_LepInvis, &b_met_Et_LepInvis);
   fChain->SetBranchAddress("met_Phi_LepInvis", &met_Phi_LepInvis, &b_met_Phi_LepInvis);
   fChain->SetBranchAddress("met_Signif_LepInvis", &met_Signif_LepInvis, &b_met_Signif_LepInvis);
   fChain->SetBranchAddress("vbfjj_dphi_met_LepInvis", &vbfjj_dphi_met_LepInvis, &b_vbfjj_dphi_met_LepInvis);
   fChain->SetBranchAddress("MT_VBFMET_LepInvis", &MT_VBFMET_LepInvis, &b_MT_VBFMET_LepInvis);
   fChain->SetBranchAddress("HTVBFOverMET_LepInvis", &HTVBFOverMET_LepInvis, &b_HTVBFOverMET_LepInvis);
   fChain->SetBranchAddress("TST_Et_LepInvis", &TST_Et_LepInvis, &b_TST_Et_LepInvis);
   fChain->SetBranchAddress("deltaPhi_MET_TST_Phi_LepInvis", &deltaPhi_MET_TST_Phi_LepInvis, &b_deltaPhi_MET_TST_Phi_LepInvis);
   fChain->SetBranchAddress("vbfRJR_PTVBF", &vbfRJR_PTVBF, &b_vbfRJR_PTVBF);
   fChain->SetBranchAddress("vbfRJR_PTS", &vbfRJR_PTS, &b_vbfRJR_PTS);
   fChain->SetBranchAddress("vbfRJR_RVBF", &vbfRJR_RVBF, &b_vbfRJR_RVBF);
   fChain->SetBranchAddress("vbfRJR_MS", &vbfRJR_MS, &b_vbfRJR_MS);
   fChain->SetBranchAddress("vbfRJR_MVBF", &vbfRJR_MVBF, &b_vbfRJR_MVBF);
   fChain->SetBranchAddress("vbfRJR_dphiVBFI", &vbfRJR_dphiVBFI, &b_vbfRJR_dphiVBFI);
   fChain->SetBranchAddress("NtruthVBFJets", &NtruthVBFJets, &b_NtruthVBFJets);
   fChain->SetBranchAddress("NtruthStatus3Leptons", &NtruthStatus3Leptons, &b_NtruthStatus3Leptons);
   fChain->SetBranchAddress("Truth_vbfPtV", &Truth_vbfPtV, &b_Truth_vbfPtV);
   fChain->SetBranchAddress("Truth_vbfjjM", &Truth_vbfjjM, &b_Truth_vbfjjM);
   fChain->SetBranchAddress("Truth_vbfjjDPhi", &Truth_vbfjjDPhi, &b_Truth_vbfjjDPhi);
   fChain->SetBranchAddress("Truth_j0_pt", &Truth_j0_pt, &b_Truth_j0_pt);
   fChain->SetBranchAddress("Truth_j1_pt", &Truth_j1_pt, &b_Truth_j1_pt);
   fChain->SetBranchAddress("Truth_MET", &Truth_MET, &b_Truth_MET);
   fChain->SetBranchAddress("truthStatus3Lepton1_pdgId", &truthStatus3Lepton1_pdgId, &b_truthStatus3Lepton1_pdgId);
   fChain->SetBranchAddress("truthStatus3Lepton1_Pt", &truthStatus3Lepton1_Pt, &b_truthStatus3Lepton1_Pt);
   fChain->SetBranchAddress("truthStatus3Lepton1_Eta", &truthStatus3Lepton1_Eta, &b_truthStatus3Lepton1_Eta);
   fChain->SetBranchAddress("truthStatus3Lepton1_Phi", &truthStatus3Lepton1_Phi, &b_truthStatus3Lepton1_Phi);
   fChain->SetBranchAddress("truthStatus3Lepton1_E", &truthStatus3Lepton1_E, &b_truthStatus3Lepton1_E);
   fChain->SetBranchAddress("truthStatus3Lepton2_pdgId", &truthStatus3Lepton2_pdgId, &b_truthStatus3Lepton2_pdgId);
   fChain->SetBranchAddress("truthStatus3Lepton2_Pt", &truthStatus3Lepton2_Pt, &b_truthStatus3Lepton2_Pt);
   fChain->SetBranchAddress("truthStatus3Lepton2_Eta", &truthStatus3Lepton2_Eta, &b_truthStatus3Lepton2_Eta);
   fChain->SetBranchAddress("truthStatus3Lepton2_Phi", &truthStatus3Lepton2_Phi, &b_truthStatus3Lepton2_Phi);
   fChain->SetBranchAddress("truthStatus3Lepton2_E", &truthStatus3Lepton2_E, &b_truthStatus3Lepton2_E);
   fChain->SetBranchAddress("truthTauFromW_n", &truthTauFromW_n, &b_truthTauFromW_n);
   fChain->SetBranchAddress("truthTauFromW_DMV", &truthTauFromW_DMV, &b_truthTauFromW_DMV);
   fChain->SetBranchAddress("jetY_Index", &jetY_Index, &b_jetY_Index);
   fChain->SetBranchAddress("jetZ_Index", &jetZ_Index, &b_jetZ_Index);
   fChain->SetBranchAddress("jetY_LepInvis_Index", &jetY_LepInvis_Index, &b_jetY_LepInvis_Index);
   fChain->SetBranchAddress("jetZ_LepInvis_Index", &jetZ_LepInvis_Index, &b_jetZ_LepInvis_Index);
   fChain->SetBranchAddress("jetY_DPhiMet", &jetY_DPhiMet, &b_jetY_DPhiMet);
   fChain->SetBranchAddress("jetZ_DPhiMet", &jetZ_DPhiMet, &b_jetZ_DPhiMet);
   fChain->SetBranchAddress("jetY_LepInvis_DPhiMet", &jetY_LepInvis_DPhiMet, &b_jetY_LepInvis_DPhiMet);
   fChain->SetBranchAddress("jetZ_LepInvis_DPhiMet", &jetZ_LepInvis_DPhiMet, &b_jetZ_LepInvis_DPhiMet);
   fChain->SetBranchAddress("jetY_MT", &jetY_MT, &b_jetY_MT);
   fChain->SetBranchAddress("jetZ_MT", &jetZ_MT, &b_jetZ_MT);
   fChain->SetBranchAddress("jetY_LepInvis_MT", &jetY_LepInvis_MT, &b_jetY_LepInvis_MT);
   fChain->SetBranchAddress("jetZ_LepInvis_MT", &jetZ_LepInvis_MT, &b_jetZ_LepInvis_MT);
   fChain->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
   fChain->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
   fChain->SetBranchAddress("higgsinoTrig2016", &higgsinoTrig2016, &b_higgsinoTrig2016);
   fChain->SetBranchAddress("higgsinoTrig2017", &higgsinoTrig2017, &b_higgsinoTrig2017);
   fChain->SetBranchAddress("trigWeight_singleMuonTrig", &trigWeight_singleMuonTrig, &b_trigWeight_singleMuonTrig);
   fChain->SetBranchAddress("trigMatch_singleMuonTrig", &trigMatch_singleMuonTrig, &b_trigMatch_singleMuonTrig);
   fChain->SetBranchAddress("trigWeight_singleElectronTrig", &trigWeight_singleElectronTrig, &b_trigWeight_singleElectronTrig);
   fChain->SetBranchAddress("trigMatch_singleElectronTrig", &trigMatch_singleElectronTrig, &b_trigMatch_singleElectronTrig);
   fChain->SetBranchAddress("trigWeight_diMuonTrig", &trigWeight_diMuonTrig, &b_trigWeight_diMuonTrig);
   fChain->SetBranchAddress("trigMatch_diMuonTrig", &trigMatch_diMuonTrig, &b_trigMatch_diMuonTrig);
   fChain->SetBranchAddress("trigWeight_diElectronTrig", &trigWeight_diElectronTrig, &b_trigWeight_diElectronTrig);
   fChain->SetBranchAddress("trigMatch_diElectronTrig", &trigMatch_diElectronTrig, &b_trigMatch_diElectronTrig);
   fChain->SetBranchAddress("passEmulTightJetCleaning1Jet", &passEmulTightJetCleaning1Jet, &b_passEmulTightJetCleaning1Jet);
   fChain->SetBranchAddress("passEmulTightJetCleaning2Jet", &passEmulTightJetCleaning2Jet, &b_passEmulTightJetCleaning2Jet);
   fChain->SetBranchAddress("passBadTileJetVeto", &passBadTileJetVeto, &b_passBadTileJetVeto);
   fChain->SetBranchAddress("HLT_2mu4_j85_xe50_mht_emul", &HLT_2mu4_j85_xe50_mht_emul, &b_HLT_2mu4_j85_xe50_mht_emul);
   fChain->SetBranchAddress("HLT_mu4_j125_xe90_mht_emul", &HLT_mu4_j125_xe90_mht_emul, &b_HLT_mu4_j125_xe90_mht_emul);
   fChain->SetBranchAddress("L1_met_Et", &L1_met_Et, &b_L1_met_Et);
   fChain->SetBranchAddress("L1_jet1Pt", &L1_jet1Pt, &b_L1_jet1Pt);
   fChain->SetBranchAddress("HLT_met_pufit_Et", &HLT_met_pufit_Et, &b_HLT_met_pufit_Et);
   fChain->SetBranchAddress("HLT_met_cell_Et", &HLT_met_cell_Et, &b_HLT_met_cell_Et);
   fChain->SetBranchAddress("HLT_jet1Pt", &HLT_jet1Pt, &b_HLT_jet1Pt);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("avg_mu", &avg_mu, &b_avg_mu);
   fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
   fChain->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
   fChain->SetBranchAddress("nEle_base", &nEle_base, &b_nEle_base);
   fChain->SetBranchAddress("nEle_signal", &nEle_signal, &b_nEle_signal);
   fChain->SetBranchAddress("nMu_base", &nMu_base, &b_nMu_base);
   fChain->SetBranchAddress("nMu_signal", &nMu_signal, &b_nMu_signal);
   fChain->SetBranchAddress("nLep_signal_PLTLooseLoose", &nLep_signal_PLTLooseLoose, &b_nLep_signal_PLTLooseLoose);
   fChain->SetBranchAddress("nTau_base", &nTau_base, &b_nTau_base);
   fChain->SetBranchAddress("nTau_signal", &nTau_signal, &b_nTau_signal);
   fChain->SetBranchAddress("tau1Loose", &tau1Loose, &b_tau1Loose);
   fChain->SetBranchAddress("tau1Medium", &tau1Medium, &b_tau1Medium);
   fChain->SetBranchAddress("tau1Tight", &tau1Tight, &b_tau1Tight);
   fChain->SetBranchAddress("tau1Pt", &tau1Pt, &b_tau1Pt);
   fChain->SetBranchAddress("tau1Eta", &tau1Eta, &b_tau1Eta);
   fChain->SetBranchAddress("tau1Phi", &tau1Phi, &b_tau1Phi);
   fChain->SetBranchAddress("tau1M", &tau1M, &b_tau1M);
   fChain->SetBranchAddress("tau1NTracks", &tau1NTracks, &b_tau1NTracks);
   fChain->SetBranchAddress("tau2Loose", &tau2Loose, &b_tau2Loose);
   fChain->SetBranchAddress("tau2Medium", &tau2Medium, &b_tau2Medium);
   fChain->SetBranchAddress("tau2Tight", &tau2Tight, &b_tau2Tight);
   fChain->SetBranchAddress("tau2Pt", &tau2Pt, &b_tau2Pt);
   fChain->SetBranchAddress("tau2Eta", &tau2Eta, &b_tau2Eta);
   fChain->SetBranchAddress("tau2Phi", &tau2Phi, &b_tau2Phi);
   fChain->SetBranchAddress("tau2M", &tau2M, &b_tau2M);
   fChain->SetBranchAddress("tau2NTracks", &tau2NTracks, &b_tau2NTracks);
   fChain->SetBranchAddress("nLep_signal_PLTLooseTight", &nLep_signal_PLTLooseTight, &b_nLep_signal_PLTLooseTight);
   fChain->SetBranchAddress("nLep_signal_PLTTightLoose", &nLep_signal_PLTTightLoose, &b_nLep_signal_PLTTightLoose);
   fChain->SetBranchAddress("nLep_signal_PLTTightTight", &nLep_signal_PLTTightTight, &b_nLep_signal_PLTTightTight);
   fChain->SetBranchAddress("lep1Flavor", &lep1Flavor, &b_lep1Flavor);
   fChain->SetBranchAddress("lep1Charge", &lep1Charge, &b_lep1Charge);
   fChain->SetBranchAddress("lep1Author", &lep1Author, &b_lep1Author);
   fChain->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
   fChain->SetBranchAddress("lep1Eta", &lep1Eta, &b_lep1Eta);
   fChain->SetBranchAddress("lep1Phi", &lep1Phi, &b_lep1Phi);
   fChain->SetBranchAddress("lep1M", &lep1M, &b_lep1M);
   fChain->SetBranchAddress("lep1MT_Met", &lep1MT_Met, &b_lep1MT_Met);
   fChain->SetBranchAddress("lep1MT_Met_LepInvis", &lep1MT_Met_LepInvis, &b_lep1MT_Met_LepInvis);
   fChain->SetBranchAddress("lep1_DPhiMet", &lep1_DPhiMet, &b_lep1_DPhiMet);
   fChain->SetBranchAddress("lep1_DPhiMet_LepInvis", &lep1_DPhiMet_LepInvis, &b_lep1_DPhiMet_LepInvis);
   fChain->SetBranchAddress("lep1D0", &lep1D0, &b_lep1D0);
   fChain->SetBranchAddress("lep1D0Sig", &lep1D0Sig, &b_lep1D0Sig);
   fChain->SetBranchAddress("lep1Z0", &lep1Z0, &b_lep1Z0);
   fChain->SetBranchAddress("lep1Z0SinTheta", &lep1Z0SinTheta, &b_lep1Z0SinTheta);
   fChain->SetBranchAddress("lep1Topoetcone20", &lep1Topoetcone20, &b_lep1Topoetcone20);
   fChain->SetBranchAddress("lep1Topoetcone30", &lep1Topoetcone30, &b_lep1Topoetcone30);
   fChain->SetBranchAddress("lep1Topoetcone40", &lep1Topoetcone40, &b_lep1Topoetcone40);
   fChain->SetBranchAddress("lep1Ptvarcone20", &lep1Ptvarcone20, &b_lep1Ptvarcone20);
   fChain->SetBranchAddress("lep1Ptvarcone30", &lep1Ptvarcone30, &b_lep1Ptvarcone30);
   fChain->SetBranchAddress("lep1Ptvarcone40", &lep1Ptvarcone40, &b_lep1Ptvarcone40);
   fChain->SetBranchAddress("lep1CorrTopoetcone20", &lep1CorrTopoetcone20, &b_lep1CorrTopoetcone20);
   fChain->SetBranchAddress("lep1CorrTopoetcone30", &lep1CorrTopoetcone30, &b_lep1CorrTopoetcone30);
   fChain->SetBranchAddress("lep1CorrTopoetcone40", &lep1CorrTopoetcone40, &b_lep1CorrTopoetcone40);
   fChain->SetBranchAddress("lep1CorrPtvarcone20", &lep1CorrPtvarcone20, &b_lep1CorrPtvarcone20);
   fChain->SetBranchAddress("lep1CorrPtvarcone30", &lep1CorrPtvarcone30, &b_lep1CorrPtvarcone30);
   fChain->SetBranchAddress("lep1CorrPtvarcone40", &lep1CorrPtvarcone40, &b_lep1CorrPtvarcone40);
   fChain->SetBranchAddress("lep1PassOR", &lep1PassOR, &b_lep1PassOR);
   fChain->SetBranchAddress("lep1Type", &lep1Type, &b_lep1Type);
   fChain->SetBranchAddress("lep1Origin", &lep1Origin, &b_lep1Origin);
   fChain->SetBranchAddress("lep1EgMotherType", &lep1EgMotherType, &b_lep1EgMotherType);
   fChain->SetBranchAddress("lep1EgMotherOrigin", &lep1EgMotherOrigin, &b_lep1EgMotherOrigin);
   fChain->SetBranchAddress("lep1NPix", &lep1NPix, &b_lep1NPix);
   fChain->SetBranchAddress("lep1PassBL", &lep1PassBL, &b_lep1PassBL);
   fChain->SetBranchAddress("lep1baseline", &lep1baseline, &b_lep1baseline);
   fChain->SetBranchAddress("lep1VeryLoose", &lep1VeryLoose, &b_lep1VeryLoose);
   fChain->SetBranchAddress("lep1Loose", &lep1Loose, &b_lep1Loose);
   fChain->SetBranchAddress("lep1Medium", &lep1Medium, &b_lep1Medium);
   fChain->SetBranchAddress("lep1Tight", &lep1Tight, &b_lep1Tight);
   fChain->SetBranchAddress("lep1LowPtWP", &lep1LowPtWP, &b_lep1LowPtWP);
   fChain->SetBranchAddress("lep1LooseBLllh", &lep1LooseBLllh, &b_lep1LooseBLllh);
   fChain->SetBranchAddress("lep1IsoHighPtCaloOnly", &lep1IsoHighPtCaloOnly, &b_lep1IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep1IsoHighPtTrackOnly", &lep1IsoHighPtTrackOnly, &b_lep1IsoHighPtTrackOnly);
   fChain->SetBranchAddress("lep1corr_IsoHighPtCaloOnly", &lep1corr_IsoHighPtCaloOnly, &b_lep1corr_IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep1IsoFCHighPtCaloOnly", &lep1IsoFCHighPtCaloOnly, &b_lep1IsoFCHighPtCaloOnly);
   fChain->SetBranchAddress("lep1IsoFCTightTrackOnly", &lep1IsoFCTightTrackOnly, &b_lep1IsoFCTightTrackOnly);
   fChain->SetBranchAddress("lep1IsoFCLoose", &lep1IsoFCLoose, &b_lep1IsoFCLoose);
   fChain->SetBranchAddress("lep1IsoFCTight", &lep1IsoFCTight, &b_lep1IsoFCTight);
   fChain->SetBranchAddress("lep1IsoFCLoose_FixedRad", &lep1IsoFCLoose_FixedRad, &b_lep1IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("lep1IsoFCTight_FixedRad", &lep1IsoFCTight_FixedRad, &b_lep1IsoFCTight_FixedRad);
   fChain->SetBranchAddress("lep1IsoLoose_VarRad", &lep1IsoLoose_VarRad, &b_lep1IsoLoose_VarRad);
   fChain->SetBranchAddress("lep1IsoLoose_FixedRad", &lep1IsoLoose_FixedRad, &b_lep1IsoLoose_FixedRad);
   fChain->SetBranchAddress("lep1IsoTight_VarRad", &lep1IsoTight_VarRad, &b_lep1IsoTight_VarRad);
   fChain->SetBranchAddress("lep1IsoTight_FixedRad", &lep1IsoTight_FixedRad, &b_lep1IsoTight_FixedRad);
   fChain->SetBranchAddress("lep1IsoTightTrackOnly_VarRad", &lep1IsoTightTrackOnly_VarRad, &b_lep1IsoTightTrackOnly_VarRad);
   fChain->SetBranchAddress("lep1IsoTightTrackOnly_FixedRad", &lep1IsoTightTrackOnly_FixedRad, &b_lep1IsoTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("lep1IsoPflowLoose_VarRad", &lep1IsoPflowLoose_VarRad, &b_lep1IsoPflowLoose_VarRad);
   fChain->SetBranchAddress("lep1IsoPflowLoose_FixedRad", &lep1IsoPflowLoose_FixedRad, &b_lep1IsoPflowLoose_FixedRad);
   fChain->SetBranchAddress("lep1IsoPflowTight_VarRad", &lep1IsoPflowTight_VarRad, &b_lep1IsoPflowTight_VarRad);
   fChain->SetBranchAddress("lep1IsoPflowTight_FixedRad", &lep1IsoPflowTight_FixedRad, &b_lep1IsoPflowTight_FixedRad);
   fChain->SetBranchAddress("lep1IsoPLVLoose", &lep1IsoPLVLoose, &b_lep1IsoPLVLoose);
   fChain->SetBranchAddress("lep1IsoPLVTight", &lep1IsoPLVTight, &b_lep1IsoPLVTight);
   fChain->SetBranchAddress("lep1Signal", &lep1Signal, &b_lep1Signal);
   fChain->SetBranchAddress("lep1SignalPLTLoose", &lep1SignalPLTLoose, &b_lep1SignalPLTLoose);
   fChain->SetBranchAddress("lep1SignalPLTTight", &lep1SignalPLTTight, &b_lep1SignalPLTTight);
   fChain->SetBranchAddress("lep1TruthMatched", &lep1TruthMatched, &b_lep1TruthMatched);
   fChain->SetBranchAddress("lep1TruthCharge", &lep1TruthCharge, &b_lep1TruthCharge);
   fChain->SetBranchAddress("lep1TruthPt", &lep1TruthPt, &b_lep1TruthPt);
   fChain->SetBranchAddress("lep1TruthEta", &lep1TruthEta, &b_lep1TruthEta);
   fChain->SetBranchAddress("lep1TruthPhi", &lep1TruthPhi, &b_lep1TruthPhi);
   fChain->SetBranchAddress("lep1TruthM", &lep1TruthM, &b_lep1TruthM);
   fChain->SetBranchAddress("lep1TrackPt", &lep1TrackPt, &b_lep1TrackPt);
   fChain->SetBranchAddress("lep1TrackEta", &lep1TrackEta, &b_lep1TrackEta);
   fChain->SetBranchAddress("lep1TrackPhi", &lep1TrackPhi, &b_lep1TrackPhi);
   fChain->SetBranchAddress("lep1Weight", &lep1Weight, &b_lep1Weight);
   fChain->SetBranchAddress("lep1PromptLepVeto_score", &lep1PromptLepVeto_score, &b_lep1PromptLepVeto_score);
   fChain->SetBranchAddress("lep1PLTInput_TrackJetNTrack", &lep1PLTInput_TrackJetNTrack, &b_lep1PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep1PLTInput_DRlj", &lep1PLTInput_DRlj, &b_lep1PLTInput_DRlj);
   fChain->SetBranchAddress("lep1PLTInput_rnnip", &lep1PLTInput_rnnip, &b_lep1PLTInput_rnnip);
   fChain->SetBranchAddress("lep1PLTInput_DL1mu", &lep1PLTInput_DL1mu, &b_lep1PLTInput_DL1mu);
   fChain->SetBranchAddress("lep1PLTInput_PtRel", &lep1PLTInput_PtRel, &b_lep1PLTInput_PtRel);
   fChain->SetBranchAddress("lep1PLTInput_PtFrac", &lep1PLTInput_PtFrac, &b_lep1PLTInput_PtFrac);
   fChain->SetBranchAddress("lep1CorrPLTInput_TrackJetNTrack", &lep1CorrPLTInput_TrackJetNTrack, &b_lep1CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep1CorrPLTInput_DRlj", &lep1CorrPLTInput_DRlj, &b_lep1CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep1CorrPLTInput_rnnip", &lep1CorrPLTInput_rnnip, &b_lep1CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep1CorrPLTInput_DL1mu", &lep1CorrPLTInput_DL1mu, &b_lep1CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep1CorrPLTInput_PtRel", &lep1CorrPLTInput_PtRel, &b_lep1CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep1CorrPLTInput_PtFrac", &lep1CorrPLTInput_PtFrac, &b_lep1CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep1CorrPromptLepVeto", &lep1CorrPromptLepVeto, &b_lep1CorrPromptLepVeto);
   fChain->SetBranchAddress("lep1LowPtPLT", &lep1LowPtPLT, &b_lep1LowPtPLT);
   fChain->SetBranchAddress("lep1LowPtPLT_Cone40", &lep1LowPtPLT_Cone40, &b_lep1LowPtPLT_Cone40);
   fChain->SetBranchAddress("lep1IsoLowPtPLT_Loose", &lep1IsoLowPtPLT_Loose, &b_lep1IsoLowPtPLT_Loose);
   fChain->SetBranchAddress("lep1IsoLowPtPLT_Tight", &lep1IsoLowPtPLT_Tight, &b_lep1IsoLowPtPLT_Tight);
   fChain->SetBranchAddress("lep1IsoLowPtPLT_MediumLH_Loose", &lep1IsoLowPtPLT_MediumLH_Loose, &b_lep1IsoLowPtPLT_MediumLH_Loose);
   fChain->SetBranchAddress("lep1IsoLowPtPLT_MediumLH_Tight", &lep1IsoLowPtPLT_MediumLH_Tight, &b_lep1IsoLowPtPLT_MediumLH_Tight);
   fChain->SetBranchAddress("lep1EWKCombiBaseline", &lep1EWKCombiBaseline, &b_lep1EWKCombiBaseline);
   fChain->SetBranchAddress("lep1PassZCR", &lep1PassZCR, &b_lep1PassZCR);
   fChain->SetBranchAddress("lep1PassWCR", &lep1PassWCR, &b_lep1PassWCR);
   fChain->SetBranchAddress("lep2Flavor", &lep2Flavor, &b_lep2Flavor);
   fChain->SetBranchAddress("lep2Charge", &lep2Charge, &b_lep2Charge);
   fChain->SetBranchAddress("lep2Author", &lep2Author, &b_lep2Author);
   fChain->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
   fChain->SetBranchAddress("lep2Eta", &lep2Eta, &b_lep2Eta);
   fChain->SetBranchAddress("lep2Phi", &lep2Phi, &b_lep2Phi);
   fChain->SetBranchAddress("lep2M", &lep2M, &b_lep2M);
   fChain->SetBranchAddress("lep2MT_Met", &lep2MT_Met, &b_lep2MT_Met);
   fChain->SetBranchAddress("lep2MT_Met_LepInvis", &lep2MT_Met_LepInvis, &b_lep2MT_Met_LepInvis);
   fChain->SetBranchAddress("lep2_DPhiMet", &lep2_DPhiMet, &b_lep2_DPhiMet);
   fChain->SetBranchAddress("lep2_DPhiMet_LepInvis", &lep2_DPhiMet_LepInvis, &b_lep2_DPhiMet_LepInvis);
   fChain->SetBranchAddress("lep2D0", &lep2D0, &b_lep2D0);
   fChain->SetBranchAddress("lep2D0Sig", &lep2D0Sig, &b_lep2D0Sig);
   fChain->SetBranchAddress("lep2Z0", &lep2Z0, &b_lep2Z0);
   fChain->SetBranchAddress("lep2Z0SinTheta", &lep2Z0SinTheta, &b_lep2Z0SinTheta);
   fChain->SetBranchAddress("lep2Topoetcone20", &lep2Topoetcone20, &b_lep2Topoetcone20);
   fChain->SetBranchAddress("lep2Topoetcone30", &lep2Topoetcone30, &b_lep2Topoetcone30);
   fChain->SetBranchAddress("lep2Topoetcone40", &lep2Topoetcone40, &b_lep2Topoetcone40);
   fChain->SetBranchAddress("lep2Ptvarcone20", &lep2Ptvarcone20, &b_lep2Ptvarcone20);
   fChain->SetBranchAddress("lep2Ptvarcone30", &lep2Ptvarcone30, &b_lep2Ptvarcone30);
   fChain->SetBranchAddress("lep2Ptvarcone40", &lep2Ptvarcone40, &b_lep2Ptvarcone40);
   fChain->SetBranchAddress("lep2CorrTopoetcone20", &lep2CorrTopoetcone20, &b_lep2CorrTopoetcone20);
   fChain->SetBranchAddress("lep2CorrTopoetcone30", &lep2CorrTopoetcone30, &b_lep2CorrTopoetcone30);
   fChain->SetBranchAddress("lep2CorrTopoetcone40", &lep2CorrTopoetcone40, &b_lep2CorrTopoetcone40);
   fChain->SetBranchAddress("lep2CorrPtvarcone20", &lep2CorrPtvarcone20, &b_lep2CorrPtvarcone20);
   fChain->SetBranchAddress("lep2CorrPtvarcone30", &lep2CorrPtvarcone30, &b_lep2CorrPtvarcone30);
   fChain->SetBranchAddress("lep2CorrPtvarcone40", &lep2CorrPtvarcone40, &b_lep2CorrPtvarcone40);
   fChain->SetBranchAddress("lep2PassOR", &lep2PassOR, &b_lep2PassOR);
   fChain->SetBranchAddress("lep2Type", &lep2Type, &b_lep2Type);
   fChain->SetBranchAddress("lep2Origin", &lep2Origin, &b_lep2Origin);
   fChain->SetBranchAddress("lep2EgMotherType", &lep2EgMotherType, &b_lep2EgMotherType);
   fChain->SetBranchAddress("lep2EgMotherOrigin", &lep2EgMotherOrigin, &b_lep2EgMotherOrigin);
   fChain->SetBranchAddress("lep2NPix", &lep2NPix, &b_lep2NPix);
   fChain->SetBranchAddress("lep2PassBL", &lep2PassBL, &b_lep2PassBL);
   fChain->SetBranchAddress("lep2baseline", &lep2baseline, &b_lep2baseline);
   fChain->SetBranchAddress("lep2VeryLoose", &lep2VeryLoose, &b_lep2VeryLoose);
   fChain->SetBranchAddress("lep2Loose", &lep2Loose, &b_lep2Loose);
   fChain->SetBranchAddress("lep2Medium", &lep2Medium, &b_lep2Medium);
   fChain->SetBranchAddress("lep2Tight", &lep2Tight, &b_lep2Tight);
   fChain->SetBranchAddress("lep2LowPtWP", &lep2LowPtWP, &b_lep2LowPtWP);
   fChain->SetBranchAddress("lep2LooseBLllh", &lep2LooseBLllh, &b_lep2LooseBLllh);
   fChain->SetBranchAddress("lep2IsoHighPtCaloOnly", &lep2IsoHighPtCaloOnly, &b_lep2IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep2IsoHighPtTrackOnly", &lep2IsoHighPtTrackOnly, &b_lep2IsoHighPtTrackOnly);
   fChain->SetBranchAddress("lep2corr_IsoHighPtCaloOnly", &lep2corr_IsoHighPtCaloOnly, &b_lep2corr_IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep2IsoFCHighPtCaloOnly", &lep2IsoFCHighPtCaloOnly, &b_lep2IsoFCHighPtCaloOnly);
   fChain->SetBranchAddress("lep2IsoFCTightTrackOnly", &lep2IsoFCTightTrackOnly, &b_lep2IsoFCTightTrackOnly);
   fChain->SetBranchAddress("lep2IsoFCLoose", &lep2IsoFCLoose, &b_lep2IsoFCLoose);
   fChain->SetBranchAddress("lep2IsoFCTight", &lep2IsoFCTight, &b_lep2IsoFCTight);
   fChain->SetBranchAddress("lep2IsoFCLoose_FixedRad", &lep2IsoFCLoose_FixedRad, &b_lep2IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("lep2IsoFCTight_FixedRad", &lep2IsoFCTight_FixedRad, &b_lep2IsoFCTight_FixedRad);
   fChain->SetBranchAddress("lep2IsoLoose_VarRad", &lep2IsoLoose_VarRad, &b_lep2IsoLoose_VarRad);
   fChain->SetBranchAddress("lep2IsoLoose_FixedRad", &lep2IsoLoose_FixedRad, &b_lep2IsoLoose_FixedRad);
   fChain->SetBranchAddress("lep2IsoTight_VarRad", &lep2IsoTight_VarRad, &b_lep2IsoTight_VarRad);
   fChain->SetBranchAddress("lep2IsoTight_FixedRad", &lep2IsoTight_FixedRad, &b_lep2IsoTight_FixedRad);
   fChain->SetBranchAddress("lep2IsoTightTrackOnly_VarRad", &lep2IsoTightTrackOnly_VarRad, &b_lep2IsoTightTrackOnly_VarRad);
   fChain->SetBranchAddress("lep2IsoTightTrackOnly_FixedRad", &lep2IsoTightTrackOnly_FixedRad, &b_lep2IsoTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("lep2IsoPflowLoose_VarRad", &lep2IsoPflowLoose_VarRad, &b_lep2IsoPflowLoose_VarRad);
   fChain->SetBranchAddress("lep2IsoPflowLoose_FixedRad", &lep2IsoPflowLoose_FixedRad, &b_lep2IsoPflowLoose_FixedRad);
   fChain->SetBranchAddress("lep2IsoPflowTight_VarRad", &lep2IsoPflowTight_VarRad, &b_lep2IsoPflowTight_VarRad);
   fChain->SetBranchAddress("lep2IsoPflowTight_FixedRad", &lep2IsoPflowTight_FixedRad, &b_lep2IsoPflowTight_FixedRad);
   fChain->SetBranchAddress("lep2IsoPLVLoose", &lep2IsoPLVLoose, &b_lep2IsoPLVLoose);
   fChain->SetBranchAddress("lep2IsoPLVTight", &lep2IsoPLVTight, &b_lep2IsoPLVTight);
   fChain->SetBranchAddress("lep2Signal", &lep2Signal, &b_lep2Signal);
   fChain->SetBranchAddress("lep2SignalPLTLoose", &lep2SignalPLTLoose, &b_lep2SignalPLTLoose);
   fChain->SetBranchAddress("lep2SignalPLTTight", &lep2SignalPLTTight, &b_lep2SignalPLTTight);
   fChain->SetBranchAddress("lep2TruthMatched", &lep2TruthMatched, &b_lep2TruthMatched);
   fChain->SetBranchAddress("lep2TruthCharge", &lep2TruthCharge, &b_lep2TruthCharge);
   fChain->SetBranchAddress("lep2TruthPt", &lep2TruthPt, &b_lep2TruthPt);
   fChain->SetBranchAddress("lep2TruthEta", &lep2TruthEta, &b_lep2TruthEta);
   fChain->SetBranchAddress("lep2TruthPhi", &lep2TruthPhi, &b_lep2TruthPhi);
   fChain->SetBranchAddress("lep2TruthM", &lep2TruthM, &b_lep2TruthM);
   fChain->SetBranchAddress("lep2TrackPt", &lep2TrackPt, &b_lep2TrackPt);
   fChain->SetBranchAddress("lep2TrackEta", &lep2TrackEta, &b_lep2TrackEta);
   fChain->SetBranchAddress("lep2TrackPhi", &lep2TrackPhi, &b_lep2TrackPhi);
   fChain->SetBranchAddress("lep2Weight", &lep2Weight, &b_lep2Weight);
   fChain->SetBranchAddress("lep2PromptLepVeto_score", &lep2PromptLepVeto_score, &b_lep2PromptLepVeto_score);
   fChain->SetBranchAddress("lep2PLTInput_TrackJetNTrack", &lep2PLTInput_TrackJetNTrack, &b_lep2PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep2PLTInput_DRlj", &lep2PLTInput_DRlj, &b_lep2PLTInput_DRlj);
   fChain->SetBranchAddress("lep2PLTInput_rnnip", &lep2PLTInput_rnnip, &b_lep2PLTInput_rnnip);
   fChain->SetBranchAddress("lep2PLTInput_DL1mu", &lep2PLTInput_DL1mu, &b_lep2PLTInput_DL1mu);
   fChain->SetBranchAddress("lep2PLTInput_PtRel", &lep2PLTInput_PtRel, &b_lep2PLTInput_PtRel);
   fChain->SetBranchAddress("lep2PLTInput_PtFrac", &lep2PLTInput_PtFrac, &b_lep2PLTInput_PtFrac);
   fChain->SetBranchAddress("lep2CorrPLTInput_TrackJetNTrack", &lep2CorrPLTInput_TrackJetNTrack, &b_lep2CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep2CorrPLTInput_DRlj", &lep2CorrPLTInput_DRlj, &b_lep2CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep2CorrPLTInput_rnnip", &lep2CorrPLTInput_rnnip, &b_lep2CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep2CorrPLTInput_DL1mu", &lep2CorrPLTInput_DL1mu, &b_lep2CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep2CorrPLTInput_PtRel", &lep2CorrPLTInput_PtRel, &b_lep2CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep2CorrPLTInput_PtFrac", &lep2CorrPLTInput_PtFrac, &b_lep2CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep2CorrPromptLepVeto", &lep2CorrPromptLepVeto, &b_lep2CorrPromptLepVeto);
   fChain->SetBranchAddress("lep2LowPtPLT", &lep2LowPtPLT, &b_lep2LowPtPLT);
   fChain->SetBranchAddress("lep2LowPtPLT_Cone40", &lep2LowPtPLT_Cone40, &b_lep2LowPtPLT_Cone40);
   fChain->SetBranchAddress("lep2IsoLowPtPLT_Loose", &lep2IsoLowPtPLT_Loose, &b_lep2IsoLowPtPLT_Loose);
   fChain->SetBranchAddress("lep2IsoLowPtPLT_Tight", &lep2IsoLowPtPLT_Tight, &b_lep2IsoLowPtPLT_Tight);
   fChain->SetBranchAddress("lep2IsoLowPtPLT_MediumLH_Loose", &lep2IsoLowPtPLT_MediumLH_Loose, &b_lep2IsoLowPtPLT_MediumLH_Loose);
   fChain->SetBranchAddress("lep2IsoLowPtPLT_MediumLH_Tight", &lep2IsoLowPtPLT_MediumLH_Tight, &b_lep2IsoLowPtPLT_MediumLH_Tight);
   fChain->SetBranchAddress("lep2EWKCombiBaseline", &lep2EWKCombiBaseline, &b_lep2EWKCombiBaseline);
   fChain->SetBranchAddress("lep2PassZCR", &lep2PassZCR, &b_lep2PassZCR);
   fChain->SetBranchAddress("lep2PassWCR", &lep2PassWCR, &b_lep2PassWCR);
   fChain->SetBranchAddress("lep3Flavor", &lep3Flavor, &b_lep3Flavor);
   fChain->SetBranchAddress("lep3Charge", &lep3Charge, &b_lep3Charge);
   fChain->SetBranchAddress("lep3Author", &lep3Author, &b_lep3Author);
   fChain->SetBranchAddress("lep3Pt", &lep3Pt, &b_lep3Pt);
   fChain->SetBranchAddress("lep3Eta", &lep3Eta, &b_lep3Eta);
   fChain->SetBranchAddress("lep3Phi", &lep3Phi, &b_lep3Phi);
   fChain->SetBranchAddress("lep3M", &lep3M, &b_lep3M);
   fChain->SetBranchAddress("lep3MT_Met", &lep3MT_Met, &b_lep3MT_Met);
   fChain->SetBranchAddress("lep3MT_Met_LepInvis", &lep3MT_Met_LepInvis, &b_lep3MT_Met_LepInvis);
   fChain->SetBranchAddress("lep3_DPhiMet", &lep3_DPhiMet, &b_lep3_DPhiMet);
   fChain->SetBranchAddress("lep3_DPhiMet_LepInvis", &lep3_DPhiMet_LepInvis, &b_lep3_DPhiMet_LepInvis);
   fChain->SetBranchAddress("lep3D0", &lep3D0, &b_lep3D0);
   fChain->SetBranchAddress("lep3D0Sig", &lep3D0Sig, &b_lep3D0Sig);
   fChain->SetBranchAddress("lep3Z0", &lep3Z0, &b_lep3Z0);
   fChain->SetBranchAddress("lep3Z0SinTheta", &lep3Z0SinTheta, &b_lep3Z0SinTheta);
   fChain->SetBranchAddress("lep3Topoetcone20", &lep3Topoetcone20, &b_lep3Topoetcone20);
   fChain->SetBranchAddress("lep3Topoetcone30", &lep3Topoetcone30, &b_lep3Topoetcone30);
   fChain->SetBranchAddress("lep3Topoetcone40", &lep3Topoetcone40, &b_lep3Topoetcone40);
   fChain->SetBranchAddress("lep3Ptvarcone20", &lep3Ptvarcone20, &b_lep3Ptvarcone20);
   fChain->SetBranchAddress("lep3Ptvarcone30", &lep3Ptvarcone30, &b_lep3Ptvarcone30);
   fChain->SetBranchAddress("lep3Ptvarcone40", &lep3Ptvarcone40, &b_lep3Ptvarcone40);
   fChain->SetBranchAddress("lep3CorrTopoetcone20", &lep3CorrTopoetcone20, &b_lep3CorrTopoetcone20);
   fChain->SetBranchAddress("lep3CorrTopoetcone30", &lep3CorrTopoetcone30, &b_lep3CorrTopoetcone30);
   fChain->SetBranchAddress("lep3CorrTopoetcone40", &lep3CorrTopoetcone40, &b_lep3CorrTopoetcone40);
   fChain->SetBranchAddress("lep3CorrPtvarcone20", &lep3CorrPtvarcone20, &b_lep3CorrPtvarcone20);
   fChain->SetBranchAddress("lep3CorrPtvarcone30", &lep3CorrPtvarcone30, &b_lep3CorrPtvarcone30);
   fChain->SetBranchAddress("lep3CorrPtvarcone40", &lep3CorrPtvarcone40, &b_lep3CorrPtvarcone40);
   fChain->SetBranchAddress("lep3PassOR", &lep3PassOR, &b_lep3PassOR);
   fChain->SetBranchAddress("lep3Type", &lep3Type, &b_lep3Type);
   fChain->SetBranchAddress("lep3Origin", &lep3Origin, &b_lep3Origin);
   fChain->SetBranchAddress("lep3EgMotherType", &lep3EgMotherType, &b_lep3EgMotherType);
   fChain->SetBranchAddress("lep3EgMotherOrigin", &lep3EgMotherOrigin, &b_lep3EgMotherOrigin);
   fChain->SetBranchAddress("lep3NPix", &lep3NPix, &b_lep3NPix);
   fChain->SetBranchAddress("lep3PassBL", &lep3PassBL, &b_lep3PassBL);
   fChain->SetBranchAddress("lep3baseline", &lep3baseline, &b_lep3baseline);
   fChain->SetBranchAddress("lep3VeryLoose", &lep3VeryLoose, &b_lep3VeryLoose);
   fChain->SetBranchAddress("lep3Loose", &lep3Loose, &b_lep3Loose);
   fChain->SetBranchAddress("lep3Medium", &lep3Medium, &b_lep3Medium);
   fChain->SetBranchAddress("lep3Tight", &lep3Tight, &b_lep3Tight);
   fChain->SetBranchAddress("lep3LowPtWP", &lep3LowPtWP, &b_lep3LowPtWP);
   fChain->SetBranchAddress("lep3LooseBLllh", &lep3LooseBLllh, &b_lep3LooseBLllh);
   fChain->SetBranchAddress("lep3IsoHighPtCaloOnly", &lep3IsoHighPtCaloOnly, &b_lep3IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep3IsoHighPtTrackOnly", &lep3IsoHighPtTrackOnly, &b_lep3IsoHighPtTrackOnly);
   fChain->SetBranchAddress("lep3corr_IsoHighPtCaloOnly", &lep3corr_IsoHighPtCaloOnly, &b_lep3corr_IsoHighPtCaloOnly);
   fChain->SetBranchAddress("lep3IsoFCHighPtCaloOnly", &lep3IsoFCHighPtCaloOnly, &b_lep3IsoFCHighPtCaloOnly);
   fChain->SetBranchAddress("lep3IsoFCTightTrackOnly", &lep3IsoFCTightTrackOnly, &b_lep3IsoFCTightTrackOnly);
   fChain->SetBranchAddress("lep3IsoFCLoose", &lep3IsoFCLoose, &b_lep3IsoFCLoose);
   fChain->SetBranchAddress("lep3IsoFCTight", &lep3IsoFCTight, &b_lep3IsoFCTight);
   fChain->SetBranchAddress("lep3IsoFCLoose_FixedRad", &lep3IsoFCLoose_FixedRad, &b_lep3IsoFCLoose_FixedRad);
   fChain->SetBranchAddress("lep3IsoFCTight_FixedRad", &lep3IsoFCTight_FixedRad, &b_lep3IsoFCTight_FixedRad);
   fChain->SetBranchAddress("lep3IsoLoose_VarRad", &lep3IsoLoose_VarRad, &b_lep3IsoLoose_VarRad);
   fChain->SetBranchAddress("lep3IsoLoose_FixedRad", &lep3IsoLoose_FixedRad, &b_lep3IsoLoose_FixedRad);
   fChain->SetBranchAddress("lep3IsoTight_VarRad", &lep3IsoTight_VarRad, &b_lep3IsoTight_VarRad);
   fChain->SetBranchAddress("lep3IsoTight_FixedRad", &lep3IsoTight_FixedRad, &b_lep3IsoTight_FixedRad);
   fChain->SetBranchAddress("lep3IsoTightTrackOnly_VarRad", &lep3IsoTightTrackOnly_VarRad, &b_lep3IsoTightTrackOnly_VarRad);
   fChain->SetBranchAddress("lep3IsoTightTrackOnly_FixedRad", &lep3IsoTightTrackOnly_FixedRad, &b_lep3IsoTightTrackOnly_FixedRad);
   fChain->SetBranchAddress("lep3IsoPflowLoose_VarRad", &lep3IsoPflowLoose_VarRad, &b_lep3IsoPflowLoose_VarRad);
   fChain->SetBranchAddress("lep3IsoPflowLoose_FixedRad", &lep3IsoPflowLoose_FixedRad, &b_lep3IsoPflowLoose_FixedRad);
   fChain->SetBranchAddress("lep3IsoPflowTight_VarRad", &lep3IsoPflowTight_VarRad, &b_lep3IsoPflowTight_VarRad);
   fChain->SetBranchAddress("lep3IsoPflowTight_FixedRad", &lep3IsoPflowTight_FixedRad, &b_lep3IsoPflowTight_FixedRad);
   fChain->SetBranchAddress("lep3IsoPLVLoose", &lep3IsoPLVLoose, &b_lep3IsoPLVLoose);
   fChain->SetBranchAddress("lep3IsoPLVTight", &lep3IsoPLVTight, &b_lep3IsoPLVTight);
   fChain->SetBranchAddress("lep3Signal", &lep3Signal, &b_lep3Signal);
   fChain->SetBranchAddress("lep3SignalPLTLoose", &lep3SignalPLTLoose, &b_lep3SignalPLTLoose);
   fChain->SetBranchAddress("lep3SignalPLTTight", &lep3SignalPLTTight, &b_lep3SignalPLTTight);
   fChain->SetBranchAddress("lep3TruthMatched", &lep3TruthMatched, &b_lep3TruthMatched);
   fChain->SetBranchAddress("lep3TruthCharge", &lep3TruthCharge, &b_lep3TruthCharge);
   fChain->SetBranchAddress("lep3TruthPt", &lep3TruthPt, &b_lep3TruthPt);
   fChain->SetBranchAddress("lep3TruthEta", &lep3TruthEta, &b_lep3TruthEta);
   fChain->SetBranchAddress("lep3TruthPhi", &lep3TruthPhi, &b_lep3TruthPhi);
   fChain->SetBranchAddress("lep3TruthM", &lep3TruthM, &b_lep3TruthM);
   fChain->SetBranchAddress("lep3TrackPt", &lep3TrackPt, &b_lep3TrackPt);
   fChain->SetBranchAddress("lep3TrackEta", &lep3TrackEta, &b_lep3TrackEta);
   fChain->SetBranchAddress("lep3TrackPhi", &lep3TrackPhi, &b_lep3TrackPhi);
   fChain->SetBranchAddress("lep3Weight", &lep3Weight, &b_lep3Weight);
   fChain->SetBranchAddress("lep3PromptLepVeto_score", &lep3PromptLepVeto_score, &b_lep3PromptLepVeto_score);
   fChain->SetBranchAddress("lep3PLTInput_TrackJetNTrack", &lep3PLTInput_TrackJetNTrack, &b_lep3PLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep3PLTInput_DRlj", &lep3PLTInput_DRlj, &b_lep3PLTInput_DRlj);
   fChain->SetBranchAddress("lep3PLTInput_rnnip", &lep3PLTInput_rnnip, &b_lep3PLTInput_rnnip);
   fChain->SetBranchAddress("lep3PLTInput_DL1mu", &lep3PLTInput_DL1mu, &b_lep3PLTInput_DL1mu);
   fChain->SetBranchAddress("lep3PLTInput_PtRel", &lep3PLTInput_PtRel, &b_lep3PLTInput_PtRel);
   fChain->SetBranchAddress("lep3PLTInput_PtFrac", &lep3PLTInput_PtFrac, &b_lep3PLTInput_PtFrac);
   fChain->SetBranchAddress("lep3CorrPLTInput_TrackJetNTrack", &lep3CorrPLTInput_TrackJetNTrack, &b_lep3CorrPLTInput_TrackJetNTrack);
   fChain->SetBranchAddress("lep3CorrPLTInput_DRlj", &lep3CorrPLTInput_DRlj, &b_lep3CorrPLTInput_DRlj);
   fChain->SetBranchAddress("lep3CorrPLTInput_rnnip", &lep3CorrPLTInput_rnnip, &b_lep3CorrPLTInput_rnnip);
   fChain->SetBranchAddress("lep3CorrPLTInput_DL1mu", &lep3CorrPLTInput_DL1mu, &b_lep3CorrPLTInput_DL1mu);
   fChain->SetBranchAddress("lep3CorrPLTInput_PtRel", &lep3CorrPLTInput_PtRel, &b_lep3CorrPLTInput_PtRel);
   fChain->SetBranchAddress("lep3CorrPLTInput_PtFrac", &lep3CorrPLTInput_PtFrac, &b_lep3CorrPLTInput_PtFrac);
   fChain->SetBranchAddress("lep3CorrPromptLepVeto", &lep3CorrPromptLepVeto, &b_lep3CorrPromptLepVeto);
   fChain->SetBranchAddress("lep3LowPtPLT", &lep3LowPtPLT, &b_lep3LowPtPLT);
   fChain->SetBranchAddress("lep3LowPtPLT_Cone40", &lep3LowPtPLT_Cone40, &b_lep3LowPtPLT_Cone40);
   fChain->SetBranchAddress("lep3IsoLowPtPLT_Loose", &lep3IsoLowPtPLT_Loose, &b_lep3IsoLowPtPLT_Loose);
   fChain->SetBranchAddress("lep3IsoLowPtPLT_Tight", &lep3IsoLowPtPLT_Tight, &b_lep3IsoLowPtPLT_Tight);
   fChain->SetBranchAddress("lep3IsoLowPtPLT_MediumLH_Loose", &lep3IsoLowPtPLT_MediumLH_Loose, &b_lep3IsoLowPtPLT_MediumLH_Loose);
   fChain->SetBranchAddress("lep3IsoLowPtPLT_MediumLH_Tight", &lep3IsoLowPtPLT_MediumLH_Tight, &b_lep3IsoLowPtPLT_MediumLH_Tight);
   fChain->SetBranchAddress("lep3EWKCombiBaseline", &lep3EWKCombiBaseline, &b_lep3EWKCombiBaseline);
   fChain->SetBranchAddress("lep3PassZCR", &lep3PassZCR, &b_lep3PassZCR);
   fChain->SetBranchAddress("lep3PassWCR", &lep3PassWCR, &b_lep3PassWCR);
   fChain->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
   fChain->SetBranchAddress("nJet25", &nJet25, &b_nJet25);
   fChain->SetBranchAddress("nJet20", &nJet20, &b_nJet20);
   fChain->SetBranchAddress("nBJet30", &nBJet30, &b_nBJet30);
   fChain->SetBranchAddress("nBJet25", &nBJet25, &b_nBJet25);
   fChain->SetBranchAddress("nBJet20", &nBJet20, &b_nBJet20);
   fChain->SetBranchAddress("nTotalJet", &nTotalJet, &b_nTotalJet);
   fChain->SetBranchAddress("nTotalJet20", &nTotalJet20, &b_nTotalJet20);
   fChain->SetBranchAddress("DecayModeTTbar", &DecayModeTTbar, &b_DecayModeTTbar);
   fChain->SetBranchAddress("met_Et", &met_Et, &b_met_Et);
   fChain->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
   fChain->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
   fChain->SetBranchAddress("TST_Et", &TST_Et, &b_TST_Et);
   fChain->SetBranchAddress("TST_Phi", &TST_Phi, &b_TST_Phi);
   fChain->SetBranchAddress("met_track_Et", &met_track_Et, &b_met_track_Et);
   fChain->SetBranchAddress("met_track_Phi", &met_track_Phi, &b_met_track_Phi);
   fChain->SetBranchAddress("deltaPhi_MET_TST_Phi", &deltaPhi_MET_TST_Phi, &b_deltaPhi_MET_TST_Phi);
   fChain->SetBranchAddress("met_muons_invis_Et", &met_muons_invis_Et, &b_met_muons_invis_Et);
   fChain->SetBranchAddress("met_muons_invis_Phi", &met_muons_invis_Phi, &b_met_muons_invis_Phi);
   fChain->SetBranchAddress("meffInc30", &meffInc30, &b_meffInc30);
   fChain->SetBranchAddress("Ht30", &Ht30, &b_Ht30);
   fChain->SetBranchAddress("LepAplanarity", &LepAplanarity, &b_LepAplanarity);
   fChain->SetBranchAddress("JetAplanarity", &JetAplanarity, &b_JetAplanarity);
   fChain->SetBranchAddress("mjj", &mjj, &b_mjj);
   fChain->SetBranchAddress("dEtajj", &dEtajj, &b_dEtajj);
   fChain->SetBranchAddress("dPhijj", &dPhijj, &b_dPhijj);
   fChain->SetBranchAddress("mt_lep1", &mt_lep1, &b_mt_lep1);
   fChain->SetBranchAddress("mt_lep2", &mt_lep2, &b_mt_lep2);
   fChain->SetBranchAddress("mt_lep3", &mt_lep3, &b_mt_lep3);
   fChain->SetBranchAddress("mt_lep1_metTrack", &mt_lep1_metTrack, &b_mt_lep1_metTrack);
   fChain->SetBranchAddress("mt_lep2_metTrack", &mt_lep2_metTrack, &b_mt_lep2_metTrack);
   fChain->SetBranchAddress("mt_lep3_metTrack", &mt_lep3_metTrack, &b_mt_lep3_metTrack);
   fChain->SetBranchAddress("METOverHT", &METOverHT, &b_METOverHT);
   fChain->SetBranchAddress("METOverJ1pT", &METOverJ1pT, &b_METOverJ1pT);
   fChain->SetBranchAddress("METTrackOverHT", &METTrackOverHT, &b_METTrackOverHT);
   fChain->SetBranchAddress("METTrackOverJ1pT", &METTrackOverJ1pT, &b_METTrackOverJ1pT);
   fChain->SetBranchAddress("DPhiJ1Met", &DPhiJ1Met, &b_DPhiJ1Met);
   fChain->SetBranchAddress("DPhiJ2Met", &DPhiJ2Met, &b_DPhiJ2Met);
   fChain->SetBranchAddress("DPhiJ3Met", &DPhiJ3Met, &b_DPhiJ3Met);
   fChain->SetBranchAddress("DPhiJ4Met", &DPhiJ4Met, &b_DPhiJ4Met);
   fChain->SetBranchAddress("minDPhiCenJetsMet", &minDPhiCenJetsMet, &b_minDPhiCenJetsMet);
   fChain->SetBranchAddress("minDPhiAllJetsMet", &minDPhiAllJetsMet, &b_minDPhiAllJetsMet);
   fChain->SetBranchAddress("minDPhiAllJetsMet_LepInvis", &minDPhiAllJetsMet_LepInvis, &b_minDPhiAllJetsMet_LepInvis);
   fChain->SetBranchAddress("DPhiJ1MetTrack", &DPhiJ1MetTrack, &b_DPhiJ1MetTrack);
   fChain->SetBranchAddress("DPhiJ2MetTrack", &DPhiJ2MetTrack, &b_DPhiJ2MetTrack);
   fChain->SetBranchAddress("DPhiJ3MetTrack", &DPhiJ3MetTrack, &b_DPhiJ3MetTrack);
   fChain->SetBranchAddress("DPhiJ4MetTrack", &DPhiJ4MetTrack, &b_DPhiJ4MetTrack);
   fChain->SetBranchAddress("minDPhiCenJetsMetTrack", &minDPhiCenJetsMetTrack, &b_minDPhiCenJetsMetTrack);
   fChain->SetBranchAddress("METOverHTLep", &METOverHTLep, &b_METOverHTLep);
   fChain->SetBranchAddress("METTrackOverHTLep", &METTrackOverHTLep, &b_METTrackOverHTLep);
   fChain->SetBranchAddress("mll", &mll, &b_mll);
   fChain->SetBranchAddress("Rll", &Rll, &b_Rll);
   fChain->SetBranchAddress("Ptll", &Ptll, &b_Ptll);
   fChain->SetBranchAddress("dPhiPllMet", &dPhiPllMet, &b_dPhiPllMet);
   fChain->SetBranchAddress("dPhiPllMetTrack", &dPhiPllMetTrack, &b_dPhiPllMetTrack);
   fChain->SetBranchAddress("METRel", &METRel, &b_METRel);
   fChain->SetBranchAddress("METTrackRel", &METTrackRel, &b_METTrackRel);
   fChain->SetBranchAddress("dPhiNearMet", &dPhiNearMet, &b_dPhiNearMet);
   fChain->SetBranchAddress("dPhiNearMetTrack", &dPhiNearMetTrack, &b_dPhiNearMetTrack);
   fChain->SetBranchAddress("dPhiMetAndMetTrack", &dPhiMetAndMetTrack, &b_dPhiMetAndMetTrack);
   fChain->SetBranchAddress("MTauTau", &MTauTau, &b_MTauTau);
   fChain->SetBranchAddress("MTauTau_metTrack", &MTauTau_metTrack, &b_MTauTau_metTrack);
   fChain->SetBranchAddress("RjlOverEl", &RjlOverEl, &b_RjlOverEl);
   fChain->SetBranchAddress("LepCosThetaLab", &LepCosThetaLab, &b_LepCosThetaLab);
   fChain->SetBranchAddress("LepCosThetaCoM", &LepCosThetaCoM, &b_LepCosThetaCoM);
   fChain->SetBranchAddress("mt2leplsp_0", &mt2leplsp_0, &b_mt2leplsp_0);
   fChain->SetBranchAddress("mt2leplsp_50", &mt2leplsp_50, &b_mt2leplsp_50);
   fChain->SetBranchAddress("mt2leplsp_100", &mt2leplsp_100, &b_mt2leplsp_100);
   fChain->SetBranchAddress("mt2leplsp_150", &mt2leplsp_150, &b_mt2leplsp_150);
   fChain->SetBranchAddress("mt2leplsp_200", &mt2leplsp_200, &b_mt2leplsp_200);
   fChain->SetBranchAddress("mt2leplsp_300", &mt2leplsp_300, &b_mt2leplsp_300);
   fChain->SetBranchAddress("mt2leplsp_0_metTrack", &mt2leplsp_0_metTrack, &b_mt2leplsp_0_metTrack);
   fChain->SetBranchAddress("mt2leplsp_50_metTrack", &mt2leplsp_50_metTrack, &b_mt2leplsp_50_metTrack);
   fChain->SetBranchAddress("mt2leplsp_100_metTrack", &mt2leplsp_100_metTrack, &b_mt2leplsp_100_metTrack);
   fChain->SetBranchAddress("mt2leplsp_150_metTrack", &mt2leplsp_150_metTrack, &b_mt2leplsp_150_metTrack);
   fChain->SetBranchAddress("mt2leplsp_200_metTrack", &mt2leplsp_200_metTrack, &b_mt2leplsp_200_metTrack);
   fChain->SetBranchAddress("mt2leplsp_300_metTrack", &mt2leplsp_300_metTrack, &b_mt2leplsp_300_metTrack);
   fChain->SetBranchAddress("nSFOS", &nSFOS, &b_nSFOS);
   fChain->SetBranchAddress("mt_minMll", &mt_minMll, &b_mt_minMll);
   fChain->SetBranchAddress("minMll", &minMll, &b_minMll);
   fChain->SetBranchAddress("minRll", &minRll, &b_minRll);
   fChain->SetBranchAddress("m3l", &m3l, &b_m3l);
   fChain->SetBranchAddress("pT3l", &pT3l, &b_pT3l);
   fChain->SetBranchAddress("dPhi3lMet", &dPhi3lMet, &b_dPhi3lMet);
   fChain->SetBranchAddress("ptZ_minMll", &ptZ_minMll, &b_ptZ_minMll);
   fChain->SetBranchAddress("ptW_minMll", &ptW_minMll, &b_ptW_minMll);
   fChain->SetBranchAddress("ptWlep_minMll", &ptWlep_minMll, &b_ptWlep_minMll);
   fChain->SetBranchAddress("RZlep_minMll", &RZlep_minMll, &b_RZlep_minMll);
   fChain->SetBranchAddress("RZWlep_minMll", &RZWlep_minMll, &b_RZWlep_minMll);
   fChain->SetBranchAddress("dPhiZMet_minMll", &dPhiZMet_minMll, &b_dPhiZMet_minMll);
   fChain->SetBranchAddress("dPhiWLepMet_minMll", &dPhiWLepMet_minMll, &b_dPhiWLepMet_minMll);
   fChain->SetBranchAddress("mt2WZlsp_0", &mt2WZlsp_0, &b_mt2WZlsp_0);
   fChain->SetBranchAddress("mt2WZlsp_100", &mt2WZlsp_100, &b_mt2WZlsp_100);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
   fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
   fChain->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
   fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
   fChain->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
   fChain->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
   fChain->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
   fChain->SetBranchAddress("triggerWeight", &triggerWeight, &b_triggerWeight);
   fChain->SetBranchAddress("totalWeight", &totalWeight, &b_totalWeight);
   fChain->SetBranchAddress("truthMll", &truthMll, &b_truthMll);
   fChain->SetBranchAddress("winoBinoMllWeight", &winoBinoMllWeight, &b_winoBinoMllWeight);
   fChain->SetBranchAddress("winoBinoXsecWeight", &winoBinoXsecWeight, &b_winoBinoXsecWeight);
   fChain->SetBranchAddress("winoBinoBrFracWeight", &winoBinoBrFracWeight, &b_winoBinoBrFracWeight);
   fChain->SetBranchAddress("winoBinoWeight", &winoBinoWeight, &b_winoBinoWeight);
   fChain->SetBranchAddress("NUHM2weight_350m12", &NUHM2weight_350m12, &b_NUHM2weight_350m12);
   fChain->SetBranchAddress("NUHM2weight_400m12", &NUHM2weight_400m12, &b_NUHM2weight_400m12);
   fChain->SetBranchAddress("NUHM2weight_500m12", &NUHM2weight_500m12, &b_NUHM2weight_500m12);
   fChain->SetBranchAddress("NUHM2weight_600m12", &NUHM2weight_600m12, &b_NUHM2weight_600m12);
   fChain->SetBranchAddress("NUHM2weight_700m12", &NUHM2weight_700m12, &b_NUHM2weight_700m12);
   fChain->SetBranchAddress("NUHM2weight_800m12", &NUHM2weight_800m12, &b_NUHM2weight_800m12);
   fChain->SetBranchAddress("FFWeight", &FFWeight, &b_FFWeight);
   fChain->SetBranchAddress("nLep_antiID", &nLep_antiID, &b_nLep_antiID);
   fChain->SetBranchAddress("lep1AntiID", &lep1AntiID, &b_lep1AntiID);
   fChain->SetBranchAddress("lep2AntiID", &lep2AntiID, &b_lep2AntiID);
   fChain->SetBranchAddress("lep3AntiID", &lep3AntiID, &b_lep3AntiID);
   fChain->SetBranchAddress("nLep_signalActual", &nLep_signalActual, &b_nLep_signalActual);
   fChain->SetBranchAddress("lep1SignalActual", &lep1SignalActual, &b_lep1SignalActual);
   fChain->SetBranchAddress("lep2SignalActual", &lep2SignalActual, &b_lep2SignalActual);
   fChain->SetBranchAddress("lep3SignalActual", &lep3SignalActual, &b_lep3SignalActual);
   fChain->SetBranchAddress("ETMissTrigSF", &ETMissTrigSF, &b_ETMissTrigSF);
   fChain->SetBranchAddress("HLT_mu4", &HLT_mu4, &b_HLT_mu4);
   fChain->SetBranchAddress("HLT_2mu4", &HLT_2mu4, &b_HLT_2mu4);
   fChain->SetBranchAddress("HLT_2mu10", &HLT_2mu10, &b_HLT_2mu10);
   fChain->SetBranchAddress("HLT_2mu4_j85_xe50_mht", &HLT_2mu4_j85_xe50_mht, &b_HLT_2mu4_j85_xe50_mht);
   fChain->SetBranchAddress("HLT_mu4_j125_xe90_mht", &HLT_mu4_j125_xe90_mht, &b_HLT_mu4_j125_xe90_mht);
   fChain->SetBranchAddress("HLT_xe70", &HLT_xe70, &b_HLT_xe70);
   fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   fChain->SetBranchAddress("HLT_xe70_mht_wEFMu", &HLT_xe70_mht_wEFMu, &b_HLT_xe70_mht_wEFMu);
   fChain->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("HLT_xe70_tc_lcw_wEFMu", &HLT_xe70_tc_lcw_wEFMu, &b_HLT_xe70_tc_lcw_wEFMu);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_tc_lcw_L1XE50", &HLT_xe90_tc_lcw_L1XE50, &b_HLT_xe90_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_tc_lcw_wEFMu_L1XE50", &HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_wEFMu_L1XE50", &HLT_xe90_mht_wEFMu_L1XE50, &b_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_L1XE50", &HLT_xe100_L1XE50, &b_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_wEFMu_L1XE50", &HLT_xe100_wEFMu_L1XE50, &b_HLT_xe100_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_tc_lcw_L1XE50", &HLT_xe100_tc_lcw_L1XE50, &b_HLT_xe100_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_tc_lcw_wEFMu_L1XE50", &HLT_xe100_tc_lcw_wEFMu_L1XE50, &b_HLT_xe100_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_wEFMu_L1XE50", &HLT_xe100_mht_wEFMu_L1XE50, &b_HLT_xe100_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_L1XE50", &HLT_xe110_L1XE50, &b_HLT_xe110_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_tc_em_L1XE50", &HLT_xe110_tc_em_L1XE50, &b_HLT_xe110_tc_em_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_wEFMu_L1XE50", &HLT_xe110_wEFMu_L1XE50, &b_HLT_xe110_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_tc_em_wEFMu_L1XE50", &HLT_xe110_tc_em_wEFMu_L1XE50, &b_HLT_xe110_tc_em_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE40", &HLT_xe90_mht_L1XE40, &b_HLT_xe90_mht_L1XE40);
   fChain->SetBranchAddress("HLT_xe50_mht_L1XE20", &HLT_xe50_mht_L1XE20, &b_HLT_xe50_mht_L1XE20);
   fChain->SetBranchAddress("HLT_xe110_pufit_xe65_L1XE50", &HLT_xe110_pufit_xe65_L1XE50, &b_HLT_xe110_pufit_xe65_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_pufit_xe70_L1XE50", &HLT_xe110_pufit_xe70_L1XE50, &b_HLT_xe110_pufit_xe70_L1XE50);
   fChain->SetBranchAddress("HLT_j85_L1J40", &HLT_j85_L1J40, &b_HLT_j85_L1J40);
   fChain->SetBranchAddress("HLT_j125_L1J50", &HLT_j125_L1J50, &b_HLT_j125_L1J50);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e60_medium", &HLT_e60_medium, &b_HLT_e60_medium);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_e5_lhvloose_nod0", &HLT_e5_lhvloose_nod0, &b_HLT_e5_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_2e5_lhvloose_nod0", &HLT_2e5_lhvloose_nod0, &b_HLT_2e5_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_e5_lhloose_nod0", &HLT_e5_lhloose_nod0, &b_HLT_e5_lhloose_nod0);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30", &HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30, &b_HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30", &HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30, &b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE40_DPHI-J20s2XE30", &HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE40_DPHI-J20s2XE30, &b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J20_XE40_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50", &HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50, &b_HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe80_pufit_2dphi10_L12MU4_J40_XE50", &HLT_2mu4_invm1_j20_xe80_pufit_2dphi10_L12MU4_J40_XE50, &b_HLT_2mu4_invm1_j20_xe80_pufit_2dphi10_L12MU4_J40_XE50);
//   fChain->SetBranchAddress("HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30", &HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30, &b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60", &HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60, &b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60);
//   fChain->SetBranchAddress("HLT_mu4_j80_xe80_pufit_2dphi10_L1MU4_XE60", &HLT_mu4_j80_xe80_pufit_2dphi10_L1MU4_XE60, &b_HLT_mu4_j80_xe80_pufit_2dphi10_L1MU4_XE60);
//   fChain->SetBranchAddress("HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50", &HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50, &b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50);
//   fChain->SetBranchAddress("HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60", &HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60, &b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60);
//   fChain->SetBranchAddress("HLT_2e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60", &HLT_2e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60, &b_HLT_2e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60);
//   fChain->SetBranchAddress("HLT_2e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50", &HLT_2e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50, &b_HLT_2e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50);
//   fChain->SetBranchAddress("HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30", &HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30, &b_HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50", &HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50, &b_HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50);
//   fChain->SetBranchAddress("HLT_e5_lhloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30", &HLT_e5_lhloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30, &b_HLT_e5_lhloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30);
//   fChain->SetBranchAddress("HLT_e5_lhmedium_nod0_j50_xe80_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50", &HLT_e5_lhmedium_nod0_j50_xe80_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50, &b_HLT_e5_lhmedium_nod0_j50_xe80_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50);
//   fChain->SetBranchAddress("HLT_e5_lhmedium_nod0_mu4_j30_xe65_pufit_2dphi10_L1MU4_XE60", &HLT_e5_lhmedium_nod0_mu4_j30_xe65_pufit_2dphi10_L1MU4_XE60, &b_HLT_e5_lhmedium_nod0_mu4_j30_xe65_pufit_2dphi10_L1MU4_XE60);
//   fChain->SetBranchAddress("HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60", &HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60, &b_HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60);
//   fChain->SetBranchAddress("HLT_e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60", &HLT_e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60, &b_HLT_e5_lhmedium_nod0_j40_xe80_pufit_2dphi10_L1XE60);
//   fChain->SetBranchAddress("HLT_e5_lhmedium_nod0_j50_xe90_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50", &HLT_e5_lhmedium_nod0_j50_xe90_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50, &b_HLT_e5_lhmedium_nod0_j50_xe90_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50);
//   fChain->SetBranchAddress("HLT_mu4_j100_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30", &HLT_mu4_j100_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30, &b_HLT_mu4_j100_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30);
//   fChain->SetBranchAddress("HLT_mu4_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30", &HLT_mu4_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30, &b_HLT_mu4_xe60mht_L1MU4_J20_XE30_DPHI-J20sXE30);
//   fChain->SetBranchAddress("HLT_2mu4_xe40mht_L12MU4_J20_XE30_DPHI-J20sXE30", &HLT_2mu4_xe40mht_L12MU4_J20_XE30_DPHI-J20sXE30, &b_HLT_2mu4_xe40mht_L12MU4_J20_XE30_DPHI-J20sXE30);
//   fChain->SetBranchAddress("HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30", &HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30, &b_HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30);
//   fChain->SetBranchAddress("HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50", &HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50, &b_HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50);
//   fChain->SetBranchAddress("HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul", &HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul, &b_HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul);
   fChain->SetBranchAddress("HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul", &HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul, &b_HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul);
   fChain->SetBranchAddress("HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul", &HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul, &b_HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul);
   fChain->SetBranchAddress("HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul", &HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul, &b_HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul);
   fChain->SetBranchAddress("HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul", &HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul, &b_HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul);
   fChain->SetBranchAddress("HLT_2dphi10_emul", &HLT_2dphi10_emul, &b_HLT_2dphi10_emul);
   fChain->SetBranchAddress("HLT_2mu4_invm1_emul", &HLT_2mu4_invm1_emul, &b_HLT_2mu4_invm1_emul);
   fChain->SetBranchAddress("HLT_xe90_pufit_L1XE50", &HLT_xe90_pufit_L1XE50, &b_HLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_pufit_L1XE50", &HLT_xe100_pufit_L1XE50, &b_HLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_pufit_L1XE55", &HLT_xe100_pufit_L1XE55, &b_HLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("HLT_xe110_pufit_L1XE50", &HLT_xe110_pufit_L1XE50, &b_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_pufit_L1XE55", &HLT_xe110_pufit_L1XE55, &b_HLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("HLT_j20", &HLT_j20, &b_HLT_j20);
   fChain->SetBranchAddress("HLT_j40", &HLT_j40, &b_HLT_j40);
   fChain->SetBranchAddress("HLT_j30", &HLT_j30, &b_HLT_j30);
   fChain->SetBranchAddress("HLT_j50", &HLT_j50, &b_HLT_j50);
   fChain->SetBranchAddress("HLT_j90", &HLT_j90, &b_HLT_j90);
   fChain->SetBranchAddress("L1_DPHI_2J20XE_emul", &L1_DPHI_2J20XE_emul, &b_L1_DPHI_2J20XE_emul);
   fChain->SetBranchAddress("L1_MU4", &L1_MU4, &b_L1_MU4);
   fChain->SetBranchAddress("L1_2MU4", &L1_2MU4, &b_L1_2MU4);
   fChain->SetBranchAddress("L1_J20", &L1_J20, &b_L1_J20);
   fChain->SetBranchAddress("L1_J30", &L1_J30, &b_L1_J30);
   fChain->SetBranchAddress("L1_J40", &L1_J40, &b_L1_J40);
   fChain->SetBranchAddress("L1_J50", &L1_J50, &b_L1_J50);
   fChain->SetBranchAddress("L1_XE30", &L1_XE30, &b_L1_XE30);
   fChain->SetBranchAddress("L1_XE40", &L1_XE40, &b_L1_XE40);
   fChain->SetBranchAddress("L1_XE50", &L1_XE50, &b_L1_XE50);
   fChain->SetBranchAddress("x1", &x1, &b_x1);
   fChain->SetBranchAddress("x2", &x2, &b_x2);
   fChain->SetBranchAddress("pdf1", &pdf1, &b_pdf1);
   fChain->SetBranchAddress("pdf2", &pdf2, &b_pdf2);
   fChain->SetBranchAddress("scalePDF", &scalePDF, &b_scalePDF);
   fChain->SetBranchAddress("id1", &id1, &b_id1);
   fChain->SetBranchAddress("id2", &id2, &b_id2);
   fChain->SetBranchAddress("RJR_PTISR", &RJR_PTISR, &b_RJR_PTISR);
   fChain->SetBranchAddress("RJR_RISR", &RJR_RISR, &b_RJR_RISR);
   fChain->SetBranchAddress("RJR_MS", &RJR_MS, &b_RJR_MS);
   fChain->SetBranchAddress("RJR_MISR", &RJR_MISR, &b_RJR_MISR);
   fChain->SetBranchAddress("RJR_dphiISRI", &RJR_dphiISRI, &b_RJR_dphiISRI);
   fChain->SetBranchAddress("RJR_NjV", &RJR_NjV, &b_RJR_NjV);
   fChain->SetBranchAddress("RJR_NjISR", &RJR_NjISR, &b_RJR_NjISR);
   fChain->SetBranchAddress("SUSYPt", &SUSYPt, &b_SUSYPt);
   fChain->SetBranchAddress("ISRWeight", &ISRWeight, &b_ISRWeight);
   fChain->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
   fChain->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
   fChain->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("FS", &FS, &b_FS);
   fChain->SetBranchAddress("LHE3Weights", &LHE3Weights, &b_LHE3Weights);
   fChain->SetBranchAddress("LHE3WeightNames", &LHE3WeightNames, &b_LHE3WeightNames);
}
   //Notify();
};

//Bool_t CompressedVBF::Notify()
//{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

//   return kTRUE;
//}

//void CompressedVBF::Show(Long64_t entry)
//{
//// Print contents of entry.
//// If entry is not specified, print current entry
//   if (!fChain) return;
//   fChain->Show(entry);
//}
//Int_t CompressedVBF::Cut(Long64_t entry)
//{
//// This function may be called from Loop.
//// returns  1 if entry is accepted.
//// returns -1 otherwise.
//   return 1;
//}
//#endif // #ifdef CompressedVBF_cxx

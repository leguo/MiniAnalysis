#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TRandom.h>

#include <functional>

#include "AnaTree.h"
#include "TLorentzVector.h"
#include "TVector3.h"

class SubTauTree : public AnaTree {
public:
    TRandom rdm; // Random generator for the TER functions

    // Name format example: 1P_TER_EM, 3P_TESUp_EM_fermiLow, 3P_TESDw_Had, 1P_TER_Had_fermiHigh
    double getTauEnergySyst(std::string name, double RecoPt, double truth_neutral_pt, double truth_pt) {
        std::string varitionEM, varitionTrk;
        if (name.find("EM") != std::string::npos) { // It's EM comp
            if (name.find("1P") != std::string::npos) {
                varitionEM = "EMComp_1P";
            } else if (name.find("3P") != std::string::npos) {
                varitionEM = "EMComp_3P";
            }

            if (name.find("Low") != std::string::npos) {
                varitionEM += "_Low";
            } else if (name.find("High") != std::string::npos) {
                varitionEM += "_High";
            }

            if (varitionFuncs.find(varitionEM) != varitionFuncs.end()) {
                if (name.find("TER") != std::string::npos) {
                    return getEMPtTER(varitionFuncs[varitionEM], RecoPt, truth_neutral_pt / truth_pt);
                } else if (name.find("TESUp") != std::string::npos) {
                    return getEMPtTESUp(varitionFuncs[varitionEM], RecoPt, truth_neutral_pt / truth_pt);
                } else if (name.find("TESDw") != std::string::npos) {
                    return getEMPtTESDw(varitionFuncs[varitionEM], RecoPt, truth_neutral_pt / truth_pt);
                }
            }
        } else if (name.find("Had") != std::string::npos) { // It's Had comp
            if (name.find("1P") != std::string::npos) {
                varitionEM = "EMComp_1P";
                varitionTrk = "trkComp_1P";
            } else if (name.find("3P") != std::string::npos) {
                varitionEM = "EMComp_3P";
                varitionTrk = "trkComp_3P";
            }

            if (name.find("Low") != std::string::npos) {
                varitionEM += "_Low";
                varitionTrk += "_Low";
            } else if (name.find("High") != std::string::npos) {
                varitionEM += "_High";
                varitionTrk += "_High";
            }

            if (varitionFuncs.find(varitionEM) != varitionFuncs.end()) {
                if (name.find("TER") != std::string::npos) {
                    return getTrkPtTER(varitionFuncs[varitionEM], varitionFuncs[varitionTrk], RecoPt, truth_neutral_pt / truth_pt);
                } else if (name.find("TESUp") != std::string::npos) {
                    return getTrkPtTESUp(varitionFuncs[varitionEM], varitionFuncs[varitionTrk], RecoPt, truth_neutral_pt / truth_pt);
                } else if (name.find("TESDw") != std::string::npos) {
                    return getTrkPtTESDw(varitionFuncs[varitionEM], varitionFuncs[varitionTrk], RecoPt, truth_neutral_pt / truth_pt);
                }
            }
        }
        LOG(ERROR) << "Error in getTauEnergySyst: No syst found for name " << name << "!!! Return value 0!!!";
        return 0;
    }

    inline double getEMPtTESDw(std::function<double(double)> varition, double RecoPt, double truth_Frac_neutral) {
        double syst_Comp = varition(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (-0.02))); // 10% syst for TES
    }

    inline double getEMPtTESUp(std::function<double(double)> varition, double RecoPt, double truth_Frac_neutral) {
        double syst_Comp = varition(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (+0.02))); // 10% syst for TES
    }

    inline double getEMPtTER(std::function<double(double)> varition, double RecoPt, double truth_Frac_neutral) {
        double syst_Comp = varition(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (0.02) * sqrt(2) * TMath::ErfInverse(2. * rdm.Rndm() - 1.))); // Also 10% syst for TER
    }

    inline double getTrkPtTESDw(std::function<double(double)> varitionEM, std::function<double(double)> varitionTrk, double RecoPt,
                                double truth_Frac_neutral) {
        double syst_Comp = 1 - varitionEM(truth_Frac_neutral) - varitionTrk(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (-0.02))); // 10% syst for TES
    }

    inline double getTrkPtTESUp(std::function<double(double)> varitionEM, std::function<double(double)> varitionTrk, double RecoPt,
                                double truth_Frac_neutral) {
        double syst_Comp = 1 - varitionEM(truth_Frac_neutral) - varitionTrk(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (+0.02))); // 10% syst for TES
    }

    inline double getTrkPtTER(std::function<double(double)> varitionEM, std::function<double(double)> varitionTrk, double RecoPt,
                              double truth_Frac_neutral) {
        double syst_Comp = 1 - varitionEM(truth_Frac_neutral) - varitionTrk(truth_Frac_neutral);
        return (RecoPt * (1 + syst_Comp * (0.02) * sqrt(2) * TMath::ErfInverse(2. * rdm.Rndm() - 1.))); // Also 10% syst for TER
    }

    std::map<std::string, std::function<double(double)>> varitionFuncs;

    void InitTESComFuncs() {
        varitionFuncs["EMComp_1P"] = [](double Frac_neutral_Pt) {
            return (4.41270e-01 + 2.88531e-01 * Frac_neutral_Pt + (-3.71932e-01 / (exp((Frac_neutral_Pt - (8.91160e-02)) / 1.27065e-02) + 1.)));
        };
        varitionFuncs["trkComp_1P"] = [](double Frac_neutral_Pt) {
            return (1.05492e-01 - 1.13490e-01 * Frac_neutral_Pt + (7.29483e-01 / (exp((Frac_neutral_Pt - (1.10150e-01)) / 2.89983e-02) + 1.)));
        };
        varitionFuncs["EMComp_1P_Low"] = [](double Frac_neutral_Pt) {
            return (4.41270e-01 + 2.88531e-01 * Frac_neutral_Pt +
                    (-3.71932e-01 / (exp((Frac_neutral_Pt - (8.91160e-02 - 2.35198e-02)) / 1.27065e-02) + 1.)));
        };
        varitionFuncs["EMComp_1P_High"] = [](double Frac_neutral_Pt) {
            return (4.41270e-01 + 2.88531e-01 * Frac_neutral_Pt +
                    (-3.71932e-01 / (exp((Frac_neutral_Pt - (8.91160e-02 + 2.35198e-02)) / 1.27065e-02) + 1.)));
        };
        varitionFuncs["trkComp_1P_Low"] = [](double Frac_neutral_Pt) {
            return (1.05492e-01 - 1.13490e-01 * Frac_neutral_Pt +
                    (7.29483e-01 / (exp((Frac_neutral_Pt - (1.10150e-01 - 2.88497e-02)) / 2.89983e-02) + 1.)));
        };
        varitionFuncs["trkComp_1P_High"] = [](double Frac_neutral_Pt) {
            return (1.05492e-01 - 1.13490e-01 * Frac_neutral_Pt +
                    (7.29483e-01 / (exp((Frac_neutral_Pt - (1.10150e-01 + 2.88497e-02)) / 2.89983e-02) + 1.)));
        };
        varitionFuncs["EMComp_3P"] = [](double Frac_neutral_Pt) {
            return (9.93786e-01 + 6.06371e-09 * Frac_neutral_Pt + (-8.06665e-01 / (exp((Frac_neutral_Pt - (1.03969e-01)) / 3.02212e-02) + 1.)));
        };
        varitionFuncs["trkComp_3P"] = [](double Frac_neutral_Pt) {
            return (1.40551e-01 - 1.81727e-01 * Frac_neutral_Pt + (7.52062e-01 / (exp((Frac_neutral_Pt - (8.94747e-02)) / 4.52454e-02) + 1.)));
        };
        varitionFuncs["EMComp_3P_Low"] = [](double Frac_neutral_Pt) {
            return (9.93786e-01 + 6.06371e-09 * Frac_neutral_Pt +
                    (-8.06665e-01 / (exp((Frac_neutral_Pt - (1.03969e-01 - 3.42284e-02)) / 3.02212e-02) + 1.)));
        };
        varitionFuncs["EMComp_3P_High"] = [](double Frac_neutral_Pt) {
            return (9.93786e-01 + 6.06371e-09 * Frac_neutral_Pt +
                    (-8.06665e-01 / (exp((Frac_neutral_Pt - (1.03969e-01 + 3.42284e-02)) / 3.02212e-02) + 1.)));
        };
        varitionFuncs["trkComp_3P_Low"] = [](double Frac_neutral_Pt) {
            return (1.40551e-01 - 1.81727e-01 * Frac_neutral_Pt +
                    (7.52062e-01 / (exp((Frac_neutral_Pt - (8.94747e-02 - 1.01820e-01)) / 4.52454e-02) + 1.)));
        };
        varitionFuncs["trkComp_3P_High"] = [](double Frac_neutral_Pt) {
            return (1.40551e-01 - 1.81727e-01 * Frac_neutral_Pt +
                    (7.52062e-01 / (exp((Frac_neutral_Pt - (8.94747e-02 + 1.01820e-01)) / 4.52454e-02) + 1.)));
        };
    }

    SubTauTree(TChain *chain) : AnaTree(chain) {
        Init(this->fChain);
        InitTESComFuncs();
    };
    // Declaration of leaf types
    UInt_t HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM;
    UInt_t HLT_mu14_ivarloose_tau25_medium1_tracktwo;
    UInt_t HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;
    UInt_t HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;
    UInt_t HLT_mu26_imedium;
    UInt_t HLT_mu26_ivarmedium;
    UInt_t HLT_mu50;
    UInt_t HLT_xe50;
    UInt_t L1_TAU25IM;
    Float_t NOMINAL_pileup_combined_weight;
    UInt_t NOMINAL_pileup_random_run_number;
    Float_t PRW_DATASF_1down_pileup_combined_weight;
    UInt_t PRW_DATASF_1down_pileup_random_run_number;
    Float_t PRW_DATASF_1up_pileup_combined_weight;
    UInt_t PRW_DATASF_1up_pileup_random_run_number;
    Double_t dilep_m;
    UInt_t event_clean_EC_LooseBad;
    Int_t event_clean_detector_core;
    ULong64_t event_number;
    UInt_t jet_0;
    TLorentzVector *jet_0_p4;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_global_effSF_MV2c10;
    Float_t jet_NOMINAL_global_ineffSF_MV2c10;
    Float_t lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
    Float_t lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad;
    Float_t lep_0_NOMINAL_MuEffSF_Reco_QualMedium;
    Float_t lep_0_NOMINAL_MuEffSF_TTVA;
    Int_t lep_0_id_loose;
    Int_t lep_0_id_medium;
    Int_t lep_0_id_tight;
    UInt_t lep_0_iso_FCLoose;
    UInt_t lep_0_iso_FCLoose_FixedRad;
    UInt_t lep_0_iso_FCTight;
    UInt_t lep_0_iso_FCTightTrackOnly;
    UInt_t lep_0_iso_FCTightTrackOnly_FixedRad;
    UInt_t lep_0_iso_FCTight_FixedRad;
    UInt_t lep_0_iso_FixedCutHighPtTrackOnly;
    UInt_t lep_0_iso_Gradient;
    UInt_t lep_0_matched;
    Int_t lep_0_matched_classifierParticleOrigin;
    Int_t lep_0_matched_classifierParticleType;
    Int_t lep_0_matched_origin;
    TLorentzVector *lep_0_matched_p4;
    Int_t lep_0_matched_pdgId;
    TLorentzVector *lep_0_p4;
    Float_t lep_0_q;
    Float_t lep_0_trk_d0;
    Float_t lep_0_trk_d0_sig;
    Float_t lep_0_trk_z0;
    Float_t lep_0_trk_z0_sig;
    Float_t lep_0_trk_z0_sintheta;
    Float_t lephad_deta;
    Float_t lephad_dphi;
    Float_t lephad_dr;
    Float_t lephad_met_sum_cos_dphi;
    Float_t lephad_mt_lep0_met;
    Float_t lephad_mt_lep1_met;
    TLorentzVector *lephad_p4;
    Float_t lephad_qxq;
    UInt_t mc_channel_number;
    TLorentzVector *met_reco_p4;
    Float_t met_reco_sumet;
    UInt_t muTrigMatch_0_HLT_mu26_ivarmedium;
    UInt_t muTrigMatch_0_HLT_mu50;
    Float_t n_actual_int;
    Float_t n_actual_int_cor;
    Float_t n_avg_int;
    Float_t n_avg_int_cor;
    Int_t n_bjets;
    Int_t n_jets;
    Int_t n_vx;
    UInt_t run_number;
    Float_t tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_NOMINAL_TauEffSF_JetBDTloose;
    Float_t tau_0_NOMINAL_TauEffSF_JetBDTmedium;
    Float_t tau_0_NOMINAL_TauEffSF_JetBDTtight;
    Float_t tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_NOMINAL_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDTPlusVeto_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_VeryLooseLlhEleOLR_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;
    Float_t tau_0_allTrk_eta;
    UInt_t tau_0_allTrk_n;
    Float_t tau_0_allTrk_phi;
    Float_t tau_0_allTrk_pt;
    UInt_t tau_0_decay_mode;
    UInt_t tau_0_jet_bdt_loose;
    UInt_t tau_0_jet_bdt_medium;
    Float_t tau_0_jet_bdt_score;
    Float_t tau_0_jet_bdt_score_trans;
    UInt_t tau_0_jet_bdt_tight;
    UInt_t tau_0_jet_bdt_veryloose;
    Float_t tau_0_jet_jvt;
    UInt_t tau_0_jet_rnn_loose;
    UInt_t tau_0_jet_rnn_medium;
    Float_t tau_0_jet_rnn_score;
    Float_t tau_0_jet_rnn_score_trans;
    UInt_t tau_0_jet_rnn_tight;
    UInt_t tau_0_jet_rnn_veryloose;
    Float_t tau_0_jet_width;
    Double_t tau_0_leadTrk_d0;
    Double_t tau_0_leadTrk_d0_sig;
    Float_t tau_0_leadTrk_eta;
    Float_t tau_0_leadTrk_phi;
    Float_t tau_0_leadTrk_pt;
    Double_t tau_0_leadTrk_pvx_z0;
    Double_t tau_0_leadTrk_pvx_z0_sig;
    Double_t tau_0_leadTrk_pvx_z0_sintheta;
    TVector3 *tau_0_leadTrk_vertex_v;
    Double_t tau_0_leadTrk_z0;
    Double_t tau_0_leadTrk_z0_sig;
    Double_t tau_0_leadTrk_z0_sintheta;
    UInt_t tau_0_n_all_tracks;
    UInt_t tau_0_n_charged_tracks;
    UInt_t tau_0_n_conversion_tracks;
    UInt_t tau_0_n_core_tracks;
    UInt_t tau_0_n_failTrackFilter_tracks;
    UInt_t tau_0_n_fake_tracks;
    UInt_t tau_0_n_isolation_tracks;
    UInt_t tau_0_n_modified_isolation_tracks;
    UInt_t tau_0_n_old_tracks;
    UInt_t tau_0_n_passTrkSelectionTight_tracks;
    UInt_t tau_0_n_passTrkSelector_tracks;
    UInt_t tau_0_n_unclassified_tracks;
    UInt_t tau_0_n_wide_tracks;
    Int_t tau_0_origin;
    TLorentzVector *tau_0_p4;
    TLorentzVector *tau_0_p4_uncorrected_caloTES;
    TLorentzVector *tau_0_p4_uncorrected_mvaTES;
    Float_t tau_0_q;
    Float_t tau_0_sub_ConstituentBased_eta;
    Float_t tau_0_sub_ConstituentBased_m;
    Float_t tau_0_sub_ConstituentBased_phi;
    Float_t tau_0_sub_ConstituentBased_pt;
    Float_t tau_0_sub_ParticleFlowCombined_eta;
    Float_t tau_0_sub_ParticleFlowCombined_m;
    Float_t tau_0_sub_ParticleFlowCombined_phi;
    Float_t tau_0_sub_ParticleFlowCombined_pt;
    Int_t tau_0_sub_n_charged;
    Int_t tau_0_sub_n_neutral;
    TLorentzVector *tau_0_track0_p4;
    TLorentzVector *tau_0_track1_p4;
    TLorentzVector *tau_0_track2_p4;
    Int_t tau_0_truth_classifierParticleOrigin;
    Int_t tau_0_truth_classifierParticleType;
    Int_t tau_0_truth_decay_mode;
    UInt_t tau_0_truth_isEle;
    UInt_t tau_0_truth_isHadTau;
    UInt_t tau_0_truth_isJet;
    UInt_t tau_0_truth_isMuon;
    UInt_t tau_0_truth_isTau;
    UInt_t tau_0_truth_isTruthMatch;
    Int_t tau_0_truth_mother_pdgId;
    Int_t tau_0_truth_mother_status;
    Int_t tau_0_truth_n_charged;
    Int_t tau_0_truth_n_charged_pion;
    Int_t tau_0_truth_n_neutral;
    Int_t tau_0_truth_n_neutral_pion;
    Int_t tau_0_truth_origin;
    TLorentzVector *tau_0_truth_p4;
    TLorentzVector *tau_0_truth_p4_vis_charged_track0;
    TLorentzVector *tau_0_truth_p4_vis_charged_track1;
    TLorentzVector *tau_0_truth_p4_vis_charged_track2;
    Int_t tau_0_truth_pdgId;
    Float_t tau_0_truth_pz;
    Float_t tau_0_truth_q;
    Int_t tau_0_truth_status;
    Int_t tau_0_truth_type;
    TLorentzVector *tau_0_truth_vis_charged_p4;
    TLorentzVector *tau_0_truth_vis_neutral_others_p4;
    TLorentzVector *tau_0_truth_vis_neutral_p4;
    TLorentzVector *tau_0_truth_vis_neutral_pions_p4;
    TLorentzVector *tau_0_truth_vis_p4;
    Int_t tau_0_type;
    Double_t tau_tes_alpha_pt_shift;
    Double_t weight_mc;

    // List of branches
    TBranch *b_HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM;                                        //!
    TBranch *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo;                                                              //!
    TBranch *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;                                   //!
    TBranch *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;                                               //!
    TBranch *b_HLT_mu26_imedium;                                                                                       //!
    TBranch *b_HLT_mu26_ivarmedium;                                                                                    //!
    TBranch *b_HLT_mu50;                                                                                               //!
    TBranch *b_HLT_xe50;                                                                                               //!
    TBranch *b_L1_TAU25IM;                                                                                             //!
    TBranch *b_NOMINAL_pileup_combined_weight;                                                                         //!
    TBranch *b_NOMINAL_pileup_random_run_number;                                                                       //!
    TBranch *b_PRW_DATASF_1down_pileup_combined_weight;                                                                //!
    TBranch *b_PRW_DATASF_1down_pileup_random_run_number;                                                              //!
    TBranch *b_PRW_DATASF_1up_pileup_combined_weight;                                                                  //!
    TBranch *b_PRW_DATASF_1up_pileup_random_run_number;                                                                //!
    TBranch *b_dilep_m;                                                                                                //!
    TBranch *b_event_clean_EC_LooseBad;                                                                                //!
    TBranch *b_event_clean_detector_core;                                                                              //!
    TBranch *b_event_number;                                                                                           //!
    TBranch *b_jet_0;                                                                                                  //!
    TBranch *b_jet_0_p4;                                                                                               //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;                                              //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;                                            //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;                                                //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;                                              //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;                                             //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;                                           //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;                                               //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;                                             //!
    TBranch *b_jet_NOMINAL_central_jets_global_effSF_JVT;                                                              //!
    TBranch *b_jet_NOMINAL_central_jets_global_ineffSF_JVT;                                                            //!
    TBranch *b_jet_NOMINAL_forward_jets_global_effSF_JVT;                                                              //!
    TBranch *b_jet_NOMINAL_forward_jets_global_ineffSF_JVT;                                                            //!
    TBranch *b_jet_NOMINAL_global_effSF_MV2c10;                                                                        //!
    TBranch *b_jet_NOMINAL_global_ineffSF_MV2c10;                                                                      //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad;                                     //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad;                                       //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad;                                      //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad;                                        //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;                                                 //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;                                                   //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;                                           //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;                                             //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;                                                  //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;                                                    //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;                                            //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;                                              //!
    TBranch *b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;                                                            //!
    TBranch *b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;                                                              //!
    TBranch *b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;                                                             //!
    TBranch *b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;                                                               //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone; //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;    //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;      //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone; //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;    //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;      //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;                               //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad;                                                     //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium;                                                                  //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_TTVA;                                                                             //!
    TBranch *b_lep_0_id_loose;                                                                                         //!
    TBranch *b_lep_0_id_medium;                                                                                        //!
    TBranch *b_lep_0_id_tight;                                                                                         //!
    TBranch *b_lep_0_iso_FCLoose;                                                                                      //!
    TBranch *b_lep_0_iso_FCLoose_FixedRad;                                                                             //!
    TBranch *b_lep_0_iso_FCTight;                                                                                      //!
    TBranch *b_lep_0_iso_FCTightTrackOnly;                                                                             //!
    TBranch *b_lep_0_iso_FCTightTrackOnly_FixedRad;                                                                    //!
    TBranch *b_lep_0_iso_FCTight_FixedRad;                                                                             //!
    TBranch *b_lep_0_iso_FixedCutHighPtTrackOnly;                                                                      //!
    TBranch *b_lep_0_iso_Gradient;                                                                                     //!
    TBranch *b_lep_0_matched;                                                                                          //!
    TBranch *b_lep_0_matched_classifierParticleOrigin;                                                                 //!
    TBranch *b_lep_0_matched_classifierParticleType;                                                                   //!
    TBranch *b_lep_0_matched_origin;                                                                                   //!
    TBranch *b_lep_0_matched_p4;                                                                                       //!
    TBranch *b_lep_0_matched_pdgId;                                                                                    //!
    TBranch *b_lep_0_p4;                                                                                               //!
    TBranch *b_lep_0_q;                                                                                                //!
    TBranch *b_lep_0_trk_d0;                                                                                           //!
    TBranch *b_lep_0_trk_d0_sig;                                                                                       //!
    TBranch *b_lep_0_trk_z0;                                                                                           //!
    TBranch *b_lep_0_trk_z0_sig;                                                                                       //!
    TBranch *b_lep_0_trk_z0_sintheta;                                                                                  //!
    TBranch *b_lephad_deta;                                                                                            //!
    TBranch *b_lephad_dphi;                                                                                            //!
    TBranch *b_lephad_dr;                                                                                              //!
    TBranch *b_lephad_met_sum_cos_dphi;                                                                                //!
    TBranch *b_lephad_mt_lep0_met;                                                                                     //!
    TBranch *b_lephad_mt_lep1_met;                                                                                     //!
    TBranch *b_lephad_p4;                                                                                              //!
    TBranch *b_lephad_qxq;                                                                                             //!
    TBranch *b_mc_channel_number;                                                                                      //!
    TBranch *b_met_reco_p4;                                                                                            //!
    TBranch *b_met_reco_sumet;                                                                                         //!
    TBranch *b_muTrigMatch_0_HLT_mu26_ivarmedium;                                                                      //!
    TBranch *b_muTrigMatch_0_HLT_mu50;                                                                                 //!
    TBranch *b_n_actual_int;                                                                                           //!
    TBranch *b_n_actual_int_cor;                                                                                       //!
    TBranch *b_n_avg_int;                                                                                              //!
    TBranch *b_n_avg_int_cor;                                                                                          //!
    TBranch *b_n_bjets;                                                                                                //!
    TBranch *b_n_jets;                                                                                                 //!
    TBranch *b_n_vx;                                                                                                   //!
    TBranch *b_run_number;                                                                                             //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;                                                             //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetBDTloose;                                                                     //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetBDTmedium;                                                                    //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetBDTtight;                                                                     //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron;                                                    //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;                                                            //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron;                                                   //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;                                                           //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron;                                                     //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_reco;                                                                            //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDTPlusVeto_electron;               //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDT_electron;                       //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDTPlusVeto_electron;              //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_VeryLooseLlhEleOLR_electron;                //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDTPlusVeto_electron;                 //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDT_electron;                         //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDTPlusVeto_electron;                //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDT_electron;                        //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_VeryLooseLlhEleOLR_electron;                  //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDTPlusVeto_electron;               //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDT_electron;                       //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDTPlusVeto_electron;              //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_VeryLooseLlhEleOLR_electron;                //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDTPlusVeto_electron;                 //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDT_electron;                         //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDTPlusVeto_electron;                //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDT_electron;                        //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_VeryLooseLlhEleOLR_electron;                  //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDTPlusVeto_electron;                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron;                            //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDTPlusVeto_electron;                   //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron;                           //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_VeryLooseLlhEleOLR_electron;                     //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDTPlusVeto_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron;                              //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDTPlusVeto_electron;                     //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron;                             //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_VeryLooseLlhEleOLR_electron;                       //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1down_TauEffSF_selection;                                 //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1up_TauEffSF_selection;                                   //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1down_TauEffSF_selection;                                  //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1up_TauEffSF_selection;                                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection;                                       //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection;                                         //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;                              //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;                                        //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;                                          //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco;                                              //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection;                                         //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco;                                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection;                                           //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;                                               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;                                          //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;                                                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;                                            //!
    TBranch *b_tau_0_allTrk_eta;                                                                                       //!
    TBranch *b_tau_0_allTrk_n;                                                                                         //!
    TBranch *b_tau_0_allTrk_phi;                                                                                       //!
    TBranch *b_tau_0_allTrk_pt;                                                                                        //!
    TBranch *b_tau_0_decay_mode;                                                                                       //!
    TBranch *b_tau_0_jet_bdt_loose;                                                                                    //!
    TBranch *b_tau_0_jet_bdt_medium;                                                                                   //!
    TBranch *b_tau_0_jet_bdt_score;                                                                                    //!
    TBranch *b_tau_0_jet_bdt_score_trans;                                                                              //!
    TBranch *b_tau_0_jet_bdt_tight;                                                                                    //!
    TBranch *b_tau_0_jet_bdt_veryloose;                                                                                //!
    TBranch *b_tau_0_jet_jvt;                                                                                          //!
    TBranch *b_tau_0_jet_rnn_loose;                                                                                    //!
    TBranch *b_tau_0_jet_rnn_medium;                                                                                   //!
    TBranch *b_tau_0_jet_rnn_score;                                                                                    //!
    TBranch *b_tau_0_jet_rnn_score_trans;                                                                              //!
    TBranch *b_tau_0_jet_rnn_tight;                                                                                    //!
    TBranch *b_tau_0_jet_rnn_veryloose;                                                                                //!
    TBranch *b_tau_0_jet_width;                                                                                        //!
    TBranch *b_tau_0_leadTrk_d0;                                                                                       //!
    TBranch *b_tau_0_leadTrk_d0_sig;                                                                                   //!
    TBranch *b_tau_0_leadTrk_eta;                                                                                      //!
    TBranch *b_tau_0_leadTrk_phi;                                                                                      //!
    TBranch *b_tau_0_leadTrk_pt;                                                                                       //!
    TBranch *b_tau_0_leadTrk_pvx_z0;                                                                                   //!
    TBranch *b_tau_0_leadTrk_pvx_z0_sig;                                                                               //!
    TBranch *b_tau_0_leadTrk_pvx_z0_sintheta;                                                                          //!
    TBranch *b_tau_0_leadTrk_vertex_v;                                                                                 //!
    TBranch *b_tau_0_leadTrk_z0;                                                                                       //!
    TBranch *b_tau_0_leadTrk_z0_sig;                                                                                   //!
    TBranch *b_tau_0_leadTrk_z0_sintheta;                                                                              //!
    TBranch *b_tau_0_n_all_tracks;                                                                                     //!
    TBranch *b_tau_0_n_charged_tracks;                                                                                 //!
    TBranch *b_tau_0_n_conversion_tracks;                                                                              //!
    TBranch *b_tau_0_n_core_tracks;                                                                                    //!
    TBranch *b_tau_0_n_failTrackFilter_tracks;                                                                         //!
    TBranch *b_tau_0_n_fake_tracks;                                                                                    //!
    TBranch *b_tau_0_n_isolation_tracks;                                                                               //!
    TBranch *b_tau_0_n_modified_isolation_tracks;                                                                      //!
    TBranch *b_tau_0_n_old_tracks;                                                                                     //!
    TBranch *b_tau_0_n_passTrkSelectionTight_tracks;                                                                   //!
    TBranch *b_tau_0_n_passTrkSelector_tracks;                                                                         //!
    TBranch *b_tau_0_n_unclassified_tracks;                                                                            //!
    TBranch *b_tau_0_n_wide_tracks;                                                                                    //!
    TBranch *b_tau_0_origin;                                                                                           //!
    TBranch *b_tau_0_p4;                                                                                               //!
    TBranch *b_tau_0_p4_uncorrected_caloTES;                                                                           //!
    TBranch *b_tau_0_p4_uncorrected_mvaTES;                                                                            //!
    TBranch *b_tau_0_q;                                                                                                //!
    TBranch *b_tau_0_sub_ConstituentBased_eta;                                                                         //!
    TBranch *b_tau_0_sub_ConstituentBased_m;                                                                           //!
    TBranch *b_tau_0_sub_ConstituentBased_phi;                                                                         //!
    TBranch *b_tau_0_sub_ConstituentBased_pt;                                                                          //!
    TBranch *b_tau_0_sub_ParticleFlowCombined_eta;                                                                     //!
    TBranch *b_tau_0_sub_ParticleFlowCombined_m;                                                                       //!
    TBranch *b_tau_0_sub_ParticleFlowCombined_phi;                                                                     //!
    TBranch *b_tau_0_sub_ParticleFlowCombined_pt;                                                                      //!
    TBranch *b_tau_0_sub_n_charged;                                                                                    //!
    TBranch *b_tau_0_sub_n_neutral;                                                                                    //!
    TBranch *b_tau_0_track0_p4;                                                                                        //!
    TBranch *b_tau_0_track1_p4;                                                                                        //!
    TBranch *b_tau_0_track2_p4;                                                                                        //!
    TBranch *b_tau_0_truth_classifierParticleOrigin;                                                                   //!
    TBranch *b_tau_0_truth_classifierParticleType;                                                                     //!
    TBranch *b_tau_0_truth_decay_mode;                                                                                 //!
    TBranch *b_tau_0_truth_isEle;                                                                                      //!
    TBranch *b_tau_0_truth_isHadTau;                                                                                   //!
    TBranch *b_tau_0_truth_isJet;                                                                                      //!
    TBranch *b_tau_0_truth_isMuon;                                                                                     //!
    TBranch *b_tau_0_truth_isTau;                                                                                      //!
    TBranch *b_tau_0_truth_isTruthMatch;                                                                               //!
    TBranch *b_tau_0_truth_mother_pdgId;                                                                               //!
    TBranch *b_tau_0_truth_mother_status;                                                                              //!
    TBranch *b_tau_0_truth_n_charged;                                                                                  //!
    TBranch *b_tau_0_truth_n_charged_pion;                                                                             //!
    TBranch *b_tau_0_truth_n_neutral;                                                                                  //!
    TBranch *b_tau_0_truth_n_neutral_pion;                                                                             //!
    TBranch *b_tau_0_truth_origin;                                                                                     //!
    TBranch *b_tau_0_truth_p4;                                                                                         //!
    TBranch *b_tau_0_truth_p4_vis_charged_track0;                                                                      //!
    TBranch *b_tau_0_truth_p4_vis_charged_track1;                                                                      //!
    TBranch *b_tau_0_truth_p4_vis_charged_track2;                                                                      //!
    TBranch *b_tau_0_truth_pdgId;                                                                                      //!
    TBranch *b_tau_0_truth_pz;                                                                                         //!
    TBranch *b_tau_0_truth_q;                                                                                          //!
    TBranch *b_tau_0_truth_status;                                                                                     //!
    TBranch *b_tau_0_truth_type;                                                                                       //!
    TBranch *b_tau_0_truth_vis_charged_p4;                                                                             //!
    TBranch *b_tau_0_truth_vis_neutral_others_p4;                                                                      //!
    TBranch *b_tau_0_truth_vis_neutral_p4;                                                                             //!
    TBranch *b_tau_0_truth_vis_neutral_pions_p4;                                                                       //!
    TBranch *b_tau_0_truth_vis_p4;                                                                                     //!
    TBranch *b_tau_0_type;                                                                                             //!
    TBranch *b_tau_tes_alpha_pt_shift;                                                                                 //!
    TBranch *b_weight_mc;                                                                                              //!

    void Init(TChain *chain) override {
        std::lock_guard<std::mutex> lock(_mutex);
        using namespace std;
        // Set object pointer
        jet_0_p4 = 0;
        lep_0_matched_p4 = 0;
        lep_0_p4 = 0;
        lephad_p4 = 0;
        met_reco_p4 = 0;
        tau_0_leadTrk_vertex_v = 0;
        tau_0_p4 = 0;
        tau_0_p4_uncorrected_caloTES = 0;
        tau_0_p4_uncorrected_mvaTES = 0;
        tau_0_track0_p4 = 0;
        tau_0_track1_p4 = 0;
        tau_0_track2_p4 = 0;
        tau_0_truth_p4 = 0;
        tau_0_truth_p4_vis_charged_track0 = 0;
        tau_0_truth_p4_vis_charged_track1 = 0;
        tau_0_truth_p4_vis_charged_track2 = 0;
        tau_0_truth_vis_charged_p4 = 0;
        tau_0_truth_vis_neutral_others_p4 = 0;
        tau_0_truth_vis_neutral_p4 = 0;
        tau_0_truth_vis_neutral_pions_p4 = 0;
        tau_0_truth_vis_p4 = 0;
        tau_0_truth_isEle = 0;
        tau_0_truth_isMuon = 0;
        // Set branch addresses and branch pointers
        fChain = chain;

        fChain->SetBranchAddress("HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM",
                                 &HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM,
                                 &b_HLT_mu14_ivarloose_L1MU11_tau35_medium1_tracktwo_L1MU11_TAU20IM);
        fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo", &HLT_mu14_ivarloose_tau25_medium1_tracktwo,
                                 &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25",
                                 &HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25,
                                 &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25);
        fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM",
                                 &HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM,
                                 &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM);
        fChain->SetBranchAddress("HLT_mu26_imedium", &HLT_mu26_imedium, &b_HLT_mu26_imedium);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
        fChain->SetBranchAddress("HLT_xe50", &HLT_xe50, &b_HLT_xe50);
        fChain->SetBranchAddress("L1_TAU25IM", &L1_TAU25IM, &b_L1_TAU25IM);
        fChain->SetBranchAddress("NOMINAL_pileup_combined_weight", &NOMINAL_pileup_combined_weight, &b_NOMINAL_pileup_combined_weight);
        fChain->SetBranchAddress("NOMINAL_pileup_random_run_number", &NOMINAL_pileup_random_run_number, &b_NOMINAL_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_combined_weight", &PRW_DATASF_1down_pileup_combined_weight,
                                 &b_PRW_DATASF_1down_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_random_run_number", &PRW_DATASF_1down_pileup_random_run_number,
                                 &b_PRW_DATASF_1down_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_combined_weight", &PRW_DATASF_1up_pileup_combined_weight,
                                 &b_PRW_DATASF_1up_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_random_run_number", &PRW_DATASF_1up_pileup_random_run_number,
                                 &b_PRW_DATASF_1up_pileup_random_run_number);
        fChain->SetBranchAddress("dilep_m", &dilep_m, &b_dilep_m);
        fChain->SetBranchAddress("event_clean_EC_LooseBad", &event_clean_EC_LooseBad, &b_event_clean_EC_LooseBad);
        fChain->SetBranchAddress("event_clean_detector_core", &event_clean_detector_core, &b_event_clean_detector_core);
        fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
        fChain->SetBranchAddress("jet_0", &jet_0, &b_jet_0);
        fChain->SetBranchAddress("jet_0_p4", &jet_0_p4, &b_jet_0_p4);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_effSF_JVT", &jet_NOMINAL_central_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_ineffSF_JVT", &jet_NOMINAL_central_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_effSF_JVT", &jet_NOMINAL_forward_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_ineffSF_JVT", &jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_MV2c10", &jet_NOMINAL_global_effSF_MV2c10, &b_jet_NOMINAL_global_effSF_MV2c10);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_MV2c10", &jet_NOMINAL_global_ineffSF_MV2c10, &b_jet_NOMINAL_global_ineffSF_MV2c10);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoFCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoFCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone",
                                 &lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone,
                                 &b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad", &lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad,
                                 &b_lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_Reco_QualMedium", &lep_0_NOMINAL_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_TTVA", &lep_0_NOMINAL_MuEffSF_TTVA, &b_lep_0_NOMINAL_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_id_loose", &lep_0_id_loose, &b_lep_0_id_loose);
        fChain->SetBranchAddress("lep_0_id_medium", &lep_0_id_medium, &b_lep_0_id_medium);
        fChain->SetBranchAddress("lep_0_id_tight", &lep_0_id_tight, &b_lep_0_id_tight);
        fChain->SetBranchAddress("lep_0_iso_FCLoose", &lep_0_iso_FCLoose, &b_lep_0_iso_FCLoose);
        fChain->SetBranchAddress("lep_0_iso_FCLoose_FixedRad", &lep_0_iso_FCLoose_FixedRad, &b_lep_0_iso_FCLoose_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_FCTight", &lep_0_iso_FCTight, &b_lep_0_iso_FCTight);
        fChain->SetBranchAddress("lep_0_iso_FCTightTrackOnly", &lep_0_iso_FCTightTrackOnly, &b_lep_0_iso_FCTightTrackOnly);
        fChain->SetBranchAddress("lep_0_iso_FCTightTrackOnly_FixedRad", &lep_0_iso_FCTightTrackOnly_FixedRad, &b_lep_0_iso_FCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_FCTight_FixedRad", &lep_0_iso_FCTight_FixedRad, &b_lep_0_iso_FCTight_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_FixedCutHighPtTrackOnly", &lep_0_iso_FixedCutHighPtTrackOnly, &b_lep_0_iso_FixedCutHighPtTrackOnly);
        fChain->SetBranchAddress("lep_0_iso_Gradient", &lep_0_iso_Gradient, &b_lep_0_iso_Gradient);
        fChain->SetBranchAddress("lep_0_matched", &lep_0_matched, &b_lep_0_matched);
        fChain->SetBranchAddress("lep_0_matched_classifierParticleOrigin", &lep_0_matched_classifierParticleOrigin,
                                 &b_lep_0_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("lep_0_matched_classifierParticleType", &lep_0_matched_classifierParticleType,
                                 &b_lep_0_matched_classifierParticleType);
        fChain->SetBranchAddress("lep_0_matched_origin", &lep_0_matched_origin, &b_lep_0_matched_origin);
        fChain->SetBranchAddress("lep_0_matched_p4", &lep_0_matched_p4, &b_lep_0_matched_p4);
        fChain->SetBranchAddress("lep_0_matched_pdgId", &lep_0_matched_pdgId, &b_lep_0_matched_pdgId);
        fChain->SetBranchAddress("lep_0_p4", &lep_0_p4, &b_lep_0_p4);
        fChain->SetBranchAddress("lep_0_q", &lep_0_q, &b_lep_0_q);
        fChain->SetBranchAddress("lep_0_trk_d0", &lep_0_trk_d0, &b_lep_0_trk_d0);
        fChain->SetBranchAddress("lep_0_trk_d0_sig", &lep_0_trk_d0_sig, &b_lep_0_trk_d0_sig);
        fChain->SetBranchAddress("lep_0_trk_z0", &lep_0_trk_z0, &b_lep_0_trk_z0);
        fChain->SetBranchAddress("lep_0_trk_z0_sig", &lep_0_trk_z0_sig, &b_lep_0_trk_z0_sig);
        fChain->SetBranchAddress("lep_0_trk_z0_sintheta", &lep_0_trk_z0_sintheta, &b_lep_0_trk_z0_sintheta);
        fChain->SetBranchAddress("lephad_deta", &lephad_deta, &b_lephad_deta);
        fChain->SetBranchAddress("lephad_dphi", &lephad_dphi, &b_lephad_dphi);
        fChain->SetBranchAddress("lephad_dr", &lephad_dr, &b_lephad_dr);
        fChain->SetBranchAddress("lephad_met_sum_cos_dphi", &lephad_met_sum_cos_dphi, &b_lephad_met_sum_cos_dphi);
        fChain->SetBranchAddress("lephad_mt_lep0_met", &lephad_mt_lep0_met, &b_lephad_mt_lep0_met);
        fChain->SetBranchAddress("lephad_mt_lep1_met", &lephad_mt_lep1_met, &b_lephad_mt_lep1_met);
        fChain->SetBranchAddress("lephad_p4", &lephad_p4, &b_lephad_p4);
        fChain->SetBranchAddress("lephad_qxq", &lephad_qxq, &b_lephad_qxq);
        fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
        fChain->SetBranchAddress("met_reco_p4", &met_reco_p4, &b_met_reco_p4);
        fChain->SetBranchAddress("met_reco_sumet", &met_reco_sumet, &b_met_reco_sumet);
        fChain->SetBranchAddress("muTrigMatch_0_HLT_mu26_ivarmedium", &muTrigMatch_0_HLT_mu26_ivarmedium, &b_muTrigMatch_0_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("muTrigMatch_0_HLT_mu50", &muTrigMatch_0_HLT_mu50, &b_muTrigMatch_0_HLT_mu50);
        fChain->SetBranchAddress("n_actual_int", &n_actual_int, &b_n_actual_int);
        fChain->SetBranchAddress("n_actual_int_cor", &n_actual_int_cor, &b_n_actual_int_cor);
        fChain->SetBranchAddress("n_avg_int", &n_avg_int, &b_n_avg_int);
        fChain->SetBranchAddress("n_avg_int_cor", &n_avg_int_cor, &b_n_avg_int_cor);
        fChain->SetBranchAddress("n_bjets", &n_bjets, &b_n_bjets);
        fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
        fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
        fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad", &tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTloose", &tau_0_NOMINAL_TauEffSF_JetBDTloose, &b_tau_0_NOMINAL_TauEffSF_JetBDTloose);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTmedium", &tau_0_NOMINAL_TauEffSF_JetBDTmedium, &b_tau_0_NOMINAL_TauEffSF_JetBDTmedium);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTtight", &tau_0_NOMINAL_TauEffSF_JetBDTtight, &b_tau_0_NOMINAL_TauEffSF_JetBDTtight);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron", &tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron", &tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron", &tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron", &tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron", &tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_reco", &tau_0_NOMINAL_TauEffSF_reco, &b_tau_0_NOMINAL_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1down_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16A_1up_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1down_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STATMC16D_1up_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDTPlusVeto_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDTPlusVeto_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDTPlusVeto_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_VeryLooseLlhEleOLR_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_VeryLooseLlhEleOLR_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_VeryLooseLlhEleOLR_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_allTrk_eta", &tau_0_allTrk_eta, &b_tau_0_allTrk_eta);
        fChain->SetBranchAddress("tau_0_allTrk_n", &tau_0_allTrk_n, &b_tau_0_allTrk_n);
        fChain->SetBranchAddress("tau_0_allTrk_phi", &tau_0_allTrk_phi, &b_tau_0_allTrk_phi);
        fChain->SetBranchAddress("tau_0_allTrk_pt", &tau_0_allTrk_pt, &b_tau_0_allTrk_pt);
        fChain->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode, &b_tau_0_decay_mode);
        fChain->SetBranchAddress("tau_0_jet_bdt_loose", &tau_0_jet_bdt_loose, &b_tau_0_jet_bdt_loose);
        fChain->SetBranchAddress("tau_0_jet_bdt_medium", &tau_0_jet_bdt_medium, &b_tau_0_jet_bdt_medium);
        fChain->SetBranchAddress("tau_0_jet_bdt_score", &tau_0_jet_bdt_score, &b_tau_0_jet_bdt_score);
        fChain->SetBranchAddress("tau_0_jet_bdt_score_trans", &tau_0_jet_bdt_score_trans, &b_tau_0_jet_bdt_score_trans);
        fChain->SetBranchAddress("tau_0_jet_bdt_tight", &tau_0_jet_bdt_tight, &b_tau_0_jet_bdt_tight);
        fChain->SetBranchAddress("tau_0_jet_bdt_veryloose", &tau_0_jet_bdt_veryloose, &b_tau_0_jet_bdt_veryloose);
        fChain->SetBranchAddress("tau_0_jet_jvt", &tau_0_jet_jvt, &b_tau_0_jet_jvt);
        fChain->SetBranchAddress("tau_0_jet_rnn_loose", &tau_0_jet_rnn_loose, &b_tau_0_jet_rnn_loose);
        fChain->SetBranchAddress("tau_0_jet_rnn_medium", &tau_0_jet_rnn_medium, &b_tau_0_jet_rnn_medium);
        fChain->SetBranchAddress("tau_0_jet_rnn_score", &tau_0_jet_rnn_score, &b_tau_0_jet_rnn_score);
        fChain->SetBranchAddress("tau_0_jet_rnn_score_trans", &tau_0_jet_rnn_score_trans, &b_tau_0_jet_rnn_score_trans);
        fChain->SetBranchAddress("tau_0_jet_rnn_tight", &tau_0_jet_rnn_tight, &b_tau_0_jet_rnn_tight);
        fChain->SetBranchAddress("tau_0_jet_rnn_veryloose", &tau_0_jet_rnn_veryloose, &b_tau_0_jet_rnn_veryloose);
        fChain->SetBranchAddress("tau_0_jet_width", &tau_0_jet_width, &b_tau_0_jet_width);
        fChain->SetBranchAddress("tau_0_leadTrk_d0", &tau_0_leadTrk_d0, &b_tau_0_leadTrk_d0);
        fChain->SetBranchAddress("tau_0_leadTrk_d0_sig", &tau_0_leadTrk_d0_sig, &b_tau_0_leadTrk_d0_sig);
        fChain->SetBranchAddress("tau_0_leadTrk_eta", &tau_0_leadTrk_eta, &b_tau_0_leadTrk_eta);
        fChain->SetBranchAddress("tau_0_leadTrk_phi", &tau_0_leadTrk_phi, &b_tau_0_leadTrk_phi);
        fChain->SetBranchAddress("tau_0_leadTrk_pt", &tau_0_leadTrk_pt, &b_tau_0_leadTrk_pt);
        fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0", &tau_0_leadTrk_pvx_z0, &b_tau_0_leadTrk_pvx_z0);
        fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0_sig", &tau_0_leadTrk_pvx_z0_sig, &b_tau_0_leadTrk_pvx_z0_sig);
        fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0_sintheta", &tau_0_leadTrk_pvx_z0_sintheta, &b_tau_0_leadTrk_pvx_z0_sintheta);
        fChain->SetBranchAddress("tau_0_leadTrk_vertex_v", &tau_0_leadTrk_vertex_v, &b_tau_0_leadTrk_vertex_v);
        fChain->SetBranchAddress("tau_0_leadTrk_z0", &tau_0_leadTrk_z0, &b_tau_0_leadTrk_z0);
        fChain->SetBranchAddress("tau_0_leadTrk_z0_sig", &tau_0_leadTrk_z0_sig, &b_tau_0_leadTrk_z0_sig);
        fChain->SetBranchAddress("tau_0_leadTrk_z0_sintheta", &tau_0_leadTrk_z0_sintheta, &b_tau_0_leadTrk_z0_sintheta);
        fChain->SetBranchAddress("tau_0_n_all_tracks", &tau_0_n_all_tracks, &b_tau_0_n_all_tracks);
        fChain->SetBranchAddress("tau_0_n_charged_tracks", &tau_0_n_charged_tracks, &b_tau_0_n_charged_tracks);
        fChain->SetBranchAddress("tau_0_n_conversion_tracks", &tau_0_n_conversion_tracks, &b_tau_0_n_conversion_tracks);
        fChain->SetBranchAddress("tau_0_n_core_tracks", &tau_0_n_core_tracks, &b_tau_0_n_core_tracks);
        fChain->SetBranchAddress("tau_0_n_failTrackFilter_tracks", &tau_0_n_failTrackFilter_tracks, &b_tau_0_n_failTrackFilter_tracks);
        fChain->SetBranchAddress("tau_0_n_fake_tracks", &tau_0_n_fake_tracks, &b_tau_0_n_fake_tracks);
        fChain->SetBranchAddress("tau_0_n_isolation_tracks", &tau_0_n_isolation_tracks, &b_tau_0_n_isolation_tracks);
        fChain->SetBranchAddress("tau_0_n_modified_isolation_tracks", &tau_0_n_modified_isolation_tracks, &b_tau_0_n_modified_isolation_tracks);
        fChain->SetBranchAddress("tau_0_n_old_tracks", &tau_0_n_old_tracks, &b_tau_0_n_old_tracks);
        fChain->SetBranchAddress("tau_0_n_passTrkSelectionTight_tracks", &tau_0_n_passTrkSelectionTight_tracks,
                                 &b_tau_0_n_passTrkSelectionTight_tracks);
        fChain->SetBranchAddress("tau_0_n_passTrkSelector_tracks", &tau_0_n_passTrkSelector_tracks, &b_tau_0_n_passTrkSelector_tracks);
        fChain->SetBranchAddress("tau_0_n_unclassified_tracks", &tau_0_n_unclassified_tracks, &b_tau_0_n_unclassified_tracks);
        fChain->SetBranchAddress("tau_0_n_wide_tracks", &tau_0_n_wide_tracks, &b_tau_0_n_wide_tracks);
        fChain->SetBranchAddress("tau_0_origin", &tau_0_origin, &b_tau_0_origin);
        fChain->SetBranchAddress("tau_0_p4", &tau_0_p4, &b_tau_0_p4);
        fChain->SetBranchAddress("tau_0_p4_uncorrected_caloTES", &tau_0_p4_uncorrected_caloTES, &b_tau_0_p4_uncorrected_caloTES);
        fChain->SetBranchAddress("tau_0_p4_uncorrected_mvaTES", &tau_0_p4_uncorrected_mvaTES, &b_tau_0_p4_uncorrected_mvaTES);
        fChain->SetBranchAddress("tau_0_q", &tau_0_q, &b_tau_0_q);
        fChain->SetBranchAddress("tau_0_sub_ConstituentBased_eta", &tau_0_sub_ConstituentBased_eta, &b_tau_0_sub_ConstituentBased_eta);
        fChain->SetBranchAddress("tau_0_sub_ConstituentBased_m", &tau_0_sub_ConstituentBased_m, &b_tau_0_sub_ConstituentBased_m);
        fChain->SetBranchAddress("tau_0_sub_ConstituentBased_phi", &tau_0_sub_ConstituentBased_phi, &b_tau_0_sub_ConstituentBased_phi);
        fChain->SetBranchAddress("tau_0_sub_ConstituentBased_pt", &tau_0_sub_ConstituentBased_pt, &b_tau_0_sub_ConstituentBased_pt);
        fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_eta", &tau_0_sub_ParticleFlowCombined_eta, &b_tau_0_sub_ParticleFlowCombined_eta);
        fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_m", &tau_0_sub_ParticleFlowCombined_m, &b_tau_0_sub_ParticleFlowCombined_m);
        fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_phi", &tau_0_sub_ParticleFlowCombined_phi, &b_tau_0_sub_ParticleFlowCombined_phi);
        fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_pt", &tau_0_sub_ParticleFlowCombined_pt, &b_tau_0_sub_ParticleFlowCombined_pt);
        fChain->SetBranchAddress("tau_0_sub_n_charged", &tau_0_sub_n_charged, &b_tau_0_sub_n_charged);
        fChain->SetBranchAddress("tau_0_sub_n_neutral", &tau_0_sub_n_neutral, &b_tau_0_sub_n_neutral);
        fChain->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4, &b_tau_0_track0_p4);
        fChain->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4, &b_tau_0_track1_p4);
        fChain->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4, &b_tau_0_track2_p4);
        fChain->SetBranchAddress("tau_0_truth_classifierParticleOrigin", &tau_0_truth_classifierParticleOrigin,
                                 &b_tau_0_truth_classifierParticleOrigin);
        fChain->SetBranchAddress("tau_0_truth_classifierParticleType", &tau_0_truth_classifierParticleType, &b_tau_0_truth_classifierParticleType);
        fChain->SetBranchAddress("tau_0_truth_decay_mode", &tau_0_truth_decay_mode, &b_tau_0_truth_decay_mode);
        fChain->SetBranchAddress("tau_0_truth_isEle", &tau_0_truth_isEle, &b_tau_0_truth_isEle);
        fChain->SetBranchAddress("tau_0_truth_isHadTau", &tau_0_truth_isHadTau, &b_tau_0_truth_isHadTau);
        fChain->SetBranchAddress("tau_0_truth_isJet", &tau_0_truth_isJet, &b_tau_0_truth_isJet);
        fChain->SetBranchAddress("tau_0_truth_isMuon", &tau_0_truth_isMuon, &b_tau_0_truth_isMuon);
        fChain->SetBranchAddress("tau_0_truth_isTau", &tau_0_truth_isTau, &b_tau_0_truth_isTau);
        fChain->SetBranchAddress("tau_0_truth_isTruthMatch", &tau_0_truth_isTruthMatch, &b_tau_0_truth_isTruthMatch);
        fChain->SetBranchAddress("tau_0_truth_mother_pdgId", &tau_0_truth_mother_pdgId, &b_tau_0_truth_mother_pdgId);
        fChain->SetBranchAddress("tau_0_truth_mother_status", &tau_0_truth_mother_status, &b_tau_0_truth_mother_status);
        fChain->SetBranchAddress("tau_0_truth_n_charged", &tau_0_truth_n_charged, &b_tau_0_truth_n_charged);
        fChain->SetBranchAddress("tau_0_truth_n_charged_pion", &tau_0_truth_n_charged_pion, &b_tau_0_truth_n_charged_pion);
        fChain->SetBranchAddress("tau_0_truth_n_neutral", &tau_0_truth_n_neutral, &b_tau_0_truth_n_neutral);
        fChain->SetBranchAddress("tau_0_truth_n_neutral_pion", &tau_0_truth_n_neutral_pion, &b_tau_0_truth_n_neutral_pion);
        fChain->SetBranchAddress("tau_0_truth_origin", &tau_0_truth_origin, &b_tau_0_truth_origin);
        fChain->SetBranchAddress("tau_0_truth_p4", &tau_0_truth_p4, &b_tau_0_truth_p4);
        fChain->SetBranchAddress("tau_0_truth_p4_vis_charged_track0", &tau_0_truth_p4_vis_charged_track0, &b_tau_0_truth_p4_vis_charged_track0);
        fChain->SetBranchAddress("tau_0_truth_p4_vis_charged_track1", &tau_0_truth_p4_vis_charged_track1, &b_tau_0_truth_p4_vis_charged_track1);
        fChain->SetBranchAddress("tau_0_truth_p4_vis_charged_track2", &tau_0_truth_p4_vis_charged_track2, &b_tau_0_truth_p4_vis_charged_track2);
        fChain->SetBranchAddress("tau_0_truth_pdgId", &tau_0_truth_pdgId, &b_tau_0_truth_pdgId);
        fChain->SetBranchAddress("tau_0_truth_pz", &tau_0_truth_pz, &b_tau_0_truth_pz);
        fChain->SetBranchAddress("tau_0_truth_q", &tau_0_truth_q, &b_tau_0_truth_q);
        fChain->SetBranchAddress("tau_0_truth_status", &tau_0_truth_status, &b_tau_0_truth_status);
        fChain->SetBranchAddress("tau_0_truth_type", &tau_0_truth_type, &b_tau_0_truth_type);
        fChain->SetBranchAddress("tau_0_truth_vis_charged_p4", &tau_0_truth_vis_charged_p4, &b_tau_0_truth_vis_charged_p4);
        fChain->SetBranchAddress("tau_0_truth_vis_neutral_others_p4", &tau_0_truth_vis_neutral_others_p4, &b_tau_0_truth_vis_neutral_others_p4);
        fChain->SetBranchAddress("tau_0_truth_vis_neutral_p4", &tau_0_truth_vis_neutral_p4, &b_tau_0_truth_vis_neutral_p4);
        fChain->SetBranchAddress("tau_0_truth_vis_neutral_pions_p4", &tau_0_truth_vis_neutral_pions_p4, &b_tau_0_truth_vis_neutral_pions_p4);
        fChain->SetBranchAddress("tau_0_truth_vis_p4", &tau_0_truth_vis_p4, &b_tau_0_truth_vis_p4);
        fChain->SetBranchAddress("tau_0_type", &tau_0_type, &b_tau_0_type);
        fChain->SetBranchAddress("tau_tes_alpha_pt_shift", &tau_tes_alpha_pt_shift, &b_tau_tes_alpha_pt_shift);
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);

        fChain->SetBranchStatus("lb_number", 0);
        fChain->SetBranchStatus("tau_0_trk_*", 0);
        fChain->SetBranchStatus("tau_0_trig*", 0);
        fChain->SetBranchStatus("tau_0_sub_neu*", 0);
        fChain->SetBranchStatus("tau_0_sub_ctrk*", 0);
        fChain->SetBranchStatus("tau_0_sub_PanTau_BDT*", 0);
        fChain->SetBranchStatus("tau_0_id_*", 0);
        fChain->SetBranchStatus("tau_0_ele*", 0);
        fChain->SetBranchStatus("*TRUEHADTAU_EFF_JETID*", 0);
        fChain->SetBranchStatus("jet_FT_EFF_Eigen_*", 0);
        fChain->SetBranchStatus("jet_FT_EFF_extrapolation*", 0);
        fChain->SetBranchStatus("L1_*", 0);
        fChain->SetBranchStatus("HLT_tau*", 0);
    }
};

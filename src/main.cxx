#include <TError.h>

#include <ctime>
#include <fstream>
#include <string>
#include <vector>

#include "Timer.h"
#include "easylogging++.h"

// function declearation
static void errorCrashHandler(int sig);
static int InitAnalysis();
int MainProcess(int argc, char* argv[]);
static int EndAnalysis();

INITIALIZE_EASYLOGGINGPP

void errorCrashHandler(int sig) {
    el::Helpers::logCrashReason(sig, true);
    // FOLLOWING LINE IS ABSOLUTELY NEEDED AT THE END IN ORDER TO ABORT APPLICATION
    el::Helpers::crashAbort(sig);
}

int InitAnalysis() {
    // Log setting
    el::Loggers::addFlag(el::LoggingFlag::MultiLoggerSupport);
    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
    el::Loggers::addFlag(el::LoggingFlag::LogDetailedCrashReason);
    el::Helpers::setCrashHandler(errorCrashHandler);

    el::Configurations conf;
    conf.setToDefault();
    // Values are always std::string
    conf.setGlobally(el::ConfigurationType::Format, "[%level | %datetime{%H:%m:%s} | %fbase:%line] %msg");
    // default logger uses default configurations
    el::Loggers::reconfigureLogger("default", conf);
    el::Loggers::reconfigureAllLoggers(el::Level::Debug, el::ConfigurationType::Enabled, "false");
    el::Loggers::reconfigureAllLoggers(el::Level::Verbose, el::ConfigurationType::Enabled, "false");

    // Supress Root set branch address Error by set Root Error output to kError
    gErrorIgnoreLevel = kError + 1;
    // Clock setting
    Timer::Instance().clock("Main");
    LOG(INFO) << "************************** Analysis Start **************************";
    return 0;
}

int EndAnalysis() {
    // Clock setting
    auto main_duration = Timer::Instance().format(Timer::Instance().clock("Main"));
    LOG(INFO) << "********** Analysis End. Total run time is: " << main_duration << " ***********";

    return 0;
}

int main(int argc, char* argv[]) {
    if (InitAnalysis() != 0) {
        return -1;
    }
    if (MainProcess(argc, argv) != 0) {
        return -2;
    }
    if (EndAnalysis() != 0) {
        return -3;
    }
    return 0;
}

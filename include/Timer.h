#pragma once
#include <chrono>
#include <iomanip>
#include <sstream>
#include <string>
#include <unordered_map>

#include "easylogging++.h"

class Timer {
    using sysTime = std::chrono::system_clock;

public:
    static Timer& Instance() {
        static Timer mTimer;
        return mTimer;
    }
    ~Timer(){};

    // Return the stored time of the query name. If there is no such name clock,
    // will create a clock with that name and return current time.
    sysTime::time_point time(std::string name) {
        if (_timeStore.find(name) == _timeStore.end()) {
            auto currentTime = sysTime::now();
            _timeStore[name] = currentTime;
            return currentTime;
        }
        return _timeStore[name];
    }

    // For duration calculation, we use milliseconds
    int duration(sysTime::time_point a, sysTime::time_point b) { return std::chrono::duration_cast<std::chrono::milliseconds>(b - a).count(); }

    int duration(std::string aName, std::string bName) {
        if (_timeStore.find(aName) == _timeStore.end()) {
            LOG(ERROR) << "No stored time named " << aName << "! Will return 0";
            return 0;
        }
        if (_timeStore.find(bName) == _timeStore.end()) {
            LOG(ERROR) << "No stored time named " << bName << "! Will return 0";
            return 0;
        }
        return std::chrono::duration_cast<std::chrono::milliseconds>(_timeStore[bName] - _timeStore[aName]).count();
    }

    // If there is no such name clock, will create a clock with that name and return 0.
    // If there is such clock, will return the duration bewteen now and this colck and **RENEW** the clock with current
    // time
    int clock(std::string name) {
        using namespace std::chrono;
        if (_timeStore.find(name) == _timeStore.end()) {
            _timeStore[name] = sysTime::now();
            return 0;
        }
        // The current time will change when processing lines. So we should store it into a variable first for
        // consistency
        auto currentTime = sysTime::now();
        auto t_duration = duration_cast<milliseconds>(currentTime - _timeStore[name]);
        _timeStore[name] = currentTime;
        return t_duration.count();
    }

    sysTime::time_point now() { return sysTime::now(); }

    std::string format(const sysTime::time_point& time, const std::string& format = "%Y-%m-%d %H:%M:%S") {
        std::time_t tt = std::chrono::system_clock::to_time_t(time);
        std::tm tm = *std::localtime(&tt); // Locale time-zone
        std::stringstream ss;
        ss << std::put_time(&tm, format.c_str());
        return ss.str();
    }

    std::string format(int t_duration) {
        int initial_second = t_duration / 1000;
        int ms = t_duration % 1000;
        int day = initial_second / 86400;
        int sec_left = initial_second % 86400;
        int hour = sec_left / 3600;
        sec_left = sec_left % 3600;
        int minite = sec_left / 60;
        int second = sec_left % 60;
        std::string outStr = "";
        if (day > 0) {
            outStr += std::to_string(day) + "d, ";
        }
        if (hour > 0) {
            outStr += std::to_string(hour) + "h, ";
        }
        if (minite > 0) {
            outStr += std::to_string(minite) + "m, ";
        }
        if (second > 0) {
            outStr += std::to_string(second) + "s, ";
        }
        outStr += std::to_string(ms) + "ms";
        return outStr;
    }

private:
    Timer(const Timer&) = delete;            // forbiden in singleton
    Timer& operator=(const Timer&) = delete; // forbiden in singleton
    Timer(){};

    std::unordered_map<std::string, sysTime::time_point> _timeStore; // usually we don't dynamiclly create timer so we use hash map to speed up
};

#include "AnaObjs.h"
#include "CutCountRunner.h"
#include "PhyUtils.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineCutCount(CutCount, XamppTree);

void CutCount::setCutSteps() {	

//  addStep("tau quality",{2 medium taus, 1 medium && 1 tight taus, 2 tight taus},'>')
	addStep("MET", {30,50,60,80,90,100,120,130,150}, '>');
    addStep("MT2",{20,40,50,60,70,80,90,100},'>');
//    addStep("dRtt",{2.4,2.6,2.8,3.0,3.2,6},'<');
    addStep("dPhitt",{0.4,0.5,0.6,0.7,0.8,1,1.2,1.4},'>');
    addStep("Meff_tt",{120,125,130,135,140,145,150,200,250,300},'>');
    addStep("tau1Pt",{80,95,100,110,120,130,140,150,160,170,180,200},'>');
    addStep("tau2Pt",{60,65,70,75,80,85,90,95,100,150,200},'>');
    addStep("tau1Mt",{20,40,50,60,70,80,90,100,150},'>');
    addStep("tau2Mt",{20,40,50,60,70,80,90,100,150},'>');
}

void CutCount::setResultFilters() {
    std::cout<<"Filter"<<std::endl;
    addYieldsFilter("totalBkg", 3);   //totalBkg
//   addYieldsFilter("Higgs_Nom",0);
//   addYieldsFilter("MultiBoson_Nom",0);
//   //addYieldsFilter("Top_Nom",0);
//   addYieldsFilter("W_Nom",0);
//   addYieldsFilter("Z_Nom",0);
//    addYieldsFilter("SingleTopS_Nom",0);
//    addYieldsFilter("SingleTopT_Nom",0);
//    addYieldsFilter("SingleTopWt_Nom",0);
//    addYieldsFilter("ttbar_Nom",0);
//    addYieldsFilter("ttV_Nom",0);
////    addYieldsFilter("ttX_M_Nom",0);
//    addYieldsFilter("ttX_X_Nom",0);
//    addYieldsFilter("tt_X_Z_Nom",0);
	addRawnumFilter("Higgs_Nom",1);
	addRawnumFilter("MultiBoson_Nom",1);
//	addRawnumFilter("Top_Nom",5);
	addRawnumFilter("W_Nom",1);
	addRawnumFilter("Z_Nom",1);
    /*
    addRawnumFilter("SingleTopS_Nom",5);
    addRawnumFilter("SingleTopT_Nom",5);
    addRawnumFilter("SingleTopWt_Nom",5);
    addRawnumFilter("ttbar_Nom",5);
    addRawnumFilter("ttV_Nom",5);
    addRawnumFilter("ttX_M_Nom",5);
    addRawnumFilter("ttX_X_Nom",5);
    addRawnumFilter("tt_X_Z_Nom",5);
    */
	addRelErrorFilter("totalBkg", 0.3);          // <0.3
	addRelErrorFilter("C1C1_Stau_300p0_150p0_Nom", 0.5);
//	addRelErrorFilter("signal", 0.3);
//	addYieldsFilter("", 0);
//  addRelErrorFilter("", 0.5);
    setZnFilter(1.5);//jidegai
}

void Looper_CutCount::setVariables() {

    setVar("MET", [&] { return tree->MetTST_met / 1000; });
//    std::cout<<"MET="<<tree->MET<<std::endl;
    setVar("MT2", [&] { return tree->MT2_min; });
//    std::cout<<"MT2="<<tree->MT2<<std::endl;
//    setVar("dRtt",   [&] {return tree->dRtt;   });
    setVar("dPhitt", [&] {return tree->dPhitt; });
    setVar("Meff_tt", [&] {return tree->Meff_TauTau; });
    setVar("tau1Pt", [&] {return tree->tau1Pt; });
    setVar("tau2Pt", [&] {return tree->tau2Pt; });
    setVar("tau1Mt", [&] {return tree->tau1Mt; });
    setVar("tau2Mt", [&] {return tree->tau2Mt; });
    setWeight([&] { return tree->Weight_mc; });
}
    

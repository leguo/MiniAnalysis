#include "SingleTreeRunner.h"
#include "CompressedVBF.h"
#include "easylogging++.h"
#include "AnaObjs.h"
//#include "PhyUtils.h"
#include "GRLDB.h" 
#include <iostream>
#include <fstream>
//using namespace std;
DefineLooper(VBF, CompressedVBF);

void VBF::loop()
{
	//std::string outFullName = getOutName();
	std::string outFullName = currentTreeName();
	std::string treeName = currentTreeName();
	LOG(INFO) << "currentTree name: " << treeName ;

	auto oTree = cloneCurrentAnaTree();
    
    Var["MET"] = 0;
    Var["vbfjjM"] = 0;
    Var["vbfTagJet1_Pt"] = 0;
    Var["vbfTagJet2_Pt"] = 0;
    Var["vbfTagJet1_Eta"] = 0;
    Var["vbfTagJet2_Eta"] = 0;
    Var["nBJet20"] = 999;
    Var["nAjets"] = 999;
    Var["vbfjjDEta"] = 0;
    Var["vbfjjDPhi"] =0;
    Var["HTVBF"] = 0;
    Var["minDPhiAllJetsMet"] = 0;
    Var["nLep_signal"] = 999;
    Var["totalWeight"] = 1;
    Var["trigMatch_metTrig"] = -1;
    Var["validVBFtags"] =-1;
	//oTree->Branch("mergedlumiBlock",&mlumiBlock);
    //新建一个Branch，命名并填值

    auto mCutflow = addCutflow();
    mCutflow->setName(Utils::splitStrBy(treeName,'_')[3]+"_"+Utils::splitStrBy(treeName,'_')[4]);
    mCutflow->setWeight([&]{return Var["totalWeight"];});    
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline",[&] {return fabs(Var["totalWeight"]) < 1000 ; });  //统计没有经过任何cut时的事例数
    mCutflow->registerCut("MET_Trigger", [&] {return Var["trigMatch_metTrig"] == 1;});
    mCutflow->registerCut("MET > 200",[&] {return Var["MET"] > 200;});
    mCutflow->registerCut("Valid VBF tag", [&] {return Var["validVBFtags"] == 1;});
    mCutflow->registerCut("vbfjjM > 500", [&] {return Var["vbfjjM"] > 500;});
    mCutflow->registerCut("nLep_signal = 0", [&] {return Var["nLep_signal"] == 0;});
    // SR
    mCutflow->registerCut("vbfjjM > 1000", [&] { return Var["vbfjjM"] > 1000;});
    mCutflow->registerCut("vbfTagJet1_Pt > 80", [&] {return Var["vbfTagJet1_Pt"] > 80;});
    mCutflow->registerCut("vbfTagJet2_Pt > 80", [&] {return Var["vbfTagJet2_Pt"] > 80;});
    mCutflow->registerCut("vbfjjDEta > 3", [&] {return Var["vbfjjDEta"] > 3;});
    mCutflow->registerCut("vbfjjDPhi < 2", [&] {return Var["vbfjjDPhi"] < 2;});
    mCutflow->registerCut("minDPhiAllJetsMet > 0.6", [&] {return Var["minDPhiAllJetsMet"] > 0.6;});
    mCutflow->registerCut("nBJet20 = 0", [&] { return Var["nBJet20"] == 0;});
    mCutflow->registerCut("2 <= nAJets <= 4",[&]{return Var["nAjets"] >= 2 && Var["nAjets"]<= 4;});
    //mCutflow->registerCut("MT2>80 GeV", [&] {return Var["MT2"] > 80; }, "MT2",12,30,150, [&] {return Var["MT2"];}, Hist::USE_OVERFLOW);
    auto Cut = mCutflow->registerCut("the END",[&] {return true ; });
    Cut->addHist("CompressedVBF2020__MET",18,100,1000, [&]{return Var["MET"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfjjM",9,500,5000, [&]{return Var["vbfjjM"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfTagJet1_Pt", 20, 0, 1000, [&]{return Var["vbfTagJet1_Pt"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfTagJet2_Pt", 20, 0, 1000,[&]{return Var["vbfTagJet2_Pt"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfTagJet1_Eta", 10, -5, 5,[&]{return Var["vbfTagJet1_Eta"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfTagJet2_Eta", 10, -5, 5,[&]{return Var["vbfTagJet2_Eta"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__nBJet20", 3, 0, 3,[&]{return Var["nBJet20"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__nAjets",2,2,4,[&]{return Var["nAjets"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfjjDEta",8,0,8,[&]{return Var["vbfjjDEta"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__vbfjjDPhi",6,0,3,[&]{return Var["vbfjjDPhi"] ;} );
    Cut->addHist("CompressedVBF2020__HTVBF",20,0,1000,[&]{return Var["HTVBF"] ;},  Hist::USE_OVERFLOW );
    Cut->addHist("CompressedVBF2020__minDPhiAllJetsMet",20,0,2,[&]{return Var["minDPhiAllJetsMet"];}, Hist::USE_OVERFLOW );

//Loop Start  对每一个事例进行筛选
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);
   	    
        Var["vbfjjM"] = tree->vbfjjM;
        Var["vbfTagJet1_Pt"] = tree->vbfTagJet1_Pt;
        Var["vbfTagJet2_Pt"] = tree->vbfTagJet2_Pt;
        Var["vbfTagJet1_Eta"]= tree->vbfTagJet1_Eta;
        Var["vbfTagJet2_Eta"]= tree->vbfTagJet2_Eta;
        Var["nBJet20"] = tree->nBJet20;
        Var["nAjets"] = tree->nAjets;       
        Var["vbfjjDPhi"] = tree->vbfjjDPhi;
        Var["vbfjjDEta"] = tree->vbfjjDEta;
   	    Var["MET"] =  tree->met_Et;
        Var["evtNum"] = tree->EventNumber;
        Var["totalWeight"] = 139000 * tree->genWeight * tree->eventWeight * tree->leptonWeight * tree->jvtWeight *tree->pileupWeight *tree->bTagWeight * tree->FFWeight;

        //Var["totalWeight"] = 1;
        Var["trigMatch_metTrig"] = tree->trigMatch_metTrig;
        Var["minDPhiAllJetsMet"] = tree->minDPhiAllJetsMet;
        Var["HTVBF"] = tree->HTVBF;
        Var["validVBFtags"] = tree->validVBFtags;
        Var["nLep_signal"] = tree->nLep_signal;

		fillRegions();
        fillCutflows();
    }
	
    for(auto cut : getCutflows()){
		//cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName,'.')[0]);
		cut->PrintOut("cutflow_" + Utils::splitStrBy(treeName,'_')[3]+"_"+Utils::splitStrBy(treeName,'_')[4]);
    }
}




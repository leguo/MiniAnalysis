#include "SingleTreeRunner.h"
#include "SubTauTree.h"
#include "easylogging++.h"

DefineLooper(SubTau_TESSystProducer, SubTauTree);

void SubTau_TESSystProducer::loop() {
    Var["totalWeight"] = 1;
    Var["TauSelection"] = 0;
    Var["MuonSelection"] = 0;
    Var["BaseSelection"] = 0;
    setWeight([&] { return Var["totalWeight"]; }); // Currently only to slim, don't care about weight

    // Nom TES, TER
    auto ter_em_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_em_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_EM");
    TLorentzVector *tau_0_p4_ter_em_dw = new TLorentzVector();
    oTree_ter_em_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_em_dw);
    ter_em_dw->setFillTree(oTree_ter_em_dw);
    ter_em_dw->registerCut("baseline", [&] { return true; });
    ter_em_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_em_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_em_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_em_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_em_dw->Pt() > 20; });
    ter_em_dw->registerCut("The END", [&] { return true; });

    auto tes_em_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_Dw");
    TLorentzVector *tau_0_p4_tes_em_dw = new TLorentzVector();
    oTree_tes_em_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_dw);
    tes_em_dw->setFillTree(oTree_tes_em_dw);
    tes_em_dw->registerCut("baseline", [&] { return true; });
    tes_em_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_dw->Pt() > 20; });
    tes_em_dw->registerCut("The END", [&] { return true; });

    auto tes_em_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_Up");
    TLorentzVector *tau_0_p4_tes_em_up = new TLorentzVector();
    oTree_tes_em_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_up);
    tes_em_up->setFillTree(oTree_tes_em_up);
    tes_em_up->registerCut("baseline", [&] { return true; });
    tes_em_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_up->Pt() > 20; });
    tes_em_up->registerCut("The END", [&] { return true; });

    auto ter_had_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_had_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_Had");
    TLorentzVector *tau_0_p4_ter_had_dw = new TLorentzVector();
    oTree_ter_had_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_had_dw);
    ter_had_dw->setFillTree(oTree_ter_had_dw);
    ter_had_dw->registerCut("baseline", [&] { return true; });
    ter_had_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_had_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_had_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_had_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_had_dw->Pt() > 20; });
    ter_had_dw->registerCut("The END", [&] { return true; });

    auto tes_had_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_Had_Dw");
    TLorentzVector *tau_0_p4_tes_had_dw = new TLorentzVector();
    oTree_tes_had_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_dw);
    tes_had_dw->setFillTree(oTree_tes_had_dw);
    tes_had_dw->registerCut("baseline", [&] { return true; });
    tes_had_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_dw->Pt() > 20; });
    tes_had_dw->registerCut("The END", [&] { return true; });

    auto tes_had_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_Had_Up");
    TLorentzVector *tau_0_p4_tes_had_up = new TLorentzVector();
    oTree_tes_had_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_up);
    tes_had_up->setFillTree(oTree_tes_had_up);
    tes_had_up->registerCut("baseline", [&] { return true; });
    tes_had_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_up->Pt() > 20; });
    tes_had_up->registerCut("The END", [&] { return true; });

    // fermiLow TES, TER
    auto ter_em_fermiLow_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_em_fermiLow_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_EM_FERMILOW");
    TLorentzVector *tau_0_p4_ter_em_fermiLow_dw = new TLorentzVector();
    oTree_ter_em_fermiLow_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_em_fermiLow_dw);
    ter_em_fermiLow_dw->setFillTree(oTree_ter_em_fermiLow_dw);
    ter_em_fermiLow_dw->registerCut("baseline", [&] { return true; });
    ter_em_fermiLow_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_em_fermiLow_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_em_fermiLow_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_em_fermiLow_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_em_fermiLow_dw->Pt() > 20; });
    ter_em_fermiLow_dw->registerCut("The END", [&] { return true; });

    auto tes_em_fermiLow_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_fermiLow_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_FERMILOW_Dw");
    TLorentzVector *tau_0_p4_tes_em_fermiLow_dw = new TLorentzVector();
    oTree_tes_em_fermiLow_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_fermiLow_dw);
    tes_em_fermiLow_dw->setFillTree(oTree_tes_em_fermiLow_dw);
    tes_em_fermiLow_dw->registerCut("baseline", [&] { return true; });
    tes_em_fermiLow_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_fermiLow_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_fermiLow_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_fermiLow_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_fermiLow_dw->Pt() > 20; });
    tes_em_fermiLow_dw->registerCut("The END", [&] { return true; });

    auto tes_em_fermiLow_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_fermiLow_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_FERMILOW_Up");
    TLorentzVector *tau_0_p4_tes_em_fermiLow_up = new TLorentzVector();
    oTree_tes_em_fermiLow_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_fermiLow_up);
    tes_em_fermiLow_up->setFillTree(oTree_tes_em_fermiLow_up);
    tes_em_fermiLow_up->registerCut("baseline", [&] { return true; });
    tes_em_fermiLow_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_fermiLow_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_fermiLow_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_fermiLow_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_fermiLow_up->Pt() > 20; });
    tes_em_fermiLow_up->registerCut("The END", [&] { return true; });

    auto ter_had_fermiLow_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_had_fermiLow_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_HAD_FERMILOW");
    TLorentzVector *tau_0_p4_ter_had_fermiLow_dw = new TLorentzVector();
    oTree_ter_had_fermiLow_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_had_fermiLow_dw);
    ter_had_fermiLow_dw->setFillTree(oTree_ter_had_fermiLow_dw);
    ter_had_fermiLow_dw->registerCut("baseline", [&] { return true; });
    ter_had_fermiLow_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_had_fermiLow_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_had_fermiLow_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_had_fermiLow_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_had_fermiLow_dw->Pt() > 20; });
    ter_had_fermiLow_dw->registerCut("The END", [&] { return true; });

    auto tes_had_fermiLow_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_fermiLow_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_HAD_FERMILOW_Dw");
    TLorentzVector *tau_0_p4_tes_had_fermiLow_dw = new TLorentzVector();
    oTree_tes_had_fermiLow_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_fermiLow_dw);
    tes_had_fermiLow_dw->setFillTree(oTree_tes_had_fermiLow_dw);
    tes_had_fermiLow_dw->registerCut("baseline", [&] { return true; });
    tes_had_fermiLow_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_fermiLow_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_fermiLow_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_fermiLow_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_fermiLow_dw->Pt() > 20; });
    tes_had_fermiLow_dw->registerCut("The END", [&] { return true; });

    auto tes_had_fermiLow_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_fermiLow_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_HAD_FERMILOW_Up");
    TLorentzVector *tau_0_p4_tes_had_fermiLow_up = new TLorentzVector();
    oTree_tes_had_fermiLow_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_fermiLow_up);
    tes_had_fermiLow_up->setFillTree(oTree_tes_had_fermiLow_up);
    tes_had_fermiLow_up->registerCut("baseline", [&] { return true; });
    tes_had_fermiLow_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_fermiLow_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_fermiLow_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_fermiLow_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_fermiLow_up->Pt() > 20; });
    tes_had_fermiLow_up->registerCut("The END", [&] { return true; });

    // fermiHigh TES, TER
    auto ter_em_fermiHigh_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_em_fermiHigh_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_EM_FERMIHIGH");
    TLorentzVector *tau_0_p4_ter_em_fermiHigh_dw = new TLorentzVector();
    oTree_ter_em_fermiHigh_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_em_fermiHigh_dw);
    ter_em_fermiHigh_dw->setFillTree(oTree_ter_em_fermiHigh_dw);
    ter_em_fermiHigh_dw->registerCut("baseline", [&] { return true; });
    ter_em_fermiHigh_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_em_fermiHigh_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_em_fermiHigh_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_em_fermiHigh_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_em_fermiHigh_dw->Pt() > 20; });
    ter_em_fermiHigh_dw->registerCut("The END", [&] { return true; });

    auto tes_em_fermiHigh_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_fermiHigh_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_FERMIHIGH_Dw");
    TLorentzVector *tau_0_p4_tes_em_fermiHigh_dw = new TLorentzVector();
    oTree_tes_em_fermiHigh_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_fermiHigh_dw);
    tes_em_fermiHigh_dw->setFillTree(oTree_tes_em_fermiHigh_dw);
    tes_em_fermiHigh_dw->registerCut("baseline", [&] { return true; });
    tes_em_fermiHigh_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_fermiHigh_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_fermiHigh_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_fermiHigh_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_fermiHigh_dw->Pt() > 20; });
    tes_em_fermiHigh_dw->registerCut("The END", [&] { return true; });

    auto tes_em_fermiHigh_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_em_fermiHigh_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_EM_FERMIHIGH_Up");
    TLorentzVector *tau_0_p4_tes_em_fermiHigh_up = new TLorentzVector();
    oTree_tes_em_fermiHigh_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_em_fermiHigh_up);
    tes_em_fermiHigh_up->setFillTree(oTree_tes_em_fermiHigh_up);
    tes_em_fermiHigh_up->registerCut("baseline", [&] { return true; });
    tes_em_fermiHigh_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_em_fermiHigh_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_em_fermiHigh_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_em_fermiHigh_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_em_fermiHigh_up->Pt() > 20; });
    tes_em_fermiHigh_up->registerCut("The END", [&] { return true; });

    auto ter_had_fermiHigh_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_ter_had_fermiHigh_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TER_HAD_FERMIHIGH");
    TLorentzVector *tau_0_p4_ter_had_fermiHigh_dw = new TLorentzVector();
    oTree_ter_had_fermiHigh_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_ter_had_fermiHigh_dw);
    ter_had_fermiHigh_dw->setFillTree(oTree_ter_had_fermiHigh_dw);
    ter_had_fermiHigh_dw->registerCut("baseline", [&] { return true; });
    ter_had_fermiHigh_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    ter_had_fermiHigh_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    ter_had_fermiHigh_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    ter_had_fermiHigh_dw->registerCut("tauPtCut", [&] { return tau_0_p4_ter_had_fermiHigh_dw->Pt() > 20; });
    ter_had_fermiHigh_dw->registerCut("The END", [&] { return true; });

    auto tes_had_fermiHigh_dw = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_fermiHigh_dw = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_HAD_FERMIHIGH_Dw");
    TLorentzVector *tau_0_p4_tes_had_fermiHigh_dw = new TLorentzVector();
    oTree_tes_had_fermiHigh_dw->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_fermiHigh_dw);
    tes_had_fermiHigh_dw->setFillTree(oTree_tes_had_fermiHigh_dw);
    tes_had_fermiHigh_dw->registerCut("baseline", [&] { return true; });
    tes_had_fermiHigh_dw->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_fermiHigh_dw->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_fermiHigh_dw->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_fermiHigh_dw->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_fermiHigh_dw->Pt() > 20; });
    tes_had_fermiHigh_dw->registerCut("The END", [&] { return true; });

    auto tes_had_fermiHigh_up = addCutflow(Cutflow::NO_HIST);
    auto oTree_tes_had_fermiHigh_up = cloneCurrentAnaTree("TAUS_TRUEHADTAU_SME_TES_HAD_FERMIHIGH_Up");
    TLorentzVector *tau_0_p4_tes_had_fermiHigh_up = new TLorentzVector();
    oTree_tes_had_fermiHigh_up->SetBranchAddress("tau_0_p4", &tau_0_p4_tes_had_fermiHigh_up);
    tes_had_fermiHigh_up->setFillTree(oTree_tes_had_fermiHigh_up);
    tes_had_fermiHigh_up->registerCut("baseline", [&] { return true; });
    tes_had_fermiHigh_up->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    tes_had_fermiHigh_up->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    tes_had_fermiHigh_up->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    tes_had_fermiHigh_up->registerCut("tauPtCut", [&] { return tau_0_p4_tes_had_fermiHigh_up->Pt() > 20; });
    tes_had_fermiHigh_up->registerCut("The END", [&] { return true; });

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["BaseSelection"] = false;
        Var["MuonSelection"] = false;
        Var["TauSelection"] = false;

        Var["MuonSelection"] = (tree->lep_0_id_medium == 1 && fabs(tree->lep_0_p4->Eta()) < 2.5 &&
                                ((tree->HLT_mu26_ivarmedium == 1 && tree->lep_0_p4->Pt() > 27.3 && tree->muTrigMatch_0_HLT_mu26_ivarmedium == 1) ||
                                 (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50)));

        double tauEta = tree->tau_0_p4->Eta();
        double DPhi = fabs(tree->tau_0_p4->Phi() - tree->lep_0_p4->Phi());
        if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;
        bool isOS = (tree->lephad_qxq == -1);

        Var["TauSelection"] = (tree->tau_0_jet_bdt_score_trans > 0.005 && abs(tree->tau_0_q) == 1 &&
                               (tree->tau_0_n_charged_tracks == 1 || tree->tau_0_n_charged_tracks == 3) &&
                               ((fabs(tauEta) < 1.37) || (1.52 < fabs(tauEta) && fabs(tauEta) < 2.47)));
        bool SRcut = (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
                      tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        bool SRSScut =
            (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 &&
             tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS && currentTreeName() == "NOMINAL");
        bool WCRCut =
            (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 && tree->met_reco_sumet > 20 &&
             tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && tree->lephad_met_sum_cos_dphi < 0 && currentTreeName() == "NOMINAL");
        bool QCRCut =
            (!tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 &&
             tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && currentTreeName() == "NOMINAL");
        bool topCR = (tree->n_bjets != 0 && tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->lephad_mt_lep0_met > 40 && tree->met_reco_sumet > 30);
        bool slimCut = (SRcut || SRSScut || WCRCut || QCRCut);
        Var["BaseSelection"] = (Var["MuonSelection"] && Var["TauSelection"] && slimCut);

        // Change the TLorentzVector of the tau for the syst
        // First, make a function template to let the process easier
        std::function<double(std::string)> getSyst = [&](std::string name) {
            double newPt = tree->tau_0_p4->Pt();
            if (tree->tau_0_n_charged_tracks == 1 && tree->tau_0_truth_isTau) {
                newPt = tree->getTauEnergySyst("1P_" + name, tree->tau_0_p4->Pt(), tree->tau_0_truth_vis_neutral_p4->Pt(),
                                               tree->tau_0_truth_vis_p4->Pt());
            } else if (tree->tau_0_n_charged_tracks == 3 && tree->tau_0_truth_isTau) {
                newPt = tree->getTauEnergySyst("3P_" + name, tree->tau_0_p4->Pt(), tree->tau_0_truth_vis_neutral_p4->Pt(),
                                               tree->tau_0_truth_vis_p4->Pt());
            }
            return newPt;
        };

        // get syst
        // Nom TES, TER
        tau_0_p4_ter_em_dw->SetPtEtaPhiM(getSyst("TER_EM"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_dw->SetPtEtaPhiM(getSyst("TESDw_EM"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_up->SetPtEtaPhiM(getSyst("TESUp_EM"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_ter_had_dw->SetPtEtaPhiM(getSyst("TER_Had"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_had_dw->SetPtEtaPhiM(getSyst("TESDw_Had"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_had_up->SetPtEtaPhiM(getSyst("TESUp_Had"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());

        // FermiLow TES, TER
        tau_0_p4_ter_em_fermiLow_dw->SetPtEtaPhiM(getSyst("TER_EM_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_fermiLow_dw->SetPtEtaPhiM(getSyst("TESDw_EM_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_fermiLow_up->SetPtEtaPhiM(getSyst("TESUp_EM_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_ter_had_fermiLow_dw->SetPtEtaPhiM(getSyst("TER_Had_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_had_fermiLow_dw->SetPtEtaPhiM(getSyst("TESDw_Had_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_had_fermiLow_up->SetPtEtaPhiM(getSyst("TESUp_Had_fermiLow"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());

        // fermiHigh TES, TER
        tau_0_p4_ter_em_fermiHigh_dw->SetPtEtaPhiM(getSyst("TER_EM_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_fermiHigh_dw->SetPtEtaPhiM(getSyst("TESDw_EM_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_em_fermiHigh_up->SetPtEtaPhiM(getSyst("TESUp_EM_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_ter_had_fermiHigh_dw->SetPtEtaPhiM(getSyst("TER_Had_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(), tree->tau_0_p4->M());
        tau_0_p4_tes_had_fermiHigh_dw->SetPtEtaPhiM(getSyst("TESDw_Had_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(),
                                                    tree->tau_0_p4->M());
        tau_0_p4_tes_had_fermiHigh_up->SetPtEtaPhiM(getSyst("TESUp_Had_fermiHigh"), tree->tau_0_p4->Eta(), tree->tau_0_p4->Phi(),
                                                    tree->tau_0_p4->M());

        LOG(DEBUG) << "Origin Pt:" << tree->tau_0_p4->Pt() << "  Origin neutralPt:" << tree->tau_0_truth_vis_neutral_p4->Pt()
                   << "  Origin truth Pt:" << tree->tau_0_truth_vis_p4->Pt() << "  ter_em_dw:" << getSyst("TER_EM")
                   << "  tes_em_dw:" << getSyst("TESDw_EM") << " tes_em_up:" << getSyst("TESUp_EM") << "  ter_had_dw:" << getSyst("TER_Had")
                   << "  tes_had_dw:" << getSyst("TESDw_Had") << "  tes_had_up:" << getSyst("TESUp_Had");

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
}

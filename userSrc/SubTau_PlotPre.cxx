#include <iostream>

#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "SubTauTree.h"
#include "easylogging++.h"

DefineLooper(SubTau_PlotPre, SubTauTree);

void SubTau_PlotPre::loop() {
    // register vars
    Var["upsilon"] = 0;
    Var["NTrack"] = 0;
    Var["Mu"] = 0;
    Var["TauPt"] = 0;
    Var["TauEta"] = 0;
    Var["MuPt"] = 0;
    Var["MuEta"] = 0;
    Var["LepHadVisMass"] = 0;
    Var["isData"] = 0;
    Var["isW"] = 0;
    Var["isLep"] = 0;
    Var["WCR"] = 0;
    Var["WCRSS"] = 0;
    Var["SR"] = 0;
    Var["SRSS"] = 0;
    Var["tauID"] = 0;
    Var["QCR"] = 0;
    Var["QCRSS"] = 0;
    Var["TruthMode"] = 0;
    Var["RecoMode"] = 0;
    Var["Weight"] = 0;
    // tauID setting. 0: nocut, 1: loose, 2: medium, 3: tight
    Var["tauIDCheckBox"] = 0;
    if (AnaConfigReader::Instance().readKey<std::string>("tauID") != NULL) {
        std::string IDChoice = AnaConfigReader::Instance().readKey<std::string>("tauID");
        if (IDChoice == "loose") {
            LOG(INFO) << "Selected tauID: loose";
            Var["tauIDCheckBox"] = 1;
        } else if (IDChoice == "medium") {
            LOG(INFO) << "Selected tauID: medium";
            Var["tauIDCheckBox"] = 2;
        } else if (IDChoice == "tight") {
            LOG(INFO) << "Selected tauID: tight";
            Var["tauIDCheckBox"] = 3;
        } else {
            LOG(INFO) << "Selected tauID: nocut";
        }
    } else {
        LOG(INFO) << "Selected tauID: nocut";
    }

    // Vars needed for region Definition
    //	std::function<double()> upsilon = [&] {return ((2 * tauAlltracked_pt - tauReco_pt) / tauReco_pt); };
    std::function<double()> upsilon = [&] { return Var["upsilon"]; };
    std::function<double()> NTrack = [&] { return Var["NTrack"]; };
    std::function<double()> Mu = [&] { return Var["Mu"]; };
    std::function<double()> TauPt = [&] { return Var["TauPt"]; };
    std::function<double()> TauEta = [&] { return Var["TauEta"]; };
    std::function<double()> MuEta = [&] { return Var["MuEta"]; };
    std::function<double()> MuPt = [&] { return Var["MuPt"]; };
    std::function<double()> LepHadVisMass = [&] { return Var["LepHadVisMass"]; };

    // Weight setting
    setWeight([&] { return Var["Weight"]; });

    // Register regions
    // loop for different Reco decaymode
    // The region number keeper
    int regionNum = 0;
    for (int i = 0; i < 5; ++i) {
        std::string RecoDecayTag;
        int Nbins(48), NbinsNT(12);
        double LowValue(0), UpValue(0);

        // bins for NTrack
        double LowValueNT(0), UpValueNT(12);
        // bins for upsilon
        if (0 == i) {
            RecoDecayTag = "R1";
            LowValue = 0.6;
            UpValue = 1.2;
            UpValueNT = 8;
            NbinsNT = 8;
        } else if (1 == i) {
            RecoDecayTag = "R2";
            LowValue = -1;
            UpValue = 1.4;
        } else if (2 == i) {
            RecoDecayTag = "R3";
            LowValue = -0.95;
            UpValue = 0.25;
        } else if (3 == i) {
            RecoDecayTag = "R4";
            LowValue = 0.8;
            UpValue = 1.1;
        } else if (4 == i) {
            RecoDecayTag = "R5";
            LowValue = -0.5;
            UpValue = 1.1;
        }

        HistTemplate upsilonHistTem(upsilon, Nbins, LowValue, UpValue, Hist::USE_OVERFLOW);
        HistTemplate etaHistTem(TauEta, 20, 0, 2.5, Hist::USE_OVERFLOW);
        HistTemplate pTHistTem(TauPt, 20, 0, 100, Hist::USE_OVERFLOW);
        HistTemplate NTrackHistTem(NTrack, NbinsNT, LowValueNT, UpValueNT, Hist::USE_OVERFLOW);
        HistTemplate LepHadVisMassTem(LepHadVisMass, 32, 40, 120, Hist::USE_OVERFLOW);

        // SR Data template region
        auto regionSR_Data =
            addRegion([&, i] { return (Var["isData"] && Var["SR"] && Var["tauID"] && Var["RecoMode"] == i); }, "Tau" + RecoDecayTag + "/SR/Data");
        // Lep template region
        auto regionSR_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["SR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/SR/Lep");
        // Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionSR_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["SR"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/SR/Tau" + TruthDecayTag);
        }
        // W template region
        auto regionSR_W = addRegion([&, i] { return (!Var["isData"] && Var["SR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                    "Tau" + RecoDecayTag + "/SR/W");

        // WCR Data template region
        auto regionWCR_Data =
            addRegion([&, i] { return (Var["WCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isData"]); }, "Tau" + RecoDecayTag + "/WCR/Data");
        auto regionWCR_W = addRegion([&, i] { return (!Var["isData"] && Var["WCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                     "Tau" + RecoDecayTag + "/WCR/W");
        // WCR Lep template region
        auto regionWCR_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["WCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/WCR/Lep");
        // WCR Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionWCR_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["WCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/WCR/Tau" + TruthDecayTag);
        }

        // SRSS Data template region
        auto regionSRSS_Data =
            addRegion([&, i] { return (Var["SRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isData"]); }, "Tau" + RecoDecayTag + "/SRSS/Data");
        auto regionSRSS_W = addRegion([&, i] { return (!Var["isData"] && Var["SRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                      "Tau" + RecoDecayTag + "/SRSS/W");
        // SRSS Lep template region
        auto regionSRSS_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["SRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/SRSS/Lep");
        // SRSS Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionSRSS_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["SRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/SRSS/Tau" + TruthDecayTag);
        }

        // WCRSS Data template region
        auto regionWCRSS_Data = addRegion([&, i] { return (Var["WCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isData"]); },
                                          "Tau" + RecoDecayTag + "/WCRSS/Data");
        // Add W TF syst! Same as nominal but will multiply syst variation by hand in WorkspaceBuilder!
        auto regionWCRSS_W = addRegion([&, i] { return (!Var["isData"] && Var["WCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                       "Tau" + RecoDecayTag + "/WCRSS/W");
        // WCRSS Lep template region
        auto regionWCRSS_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["WCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/WCRSS/Lep");
        // WCRSS Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionWCRSS_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["WCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/WCRSS/Tau" + TruthDecayTag);
        }

        // QCR Data template region
        auto regionQCR_Data =
            addRegion([&, i] { return (Var["QCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isData"]); }, "Tau" + RecoDecayTag + "/QCR/Data");
        auto regionQCR_W = addRegion([&, i] { return (!Var["isData"] && Var["QCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                     "Tau" + RecoDecayTag + "/QCR/W");
        // QCR Lep template region
        auto regionQCR_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["QCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/QCR/Lep");
        // QCR Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionQCR_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["QCR"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/QCR/Tau" + TruthDecayTag);
        }

        // QCRSS Data template region
        auto regionQCRSS_Data = addRegion([&, i] { return (Var["QCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isData"]); },
                                          "Tau" + RecoDecayTag + "/QCRSS/Data");
        auto regionQCRSS_W = addRegion([&, i] { return (!Var["isData"] && Var["QCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isW"]); },
                                       "Tau" + RecoDecayTag + "/QCRSS/W");
        // QCRSS Lep template region
        auto regionQCRSS_Lep =
            addRegion([&, i] { return (!Var["isData"] && Var["QCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["isLep"] && !Var["isW"]); },
                      "Tau" + RecoDecayTag + "/QCRSS/Lep");
        // QCRSS Tau template region
        for (int j = 0; j < 5; ++j) {
            std::string TruthDecayTag = "T" + std::to_string(j + 1);
            auto regionQCRSS_Sig = addRegion(
                [&, i, j] { return !Var["isData"] && Var["QCRSS"] && Var["tauID"] && Var["RecoMode"] == i && Var["TruthMode"] == j && !Var["isW"]; },
                "Tau" + RecoDecayTag + "/QCRSS/Tau" + TruthDecayTag);
        }

        // Add histograms for each region.  Because the hist is stored in different Dir, so there will not be overwrite!
        if (currentTreeName() == "NOMINAL") {
            int newRegionNum = getRegions().size();
            for (int i = regionNum; i < newRegionNum; i++) {
                auto region = getRegions()[i];
                region->addHist(upsilonHistTem, "Upsilon");
                region->addHist(etaHistTem, "Eta");
                region->addHist(pTHistTem, "Pt");
                region->addHist(NTrackHistTem, "Ntrack");
                region->addHist(LepHadVisMassTem, "Mll");
            }
            regionNum = newRegionNum;
        }
    }

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["upsilon"] = (2 * tree->tau_0_allTrk_pt - tree->tau_0_p4->Pt()) / tree->tau_0_p4->Pt();
        Var["NTrack"] = tree->tau_0_n_charged_tracks + tree->tau_0_n_conversion_tracks + tree->tau_0_n_isolation_tracks;
        Var["Mu"] = tree->n_avg_int_cor;
        Var["TauPt"] = tree->tau_0_p4->Pt();
        Var["TauEta"] = fabs(tree->tau_0_p4->Eta());
        Var["MuPt"] = tree->lep_0_p4->Pt();
        Var["MuEta"] = fabs(tree->lep_0_p4->Eta());
        Var["LepHadVisMass"] = tree->lephad_p4->M();
        Var["isData"] = (tree->mc_channel_number == 0);
        Var["isW"] = (tree->mc_channel_number >= 361100 && tree->mc_channel_number <= 361105);
        if (Var["tauIDCheckBox"] == 1) {
            Var["tauID"] = (tree->tau_0_jet_rnn_loose == 1 && tree->tau_0_jet_rnn_medium == 0);
        } else if (Var["tauIDCheckBox"] == 2) {
            Var["tauID"] = (tree->tau_0_jet_rnn_medium == 1 && tree->tau_0_jet_rnn_tight == 0);
        } else if (Var["tauIDCheckBox"] == 3) {
            Var["tauID"] = (tree->tau_0_jet_rnn_tight == 1);
        } else {
            Var["tauID"] = (tree->tau_0_jet_rnn_loose == 0);
        }
        Var["isLep"] = (tree->tau_0_truth_isEle || tree->tau_0_truth_isMuon);
        Var["TruthMode"] = tree->tau_0_truth_decay_mode;
        Var["RecoMode"] = tree->tau_0_decay_mode;

        // Weight and compaign setting
        if (tree->mc_channel_number == 0) {
            Var["Weight"] = 1;
        } else {
            double extraWei = tree->weight_mc * tree->NOMINAL_pileup_combined_weight * tree->jet_NOMINAL_forward_jets_global_effSF_JVT *
                              tree->jet_NOMINAL_forward_jets_global_ineffSF_JVT * tree->jet_NOMINAL_central_jets_global_effSF_JVT *
                              tree->jet_NOMINAL_central_jets_global_ineffSF_JVT * tree->tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad *
                              tree->tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron * tree->jet_NOMINAL_global_ineffSF_MV2c10 *
                              tree->jet_NOMINAL_global_effSF_MV2c10 * tree->lep_0_NOMINAL_MuEffSF_Reco_QualMedium * tree->lep_0_NOMINAL_MuEffSF_TTVA *
                              tree->tau_0_NOMINAL_TauEffSF_reco * tree->lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad *
                              tree->lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
            Var["Weight"] =
                MetaDB::Instance().getWeight(PhyUtils::getCompaign(tree->NOMINAL_pileup_random_run_number), tree->mc_channel_number, 0, extraWei);
        }
        if (fabs(Var["Weight"] > 100)) continue;

        bool MuonSelection = (tree->lep_0_id_medium == 1 && Var["MuEta"] < 2.5 &&
                              ((tree->HLT_mu26_ivarmedium == 1 && tree->lep_0_p4->Pt() > 27.3 && tree->muTrigMatch_0_HLT_mu26_ivarmedium == 1) ||
                               (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50)));

        double DPhi = fabs(tree->tau_0_p4->Phi() - tree->lep_0_p4->Phi());
        if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;

        bool TauSelection = (tree->tau_0_jet_bdt_score_trans > 0.005 && Var["TauPt"] > 20 && abs(tree->tau_0_q) == 1 &&
                             (tree->tau_0_n_charged_tracks == 1 || tree->tau_0_n_charged_tracks == 3) &&
                             ((Var["TauEta"] < 1.37) || (1.52 < Var["TauEta"] && Var["TauEta"] < 2.47)));
        bool isOS = (tree->lephad_qxq == -1);

        Var["SR"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
             tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        Var["SRSS"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
             tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS);
        Var["QCR"] =
            (MuonSelection && !tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
             tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        Var["QCRSS"] =
            (MuonSelection && !tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
             tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS);
        Var["WCR"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 &&
             tree->met_reco_sumet > 20 && tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && tree->lephad_met_sum_cos_dphi < 0 && isOS);
        Var["WCRSS"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 &&
             tree->met_reco_sumet > 20 && tree->lephad_p4->M() < 90 && tree->lephad_p4->M() > 48 && tree->lephad_met_sum_cos_dphi < 0 && !isOS);
        fillCutflows();
        fillRegions();
    }
    int i = 0;
    for (auto region : getRegions()) {
        region->printOut("yield", std::to_string(i));
        i++;
    }
}

#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Xampp_AddVjetsSysts, XamppTree);

// Oh, you write this function in so bad way? Look at the 'brilliant' initial func at
// https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/scripts/AddZJetsWeights.py!
// I tried my best to adjust it... Someone could use if((mcChannelNumber % xx) == yy) in the furture
static double getPtBin(int mcChannelNumber) {
    if ((mcChannelNumber >= 363460 && mcChannelNumber < 363463) || (mcChannelNumber >= 363331 && mcChannelNumber < 363334) ||
        (mcChannelNumber >= 363388 && mcChannelNumber < 363391) || (mcChannelNumber >= 363412 && mcChannelNumber < 363415) ||
        (mcChannelNumber >= 364142 && mcChannelNumber < 364145) || (mcChannelNumber >= 363099 && mcChannelNumber < 363102) ||
        (mcChannelNumber >= 363364 && mcChannelNumber < 363367) || (mcChannelNumber >= 363436 && mcChannelNumber < 363439) ||
        (mcChannelNumber >= 364100 && mcChannelNumber < 364103) || (mcChannelNumber >= 364128 && mcChannelNumber < 364131) ||
        (mcChannelNumber >= 364170 && mcChannelNumber < 364173) || (mcChannelNumber >= 364156 && mcChannelNumber < 364159) ||
        (mcChannelNumber >= 364184 && mcChannelNumber < 364187))
        return 1;

    if ((mcChannelNumber >= 363391 && mcChannelNumber < 363394) || (mcChannelNumber >= 363415 && mcChannelNumber < 363418) ||
        (mcChannelNumber >= 363367 && mcChannelNumber < 363370) || (mcChannelNumber >= 363102 && mcChannelNumber < 363105) ||
        (mcChannelNumber >= 363463 && mcChannelNumber < 363466) || (mcChannelNumber >= 364145 && mcChannelNumber < 364148) ||
        (mcChannelNumber >= 363439 && mcChannelNumber < 363442) || (mcChannelNumber >= 363334 && mcChannelNumber < 363337) ||
        (mcChannelNumber >= 364117 && mcChannelNumber < 364120) || (mcChannelNumber >= 364103 && mcChannelNumber < 364106) ||
        (mcChannelNumber >= 364187 && mcChannelNumber < 364190) || (mcChannelNumber >= 364173 && mcChannelNumber < 364176) ||
        (mcChannelNumber >= 364131 && mcChannelNumber < 364134) || (mcChannelNumber >= 364159 && mcChannelNumber < 364162))
        return 2;

    if ((mcChannelNumber >= 363418 && mcChannelNumber < 363421) || (mcChannelNumber >= 363394 && mcChannelNumber < 363397) ||
        (mcChannelNumber >= 363370 && mcChannelNumber < 363373) || (mcChannelNumber >= 363105 && mcChannelNumber < 363108) ||
        (mcChannelNumber >= 363466 && mcChannelNumber < 363469) || (mcChannelNumber >= 364148 && mcChannelNumber < 364151) ||
        (mcChannelNumber >= 363442 && mcChannelNumber < 363445) || (mcChannelNumber >= 363337 && mcChannelNumber < 363340) ||
        (mcChannelNumber >= 364120 && mcChannelNumber < 364123) || (mcChannelNumber >= 364106 && mcChannelNumber < 364109) ||
        (mcChannelNumber >= 364176 && mcChannelNumber < 364179) || (mcChannelNumber >= 364134 && mcChannelNumber < 364137) ||
        (mcChannelNumber >= 364190 && mcChannelNumber < 364193) || (mcChannelNumber >= 364162 && mcChannelNumber < 364165))
        return 3;

    if ((mcChannelNumber >= 363445 && mcChannelNumber < 363448) || (mcChannelNumber >= 363421 && mcChannelNumber < 363424) ||
        (mcChannelNumber >= 363397 && mcChannelNumber < 363400) || (mcChannelNumber >= 363373 && mcChannelNumber < 363376) ||
        (mcChannelNumber >= 363108 && mcChannelNumber < 363111) || (mcChannelNumber >= 363469 && mcChannelNumber < 363472) ||
        (mcChannelNumber >= 363340 && mcChannelNumber < 363343) || (mcChannelNumber >= 364151 && mcChannelNumber < 364154) ||
        (mcChannelNumber >= 364123 && mcChannelNumber < 364126) || (mcChannelNumber >= 364109 && mcChannelNumber < 364112) ||
        (mcChannelNumber >= 364179 && mcChannelNumber < 364182) || (mcChannelNumber >= 364137 && mcChannelNumber < 364140) ||
        (mcChannelNumber >= 364165 && mcChannelNumber < 364168) || (mcChannelNumber >= 364193 && mcChannelNumber < 364196))
        return 4;

    if ((mcChannelNumber >= 363424 && mcChannelNumber < 363427) || (mcChannelNumber >= 363400 && mcChannelNumber < 363403) ||
        (mcChannelNumber >= 363376 && mcChannelNumber < 363379) || (mcChannelNumber >= 363111 && mcChannelNumber < 363114) ||
        (mcChannelNumber >= 363472 && mcChannelNumber < 363475) || (mcChannelNumber >= 363448 && mcChannelNumber < 363451) ||
        (mcChannelNumber >= 363343 && mcChannelNumber < 363346))
        return 5;
    if ((mcChannelNumber >= 363427 && mcChannelNumber < 363430) || (mcChannelNumber >= 363403 && mcChannelNumber < 363406) ||
        (mcChannelNumber >= 363379 && mcChannelNumber < 363382) || (mcChannelNumber >= 363114 && mcChannelNumber < 363117) ||
        (mcChannelNumber >= 363475 && mcChannelNumber < 363478) || (mcChannelNumber >= 363451 && mcChannelNumber < 363454) ||
        (mcChannelNumber >= 363346 && mcChannelNumber < 363349))
        return 6;

    if ((mcChannelNumber >= 363430 && mcChannelNumber < 363433) || (mcChannelNumber >= 363406 && mcChannelNumber < 363409) ||
        (mcChannelNumber >= 363382 && mcChannelNumber < 363385) || (mcChannelNumber >= 363117 && mcChannelNumber < 363120) ||
        (mcChannelNumber >= 363478 && mcChannelNumber < 363481) || (mcChannelNumber >= 363454 && mcChannelNumber < 363457) ||
        (mcChannelNumber >= 363349 && mcChannelNumber < 363352))
        return 7;

    if ((mcChannelNumber >= 363433 && mcChannelNumber < 363436) || (mcChannelNumber >= 363409 && mcChannelNumber < 363412) ||
        (mcChannelNumber >= 363385 && mcChannelNumber < 363388) || (mcChannelNumber >= 363120 && mcChannelNumber < 363123) ||
        (mcChannelNumber >= 363481 && mcChannelNumber < 363484) || (mcChannelNumber >= 363447 && mcChannelNumber < 363460) ||
        (mcChannelNumber >= 363352 && mcChannelNumber < 363355))
        return 8;

    for (int i : {366011, 366012, 366013, 366020, 366021, 366022, 366029, 366030, 366031, 366010, 366019, 366028}) {
        if (mcChannelNumber == i) return 2;
    }

    for (int i : {366014, 366015, 366016, 366023, 366024, 366025, 366032, 366033, 366034}) {
        if (mcChannelNumber == i) return 3;
    }
    for (int i : {366017, 366026, 366035}) {
        if (mcChannelNumber == i) return 4;
    }

    for (int i : {364154, 364222, 364126, 364218, 364112, 364216, 364140, 364220, 364196, 364228, 364168, 364224, 364182, 364226}) {
        if (mcChannelNumber == i) return 5;
    }

    for (int i : {364155, 364113, 364217, 364127, 364219, 364197, 364229, 364183, 364227, 364141, 364221, 364169, 364225, 364223}) {
        if (mcChannelNumber == i) return 7;
    }
    return 0;
}

static std::string getDecayType(int MCID) {
    if ((363102 <= MCID && MCID <= 363122) || (363361 <= MCID && MCID <= 363363) || (363364 <= MCID && MCID <= 363411) ||
        (364114 <= MCID && MCID <= 364127) || (364100 <= MCID && MCID <= 364113) || (364128 <= MCID && MCID <= 364141) ||
        (364216 <= MCID && MCID <= 364217) || (364220 <= MCID && MCID <= 364221)) {
        return "Zee";
    } else if ((363412 <= MCID && MCID <= 363435) || (364142 <= MCID && MCID <= 364155) || (366010 <= MCID && MCID <= 366035) ||
               (364222 <= MCID && MCID <= 364223)) {
        return "Znunu";
    } else if ((363331 <= MCID && MCID <= 363354) || (363436 <= MCID && MCID <= 363459) || (363460 <= MCID && MCID <= 363483) ||
               (364156 <= MCID && MCID <= 364169) || (364170 <= MCID && MCID <= 364183) || (364184 <= MCID && MCID <= 364197) ||
               (364226 <= MCID && MCID <= 364227) || (364224 <= MCID && MCID <= 364225) || (364228 <= MCID && MCID <= 364229)) {
        return "Wenu";
    }
    return "none";
}

void Xampp_AddVjetsSysts::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    // "Staus" length is 5

    auto oTree = cloneCurrentAnaTree();

    Var["ckkw15_Weight"] = 0;
    Var["ckkw30_Weight"] = 0;
    Var["fac025_Weight"] = 0;
    Var["fac4_Weight"] = 0;
    Var["renorm025_Weight"] = 0;
    Var["renorm4_Weight"] = 0;
    Var["qsf025_Weight"] = 0;
    Var["qsf4_Weight"] = 0;

    Cutflow_ptr mCutflow = addCutflow(Cutflow::NO_HIST);

    // set mc channel number to 0 initially to seperate data and mc
    tree->mcChannelNumber = 0;
    // Only do for W+jets and Z+jets
    bool addBranch = false;
    if (currentTreeName() == "Z_Nominal" || currentTreeName() == "W_Nominal") {
        addBranch = true;
    }
    if (addBranch) {
        oTree->Branch("GenWeight_LHE_ckkwWeight_dw", &Var["ckkw15_Weight"]);
        oTree->Branch("GenWeight_LHE_ckkwWeight_up", &Var["ckkw30_Weight"]);
        oTree->Branch("GenWeight_LHE_facWeight_dw", &Var["fac4_Weight"]);
        oTree->Branch("GenWeight_LHE_facWeight_up", &Var["fac025_Weight"]);
        oTree->Branch("GenWeight_LHE_renormWeight_dw", &Var["renorm4_Weight"]);
        oTree->Branch("GenWeight_LHE_renormWeight_up", &Var["renorm025_Weight"]);
        oTree->Branch("GenWeight_LHE_qsfWeight_dw", &Var["qsf4_Weight"]);
        oTree->Branch("GenWeight_LHE_qsfWeight_up", &Var["qsf025_Weight"]);
    }

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    auto preselect = mCutflow->registerCut("The END", [&] { return true; });

    std::map<std::string, TH2F*> Histos;
    std::string weightFile = AnaConfigReader::Instance().readKey<std::string>("WeightSample");
    LOG(INFO) << "Loaded the VJets Syst Weights File:" << weightFile;
    TFile tfile(weightFile.c_str());

    for (std::string sample : {"Zee", "Znunu", "Wenu"}) {
        for (std::string systList : {"ckkw15", "ckkw30", "fac025", "fac4", "renorm025", "renorm4", "qsf025", "qsf4"}) {
            std::string histName = sample + systList;
            Histos[histName] = (TH2F*)tfile.Get(histName.c_str());
        }
    }

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        if (addBranch) {
            int ZpTBin = getPtBin(tree->mcChannelNumber);

            for (std::string systList : {"ckkw15", "ckkw30", "fac025", "fac4", "renorm025", "renorm4", "qsf025", "qsf4"}) {
                std::string systName = systList + "_Weight";
                Var[systName] = 1;
            }

            if (ZpTBin != 0) {
                int nTruthJetBin = tree->n_TruthJets;
                if (nTruthJetBin > 12) {
                    nTruthJetBin = 12;
                }
                std::string decayType = getDecayType(tree->mcChannelNumber);
                if (decayType != "none") {
                    for (std::string systList : {"ckkw15", "ckkw30", "fac025", "fac4", "renorm025", "renorm4", "qsf025", "qsf4"}) {
                        std::string systName = systList + "_Weight";
                        Var[systName] = Histos[decayType + systList]->GetBinContent(ZpTBin, nTruthJetBin) * tree->GenWeight;
                    }
                }
            }
        }

        fillRegions();
        fillCutflows();
    }
}
